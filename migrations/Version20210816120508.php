<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210816120508 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, parent_id INT DEFAULT NULL, image_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, category_slug VARCHAR(50) NOT NULL, INDEX IDX_64C19C1727ACA70 (parent_id), UNIQUE INDEX UNIQ_64C19C13DA5256D (image_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE category_tag_type_limitation (category_id INT NOT NULL, tag_type_limitation_id INT NOT NULL, INDEX IDX_45F44AB812469DE2 (category_id), INDEX IDX_45F44AB860033EEB (tag_type_limitation_id), PRIMARY KEY(category_id, tag_type_limitation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, logo_id INT DEFAULT NULL, banner_id INT DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, name VARCHAR(80) NOT NULL, description VARCHAR(255) DEFAULT NULL, type VARCHAR(40) DEFAULT NULL, company_slug VARCHAR(80) NOT NULL, client_id VARCHAR(80) DEFAULT NULL, big INT DEFAULT NULL, credit INT DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(60) DEFAULT NULL, postal_code VARCHAR(255) DEFAULT NULL, siret VARCHAR(25) DEFAULT NULL, tva_rate VARCHAR(255) DEFAULT NULL, tva_code VARCHAR(15) DEFAULT NULL, code_ape VARCHAR(15) DEFAULT NULL, iban VARCHAR(11) DEFAULT NULL, bic VARCHAR(34) DEFAULT NULL, currency VARCHAR(15) DEFAULT NULL, activation_token VARCHAR(255) DEFAULT NULL, enabled TINYINT(1) DEFAULT \'0\', is_verified TINYINT(1) NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, UNIQUE INDEX UNIQ_4FBF094FDBF0818C (company_slug), UNIQUE INDEX UNIQ_4FBF094F19EB6921 (client_id), UNIQUE INDEX UNIQ_4FBF094F26E94372 (siret), UNIQUE INDEX UNIQ_4FBF094FF98F144A (logo_id), UNIQUE INDEX UNIQ_4FBF094F684EC833 (banner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contract (id INT AUTO_INCREMENT NOT NULL, rent_id INT DEFAULT NULL, lender_id INT DEFAULT NULL, borrower_id INT DEFAULT NULL, big_value INT NOT NULL, validation_date DATE NOT NULL, cagibig_commission INT NOT NULL, stock_commission INT NOT NULL, maintenance_commission INT DEFAULT NULL, contract_content LONGTEXT NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_E98F2859E5FD6250 (rent_id), INDEX IDX_E98F2859855D3E3D (lender_id), INDEX IDX_E98F285911CE312B (borrower_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE email (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, company_id INT DEFAULT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_E7927C74A76ED395 (user_id), INDEX IDX_E7927C74979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, logo_id INT DEFAULT NULL, banner_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, name VARCHAR(80) NOT NULL, description VARCHAR(255) DEFAULT NULL, event_slug VARCHAR(80) NOT NULL, date_start DATETIME(6) NOT NULL, date_end DATETIME(6) NOT NULL, all_day TINYINT(1) DEFAULT NULL, active TINYINT(1) DEFAULT NULL, budget INT DEFAULT NULL, background_color VARCHAR(7) DEFAULT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, UNIQUE INDEX UNIQ_3BAE0AA7F98F144A (logo_id), UNIQUE INDEX UNIQ_3BAE0AA7684EC833 (banner_id), INDEX IDX_3BAE0AA7727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE img_avatar (id INT AUTO_INCREMENT NOT NULL, image_name VARCHAR(255) NOT NULL, file_path VARCHAR(255) DEFAULT NULL, alt VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE img_banner (id INT AUTO_INCREMENT NOT NULL, image_name VARCHAR(255) NOT NULL, file_path VARCHAR(255) DEFAULT NULL, alt VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE img_category (id INT AUTO_INCREMENT NOT NULL, image_name VARCHAR(255) NOT NULL, file_path VARCHAR(255) DEFAULT NULL, alt VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE img_logo (id INT AUTO_INCREMENT NOT NULL, image_name VARCHAR(255) NOT NULL, file_path VARCHAR(255) DEFAULT NULL, alt VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE img_product (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, image_name VARCHAR(255) NOT NULL, file_path VARCHAR(255) DEFAULT NULL, alt VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, INDEX IDX_9E254045979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE invest_model (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(40) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE investment (id INT AUTO_INCREMENT NOT NULL, logo_id INT DEFAULT NULL, banner_id INT DEFAULT NULL, invest_model_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, description VARCHAR(200) NOT NULL, investment_slug VARCHAR(100) NOT NULL, geografic_limitation VARCHAR(255) DEFAULT NULL, end_cost INT DEFAULT NULL, dead_line DATE DEFAULT NULL, credit_lifetime VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:dateinterval)\', state JSON DEFAULT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, UNIQUE INDEX UNIQ_43CA0AD6F98F144A (logo_id), UNIQUE INDEX UNIQ_43CA0AD6684EC833 (banner_id), INDEX IDX_43CA0AD627E7C4BA (invest_model_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE line (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, amortization_year INT NOT NULL, percentage INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opening_time (id INT AUTO_INCREMENT NOT NULL, stockpile_id INT DEFAULT NULL, days VARCHAR(255) NOT NULL, start_time TIME DEFAULT NULL, end_time TIME DEFAULT NULL, INDEX IDX_89115E6EA4D0E58D (stockpile_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pack_relationship (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, number INT NOT NULL, slug VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, `function` VARCHAR(255) DEFAULT NULL, params JSON DEFAULT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE phone (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, company_id INT DEFAULT NULL, phone_number VARCHAR(35) NOT NULL COMMENT \'(DC2Type:phone_number)\', type VARCHAR(80) NOT NULL, INDEX IDX_444F97DDA76ED395 (user_id), INDEX IDX_444F97DD979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, state_id INT DEFAULT NULL, line_id INT DEFAULT NULL, stockpile_id INT DEFAULT NULL, investment_id INT DEFAULT NULL, time_coefficient_id INT DEFAULT NULL, name VARCHAR(80) NOT NULL, product_reference VARCHAR(80) NOT NULL, global_reference VARCHAR(30) NOT NULL, description VARCHAR(350) DEFAULT NULL, disponibility TINYINT(1) DEFAULT NULL, quantity INT DEFAULT NULL, geografic_limitation VARCHAR(255) DEFAULT NULL, big_cost INT DEFAULT NULL, deposit_value VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:money)\', tva_option INT DEFAULT NULL, control_documentation JSON DEFAULT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, weight VARCHAR(255) DEFAULT NULL, length VARCHAR(255) DEFAULT NULL, height VARCHAR(255) DEFAULT NULL, width VARCHAR(255) DEFAULT NULL, transport_volume VARCHAR(255) DEFAULT NULL, prepared_by INT DEFAULT NULL, current VARCHAR(255) DEFAULT NULL, power VARCHAR(255) DEFAULT NULL, surface_material TINYINT(1) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, INDEX IDX_D34A04AD5D83CC1 (state_id), INDEX IDX_D34A04AD4D7B7542 (line_id), INDEX IDX_D34A04ADA4D0E58D (stockpile_id), INDEX IDX_D34A04AD6E1B4FD5 (investment_id), INDEX IDX_D34A04AD128E4554 (time_coefficient_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_category (product_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_CDFC73564584665A (product_id), INDEX IDX_CDFC735612469DE2 (category_id), PRIMARY KEY(product_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_product_image (product_id INT NOT NULL, product_image_id INT NOT NULL, INDEX IDX_BE8C63F14584665A (product_id), INDEX IDX_BE8C63F1F6154FFA (product_image_id), PRIMARY KEY(product_id, product_image_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_tag_type_limitation (product_id INT NOT NULL, tag_type_limitation_id INT NOT NULL, INDEX IDX_97E64B134584665A (product_id), INDEX IDX_97E64B1360033EEB (tag_type_limitation_id), PRIMARY KEY(product_id, tag_type_limitation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rent (id INT AUTO_INCREMENT NOT NULL, stage_id INT DEFAULT NULL, rent_slug VARCHAR(80) NOT NULL, title VARCHAR(80) NOT NULL, intern_rent TINYINT(1) DEFAULT NULL, date_start DATETIME(6) NOT NULL, date_end DATETIME(6) NOT NULL, created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, INDEX IDX_2784DCC2298D193 (stage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rent_product (rent_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_32667FDDE5FD6250 (rent_id), INDEX IDX_32667FDD4584665A (product_id), PRIMARY KEY(rent_id, product_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_company_user (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, user_id INT DEFAULT NULL, roles JSON NOT NULL, is_verified TINYINT(1) NOT NULL, INDEX IDX_5AA8A07B979B1AD6 (company_id), INDEX IDX_5AA8A07BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_event_company (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, company_id INT DEFAULT NULL, roles JSON NOT NULL, budget INT DEFAULT NULL, INDEX IDX_4519687371F7E88B (event_id), INDEX IDX_45196873979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_event_user (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, company_id INT DEFAULT NULL, user_id INT DEFAULT NULL, roles JSON NOT NULL, is_verified TINYINT(1) NOT NULL, INDEX IDX_1D6CF0CE71F7E88B (event_id), INDEX IDX_1D6CF0CE979B1AD6 (company_id), INDEX IDX_1D6CF0CEA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_investment_company (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, investment_id INT DEFAULT NULL, roles JSON NOT NULL, credit INT DEFAULT NULL, credit_lifetime VARCHAR(255) DEFAULT NULL COMMENT \'(DC2Type:dateinterval)\', created_by VARCHAR(255) DEFAULT NULL, updated_by VARCHAR(255) DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, INDEX IDX_DD97D1E6979B1AD6 (company_id), INDEX IDX_DD97D1E66E1B4FD5 (investment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_stockpile_company (id INT AUTO_INCREMENT NOT NULL, stockpile_id INT DEFAULT NULL, company_id INT DEFAULT NULL, roles JSON NOT NULL, INDEX IDX_36662B5DA4D0E58D (stockpile_id), INDEX IDX_36662B5D979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_stockpile_user (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, stockpile_id INT DEFAULT NULL, user_id INT DEFAULT NULL, roles JSON NOT NULL, is_verified TINYINT(1) NOT NULL, INDEX IDX_D33AA7C8979B1AD6 (company_id), INDEX IDX_D33AA7C8A4D0E58D (stockpile_id), INDEX IDX_D33AA7C8A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_company_event_role (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, name JSON NOT NULL, description VARCHAR(100) NOT NULL, rules VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_company_investment_role (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, name JSON NOT NULL, description VARCHAR(100) NOT NULL, rules VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_company_stockpile_role (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, name JSON NOT NULL, description VARCHAR(100) NOT NULL, rules VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_company_user_role (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, name JSON NOT NULL, description VARCHAR(100) NOT NULL, rules VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_event_user_role (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(50) NOT NULL, name JSON NOT NULL, description VARCHAR(100) NOT NULL, rules VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_invest_tag_type_limitation (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug_limitation_investment VARCHAR(100) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag_type_limitation_investment (tag_type_limitation_id INT NOT NULL, investment_id INT NOT NULL, INDEX IDX_D05FA00060033EEB (tag_type_limitation_id), INDEX IDX_D05FA0006E1B4FD5 (investment_id), PRIMARY KEY(tag_type_limitation_id, investment_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag_type_limitation_company (tag_type_limitation_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_B1F1C9F460033EEB (tag_type_limitation_id), INDEX IDX_B1F1C9F4979B1AD6 (company_id), PRIMARY KEY(tag_type_limitation_id, company_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_prod_tag_type_limitation (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug_product_limitation VARCHAR(80) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE setting_time_coefficient (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(50) NOT NULL, description VARCHAR(255) DEFAULT NULL, data LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', calcul_function VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_E2BA98D85E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stage (id INT AUTO_INCREMENT NOT NULL, event_id INT DEFAULT NULL, name VARCHAR(50) NOT NULL, stage_slug VARCHAR(50) NOT NULL, details VARCHAR(255) NOT NULL, address VARCHAR(255) NOT NULL, city VARCHAR(60) NOT NULL, postal_code INT DEFAULT NULL, INDEX IDX_C27C936971F7E88B (event_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE state (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, value INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stockpile (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(40) NOT NULL, stockpile_slug VARCHAR(40) NOT NULL, address VARCHAR(255) NOT NULL, city VARCHAR(60) NOT NULL, postal_code VARCHAR(255) NOT NULL, contract_content LONGTEXT DEFAULT NULL, stock_commission INT DEFAULT NULL, maintenance_commission INT DEFAULT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, avatar_id INT DEFAULT NULL, username VARCHAR(200) NOT NULL, email VARCHAR(80) NOT NULL, api_token VARCHAR(180) DEFAULT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, nickname VARCHAR(80) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, city VARCHAR(50) DEFAULT NULL, postal_code VARCHAR(255) DEFAULT NULL, enabled TINYINT(1) DEFAULT NULL, is_verified TINYINT(1) NOT NULL, created_at DATETIME(6) NOT NULL, updated_at DATETIME(6) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6497BA2F5EB (api_token), UNIQUE INDEX UNIQ_8D93D64986383B10 (avatar_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1727ACA70 FOREIGN KEY (parent_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C13DA5256D FOREIGN KEY (image_id) REFERENCES img_category (id)');
        $this->addSql('ALTER TABLE category_tag_type_limitation ADD CONSTRAINT FK_45F44AB812469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE category_tag_type_limitation ADD CONSTRAINT FK_45F44AB860033EEB FOREIGN KEY (tag_type_limitation_id) REFERENCES setting_prod_tag_type_limitation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FF98F144A FOREIGN KEY (logo_id) REFERENCES img_logo (id)');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F684EC833 FOREIGN KEY (banner_id) REFERENCES img_banner (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859E5FD6250 FOREIGN KEY (rent_id) REFERENCES rent (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F2859855D3E3D FOREIGN KEY (lender_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE contract ADD CONSTRAINT FK_E98F285911CE312B FOREIGN KEY (borrower_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C74A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C74979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7F98F144A FOREIGN KEY (logo_id) REFERENCES img_logo (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7684EC833 FOREIGN KEY (banner_id) REFERENCES img_banner (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7727ACA70 FOREIGN KEY (parent_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE img_product ADD CONSTRAINT FK_9E254045979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE investment ADD CONSTRAINT FK_43CA0AD6F98F144A FOREIGN KEY (logo_id) REFERENCES img_logo (id)');
        $this->addSql('ALTER TABLE investment ADD CONSTRAINT FK_43CA0AD6684EC833 FOREIGN KEY (banner_id) REFERENCES img_banner (id)');
        $this->addSql('ALTER TABLE investment ADD CONSTRAINT FK_43CA0AD627E7C4BA FOREIGN KEY (invest_model_id) REFERENCES invest_model (id)');
        $this->addSql('ALTER TABLE opening_time ADD CONSTRAINT FK_89115E6EA4D0E58D FOREIGN KEY (stockpile_id) REFERENCES stockpile (id)');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DDA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE phone ADD CONSTRAINT FK_444F97DD979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD5D83CC1 FOREIGN KEY (state_id) REFERENCES state (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD4D7B7542 FOREIGN KEY (line_id) REFERENCES line (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADA4D0E58D FOREIGN KEY (stockpile_id) REFERENCES stockpile (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD6E1B4FD5 FOREIGN KEY (investment_id) REFERENCES investment (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD128E4554 FOREIGN KEY (time_coefficient_id) REFERENCES setting_time_coefficient (id)');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC73564584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC735612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_product_image ADD CONSTRAINT FK_BE8C63F14584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_product_image ADD CONSTRAINT FK_BE8C63F1F6154FFA FOREIGN KEY (product_image_id) REFERENCES img_product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_tag_type_limitation ADD CONSTRAINT FK_97E64B134584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_tag_type_limitation ADD CONSTRAINT FK_97E64B1360033EEB FOREIGN KEY (tag_type_limitation_id) REFERENCES setting_prod_tag_type_limitation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rent ADD CONSTRAINT FK_2784DCC2298D193 FOREIGN KEY (stage_id) REFERENCES stage (id)');
        $this->addSql('ALTER TABLE rent_product ADD CONSTRAINT FK_32667FDDE5FD6250 FOREIGN KEY (rent_id) REFERENCES rent (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE rent_product ADD CONSTRAINT FK_32667FDD4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_company_user ADD CONSTRAINT FK_5AA8A07B979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE role_company_user ADD CONSTRAINT FK_5AA8A07BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE role_event_company ADD CONSTRAINT FK_4519687371F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE role_event_company ADD CONSTRAINT FK_45196873979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE role_event_user ADD CONSTRAINT FK_1D6CF0CE71F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE role_event_user ADD CONSTRAINT FK_1D6CF0CE979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE role_event_user ADD CONSTRAINT FK_1D6CF0CEA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE role_investment_company ADD CONSTRAINT FK_DD97D1E6979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE role_investment_company ADD CONSTRAINT FK_DD97D1E66E1B4FD5 FOREIGN KEY (investment_id) REFERENCES investment (id)');
        $this->addSql('ALTER TABLE role_stockpile_company ADD CONSTRAINT FK_36662B5DA4D0E58D FOREIGN KEY (stockpile_id) REFERENCES stockpile (id)');
        $this->addSql('ALTER TABLE role_stockpile_company ADD CONSTRAINT FK_36662B5D979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE role_stockpile_user ADD CONSTRAINT FK_D33AA7C8979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE role_stockpile_user ADD CONSTRAINT FK_D33AA7C8A4D0E58D FOREIGN KEY (stockpile_id) REFERENCES stockpile (id)');
        $this->addSql('ALTER TABLE role_stockpile_user ADD CONSTRAINT FK_D33AA7C8A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tag_type_limitation_investment ADD CONSTRAINT FK_D05FA00060033EEB FOREIGN KEY (tag_type_limitation_id) REFERENCES setting_invest_tag_type_limitation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_type_limitation_investment ADD CONSTRAINT FK_D05FA0006E1B4FD5 FOREIGN KEY (investment_id) REFERENCES investment (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_type_limitation_company ADD CONSTRAINT FK_B1F1C9F460033EEB FOREIGN KEY (tag_type_limitation_id) REFERENCES setting_invest_tag_type_limitation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_type_limitation_company ADD CONSTRAINT FK_B1F1C9F4979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936971F7E88B FOREIGN KEY (event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64986383B10 FOREIGN KEY (avatar_id) REFERENCES img_avatar (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C1727ACA70');
        $this->addSql('ALTER TABLE category_tag_type_limitation DROP FOREIGN KEY FK_45F44AB812469DE2');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC735612469DE2');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859855D3E3D');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F285911CE312B');
        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C74979B1AD6');
        $this->addSql('ALTER TABLE img_product DROP FOREIGN KEY FK_9E254045979B1AD6');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DD979B1AD6');
        $this->addSql('ALTER TABLE role_company_user DROP FOREIGN KEY FK_5AA8A07B979B1AD6');
        $this->addSql('ALTER TABLE role_event_company DROP FOREIGN KEY FK_45196873979B1AD6');
        $this->addSql('ALTER TABLE role_event_user DROP FOREIGN KEY FK_1D6CF0CE979B1AD6');
        $this->addSql('ALTER TABLE role_investment_company DROP FOREIGN KEY FK_DD97D1E6979B1AD6');
        $this->addSql('ALTER TABLE role_stockpile_company DROP FOREIGN KEY FK_36662B5D979B1AD6');
        $this->addSql('ALTER TABLE role_stockpile_user DROP FOREIGN KEY FK_D33AA7C8979B1AD6');
        $this->addSql('ALTER TABLE tag_type_limitation_company DROP FOREIGN KEY FK_B1F1C9F4979B1AD6');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7727ACA70');
        $this->addSql('ALTER TABLE role_event_company DROP FOREIGN KEY FK_4519687371F7E88B');
        $this->addSql('ALTER TABLE role_event_user DROP FOREIGN KEY FK_1D6CF0CE71F7E88B');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936971F7E88B');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64986383B10');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F684EC833');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7684EC833');
        $this->addSql('ALTER TABLE investment DROP FOREIGN KEY FK_43CA0AD6684EC833');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C13DA5256D');
        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FF98F144A');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7F98F144A');
        $this->addSql('ALTER TABLE investment DROP FOREIGN KEY FK_43CA0AD6F98F144A');
        $this->addSql('ALTER TABLE product_product_image DROP FOREIGN KEY FK_BE8C63F1F6154FFA');
        $this->addSql('ALTER TABLE investment DROP FOREIGN KEY FK_43CA0AD627E7C4BA');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD6E1B4FD5');
        $this->addSql('ALTER TABLE role_investment_company DROP FOREIGN KEY FK_DD97D1E66E1B4FD5');
        $this->addSql('ALTER TABLE tag_type_limitation_investment DROP FOREIGN KEY FK_D05FA0006E1B4FD5');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD4D7B7542');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC73564584665A');
        $this->addSql('ALTER TABLE product_product_image DROP FOREIGN KEY FK_BE8C63F14584665A');
        $this->addSql('ALTER TABLE product_tag_type_limitation DROP FOREIGN KEY FK_97E64B134584665A');
        $this->addSql('ALTER TABLE rent_product DROP FOREIGN KEY FK_32667FDD4584665A');
        $this->addSql('ALTER TABLE contract DROP FOREIGN KEY FK_E98F2859E5FD6250');
        $this->addSql('ALTER TABLE rent_product DROP FOREIGN KEY FK_32667FDDE5FD6250');
        $this->addSql('ALTER TABLE tag_type_limitation_investment DROP FOREIGN KEY FK_D05FA00060033EEB');
        $this->addSql('ALTER TABLE tag_type_limitation_company DROP FOREIGN KEY FK_B1F1C9F460033EEB');
        $this->addSql('ALTER TABLE category_tag_type_limitation DROP FOREIGN KEY FK_45F44AB860033EEB');
        $this->addSql('ALTER TABLE product_tag_type_limitation DROP FOREIGN KEY FK_97E64B1360033EEB');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD128E4554');
        $this->addSql('ALTER TABLE rent DROP FOREIGN KEY FK_2784DCC2298D193');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD5D83CC1');
        $this->addSql('ALTER TABLE opening_time DROP FOREIGN KEY FK_89115E6EA4D0E58D');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADA4D0E58D');
        $this->addSql('ALTER TABLE role_stockpile_company DROP FOREIGN KEY FK_36662B5DA4D0E58D');
        $this->addSql('ALTER TABLE role_stockpile_user DROP FOREIGN KEY FK_D33AA7C8A4D0E58D');
        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C74A76ED395');
        $this->addSql('ALTER TABLE phone DROP FOREIGN KEY FK_444F97DDA76ED395');
        $this->addSql('ALTER TABLE role_company_user DROP FOREIGN KEY FK_5AA8A07BA76ED395');
        $this->addSql('ALTER TABLE role_event_user DROP FOREIGN KEY FK_1D6CF0CEA76ED395');
        $this->addSql('ALTER TABLE role_stockpile_user DROP FOREIGN KEY FK_D33AA7C8A76ED395');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE category_tag_type_limitation');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE contract');
        $this->addSql('DROP TABLE email');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE img_avatar');
        $this->addSql('DROP TABLE img_banner');
        $this->addSql('DROP TABLE img_category');
        $this->addSql('DROP TABLE img_logo');
        $this->addSql('DROP TABLE img_product');
        $this->addSql('DROP TABLE invest_model');
        $this->addSql('DROP TABLE investment');
        $this->addSql('DROP TABLE line');
        $this->addSql('DROP TABLE opening_time');
        $this->addSql('DROP TABLE pack_relationship');
        $this->addSql('DROP TABLE phone');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE product_product_image');
        $this->addSql('DROP TABLE product_tag_type_limitation');
        $this->addSql('DROP TABLE rent');
        $this->addSql('DROP TABLE rent_product');
        $this->addSql('DROP TABLE role_company_user');
        $this->addSql('DROP TABLE role_event_company');
        $this->addSql('DROP TABLE role_event_user');
        $this->addSql('DROP TABLE role_investment_company');
        $this->addSql('DROP TABLE role_stockpile_company');
        $this->addSql('DROP TABLE role_stockpile_user');
        $this->addSql('DROP TABLE setting_company_event_role');
        $this->addSql('DROP TABLE setting_company_investment_role');
        $this->addSql('DROP TABLE setting_company_stockpile_role');
        $this->addSql('DROP TABLE setting_company_user_role');
        $this->addSql('DROP TABLE setting_event_user_role');
        $this->addSql('DROP TABLE setting_invest_tag_type_limitation');
        $this->addSql('DROP TABLE tag_type_limitation_investment');
        $this->addSql('DROP TABLE tag_type_limitation_company');
        $this->addSql('DROP TABLE setting_prod_tag_type_limitation');
        $this->addSql('DROP TABLE setting_time_coefficient');
        $this->addSql('DROP TABLE stage');
        $this->addSql('DROP TABLE state');
        $this->addSql('DROP TABLE stockpile');
        $this->addSql('DROP TABLE user');
    }
}
