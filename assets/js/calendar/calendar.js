import 'jquery';
import { Calendar } from '@fullcalendar/core';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import interactionPlugin from '@fullcalendar/interaction';

window.onload = () => {
    let calendarElt = document.querySelector("#calendar")
    let calendar = new Calendar(calendarElt, {
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        plugins: [ resourceTimelinePlugin, interactionPlugin ],
        initialView: 'resourceTimelineWeek',
        selectable: true,
        dragScroll: true,
        locale: 'fr',
        timeZone: 'Europe/Paris',
        //themeSystem: 'bootstrap',//TODO ne marche pas thème boostrsap le seul dispo pour le moment
        headerToolbar: {
            left: 'today prev,next',
            center: 'title',
            right: 'resourceTimelineDay,resourceTimelineTenDay,resourceTimelineMonth,resourceTimelineYear'
        },
        businessHours:
            {
                daysOfWeeek:[1,2,3,4,5],
                startTime: '09:00',
                startEnd: '17:30'
            },

        scrollTime: 6.00,
        editable: true,
        resourceAreaHeaderContent: 'Events',
        resources: $('#calendar').data('ressources'),
        events: $('#calendar').data('events'),
    })
    calendar.render();

};




