cd######### COMPOSER #########
composer-update:
	@echo ----------------- Running composer to update vendors ------------------
	php -d memory_limit=-1 /usr/local/bin/composer update --prefer-dist

######### FIXTURES #########
run-fixtures:
	@echo ----------------- Run fixtures ------------------
	php bin/console hautelook:fixtures:load -n

run-fixtures-xdebug:
	@echo ----------------- Run fixtures ------------------
	export XDEBUG_CONFIG="idekey=PHPSTORM" && php -d xdebug.remote_host=10.0.2.2 bin/console hautelook:fixtures:load -n

######### DATABASE #########
create-database:
	@echo ----------------- Drop/Create database ------------------
	php bin/console doctrine:database:drop --force && \
	php bin/console doctrine:database:create

make-migration:
	@echo ----------------- Make migration ------------------
	php bin/console make:migration

run-migration rerun-migration:
	@echo ----------------- Run Migration ------------------
	php bin/console doctrine:migrations:migrate -n --allow-no-migration

reset-database: create-database run-migration

make-run-migration: make-migration run-migration

run-migration-and-fixtures: run-migration run-fixtures

make-run-migration-and-fixtures: make-migration run-migration-and-fixtures

rerun-database: create-database run-migration run-fixtures

create-short-migration: create-database run-migration make-migration rerun-migration

######### PHP CS FIXER #########
run-php-cs-fixer:
	php-cs-fixer fix ./src --rules=@Symfony

######### TEST #########
run-test:
	@echo ----------------- Run test ------------------
	php ./bin/phpunit --testdox --testdox-html  ./tests/logs.html --testdox-text  ./tests/logs.txt
run-test-xdebug:
	@echo ----------------- Run test - With Xdebug ------------------
	export XDEBUG_CONFIG="idekey=PHPSTORM" && php -d xdebug.remote_host=10.0.2.2 ./bin/phpunit --testdox

rerun-database-and-test: rerun-database run-test