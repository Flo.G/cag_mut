#Translation du site
##Configuration
La configuration s'éxécute en trois étapes: 
    - Créer un variable global (dans le services.yaml->parameters) qui contiendra toutes les langues utilisées 
pour la traduction du site (ex: app.locales: [en, fr]).
```yaml
{
  app.locales: [en, fr]
}
```
   - Dans le fichier twig.yaml appelé cette variable global qui pourra ensuite être utilisé dans le twig.
```yaml
{
  globals:
    locales: '%app.locales%'
}
```
   - Dans le fichier translation.yaml régler le default_local sur la langue par défault de votre site, le défault_path 
défini votre chemin vers les fichiers de traduction, le fallbacks sur la langue de base du code.
```yaml
{
  default_locale: fr
    translator:
      default_path: '%kernel.project_dir%/translations'

      fallbacks:
        - en 
}
```

##Mise en place des fichiers de traduction
Nous travaillons avec les fichiers XLF. Chaque fichier messages XLF sont traduis pour une langue (en créer d'autre pour 
les autres langues).
Une fois la mise en place de la configuration faite nous utilisons la commande suivante
```bash
{ 
php bin/console translation:update --force fr
}
``` 
qui va générer plusieurs fichirers qui seront introduits dans le dossier,
à la racine du projet, translations. Dans ce dossier on va retrouver différents fichiers comme sécurity.fr.xlf 
dedans on va retrouver par exemple  tous les messages d'erreurs/confirmation lié au système d'email symfony. Le(s) 
fichier(s) qui va/vont nous intéressé est/sont messages+intl-icu.fr.xlf ou l'on va retrouver nos traductions dedans.
```xlf
{
<?xml version="1.0" encoding="utf-8"?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:1.2" version="1.2">
  <file source-language="fr" target-language="fr" datatype="plaintext" original="file.ext">
    <header>
      <tool tool-id="symfony" tool-name="Symfony"/>
    </header>
    <body>
      <trans-unit id="KSwG8AR" resname="Category">
        <source>Category</source>
        <target>Catégorie</target>
      </trans-unit>
    </body>
  </file>
</xliff>
}
```

On a plusieurs manières d'introduire de la traduction dedans:
Par twig: 
   - En mettant des balises 
```twig
{
{% trans %}Submit{% endtrans %}
}
``` 
entre chaques mots/phrases que l'on veut traduire puis de 
nouveau exécuter la commande décrite plus haut. Dans le fichier messages+intl-icu.fr.xlf va aparaitre votre mots/phrase 
il faudra donc changer la target (votre traduction).

   - Depuis les contrôler traduire les chaines de caractères avec cette méthode: 
```php
{
use Symfony\Contracts\Translation\TranslatorInterface;

public function index(TranslatorInterface $translator)
{
        $message = $trans->trans('Your email address has been verified.');
        $this->addFlash('success', $message);
}
}
```
### Changement d'affichage de langue 

Controller -> TranslationController;
EventSubscriber -> LocalSubscriber;

Puis dans le twig
Nous appellerons cette méthode depuis notre menu ou tout fichier Twig dans lequel nous donnerons le choix de la langue 
aux utilisateurs.

```twig
{
    {% if locale != app.request.locale %}
        <li class="nav-item">
            <a class="nav-link" href="{{ path('translation_locale', {'locale': locale}) }}"><img src ="{{asset('img/'~locale~'.png')}}" alt="{{ locale }}"></a>
        </li>
    {% endif %}
}
```