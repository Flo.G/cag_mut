### Hiérarchie
- roles user: "ROLE_SUPER_ADMIN" -> "ROLE_AFFILIATED" -> "ROLE_USER" -> "ROLE_PWD_CHANGE" -> "ROLE_LIMIT"user
  unnconnected
- roles user<-->Company: "ROLE_COMPANY_ADMIN" -> "ROLE_ADMIN_EVENTS"
- roles user<-->event<-->company: "ROLE_EVENT_ADMIN" -> "ROLE_STAGEMAN"
- roles Company<-->event: "ROLE_OWNER" -> "ROLE_VIEWER"
- roles Company<-->investment: "ROLE_INVEST_ADMIN" -> "ROLE_INVEST_OWNER" -> "ROLE_INVEST_VIEWER" (on peut etre admin
  sans sans etre owner)
- roles Company<-->stockpile: "TO_DEF" -> "TO_DEF"

Liste des rôles:

Les rôles du User:

- ROLE_AFFILIATED: User affilié à une structure ou à un event existant ou en créant sa propre structure
- ROLE_USER: Utilisateur inscrit sur l'appli sans affiliation (role par défault symfony)
- ROLE_SUPER_ADMIN: Role attribué aux administrateur du site
  A voir Service-> SecurityManager:
- ROLE_PWD_CHANGE: attribuer au moment du change de mot de passe (temporaire)
- ROLE_LIMIT: Role attribuer quand demande de MDP non finalisé et quand User non vérifié 

Table de jointure entre le User et la Company:
- ROLE_COMPANY_ADMIN: est le user qui à créer la structure 
- ROLE_ADMIN_EVENTS: User qui peut créer des events et attribuer des personnes (stageman) à ces events 

Table de jointure entre le User et l'Event:
ROLE_STAGEMAN: est le User affilié  à un event et peu emprunter du matériel 
ROLE_EVENT_ADMIN: Peut intégeré des stageman 

Table de jointure entre la Company et l'Event
ROLE_OWNER: Compagny organisatrice de l'event
ROLE_VIEWVER: Personne qui peut voir les events 

Table de jointure Company et Investment
ROLE_OWNER: Company qui propose des investissements
ROLE_INVESTOR: Accorder au company qui investissent dans des investissement

Table de jointure Company et Stockpile: 
ROLE_STOCKING: 