# Projet de recontruction du site Cagibig

## Documentation
###Attention
Les commandes affichés dans cette doc peuvent excuter de manière différentes à voir sur internet selon ton 
système d'exploitation et/ou ton installation de base de ton environnement de travail.

###Installation de la machine virtuelle
Se référer à la doc dans repo gitlab de la plateforme (Cagibig -> Infra -> vagrant)

### Installation de php 7.4:
On a fait le choix de partir sur une version récente de Php pas la plus récente 
(pour des soucis de compatibilité avec les bundles utilisés, avec php8).
Commande:
    - sudo add-apt-repository ppa:ondrej/php
    - sudo apt update
    - sudo apt-get install php 7.4
    - sudo apt install php7.4-cli php7.4-json php7.4-pdo php7.4-mysql php7.4-zip php7.4-gd php7.4-mbstring php7.4-curl php7.4-xml
        php7.4-bcmath php7.4-json

### Installation de Composer:
curl https://getcomposer.org/composer.phar --output composer.phar

### Installation de MariaDB:
￼sudo apt install mariadb-server

### Configuration de Mariadb
https://www.digitalocean.com/community/tutorials/how-to-install-mariadb-on-ubuntu-20-04-quickstart-fr

###Projet avec Symfony 5.1
Nous avons utilisé Symfony 5.1 au départ, une update vers la 5.2 
composer create-project symfony/website-skeleton my_project_name (Doctrine inclut avec cette commande)
Puis update 5.2
En suivant la doc Symfony pour update -> on change manuelle, dans le composer.json, la version des bundle symfony ("^5.1"->"^5.2")
ensuite en exécutant Composer install, il existe d'autre manière de faire.

### Installation des différents Bundle or ceux installer par Symfony
Les bundles utilisés dans le développement:
EasyAdmin:
    - easycorp/easyadmin-bundle (voir doc sur ce bundle),
Connexion via les réseaux sociaux
    - league/oauth2-facebook,
    - league/oauth2-google,
Redimmenssion, uplaod, rename des Images: (voir la doc sur ces bundles)
    - liip/imagine-bundle,
    - vich/uploader-bundle,
Pour la création des creat_at/update_at dans les entity(typage):
https://carbon.nesbot.com/symfony/
    - nesbot/carbon
Pour l'ajout de numéro de tel avec différent format:
    - odolbeau/phone-number-bundle
Pour la géoloc (option futur):
    - willdurand/geocoder-bundle
Pour le architectures (ex: catégorie, sous catégorie):
    - gedmo/doctrine-extensions
Pour la créations des fixtures:
    - hautelook/alice-bundle
Pour la création des big (pas utiliser pour le moment):
    - moneyphp/money
    - tbbc/money-bundle : https://packagist.org/packages/tbbc/money-bundle
JS nous utilison jQuery:
Pour la search bar en js:
    -select2/select2