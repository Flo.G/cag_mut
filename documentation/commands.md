###Regenerate
Pour modifications des entity ou remattre à niveau:
- bin/console make:entity --regenerate

### Création du login form
- bin/console m:auth (nommer LoginFormAuthenticator)
- paramétrage https://symfony.com/doc/current/security/form_login_setup.html
- Put the remember_me check_box : https://symfony.com/doc/current/security/remember_me.html You want to force user
  re-authenticate before accessing certain resources

###Database
Pour creér database:
- bin/console do:da:cre
- bin/console ma:mi
- bin/console do:mi:mi 
  -bin/console d:m:m || bin/console d:s:u --force //(update database)

### Commandes pour debug
- bin/console debug:router (debug routing)
- bin/console doctrine:schema:validate (pour debugger le mappage-entity-relation)
- bin/console debug:container (Affiche liste de tout les services charger)
- php bin/console debug:validator 'App\Entity\SomeClass' -> affiche la liste des asserts.
- php bin/console debug:container --parameters -> tous les param accessible par l'apli
- php bin/console doctrine:schema:update --dump-sql

### Use js in assets
Stop and restart encore each time you update your webpack.config.js file.(npm run watch do it)
- npm run dev
- npm run watch
- npm run build

--------------------security-systeme--------------------------
- Put the remember me bazar : https://symfony.com/doc/current/security/remember_me.html
- Create TokenAuthenticator.php in security directory (permet point d'entré connection API)






