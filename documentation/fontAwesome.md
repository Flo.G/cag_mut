Company dashboard

- 'Dashboard', 'fab fa-cotton-bureau'

- 'Events', 'fa fa-calendar'
- 'Add Events', 'fas fa-calendar-plus'
- 'Show Events', 'fas fa-calendar-week'

- 'Users', 'fa fa-users')
- 'Affiliat Users', 'fas fa-user-plus'
- 'Affiliated Users', 'fa fa-user-friends'
- 'UnAffiliat Users', 'fa fa-user-minus'

SuperAdmin dashboard

- 'Dashboard', 'fab fa-cotton-bureau'

- 'Events', 'fa fa-calendar'
- 'Add Events', 'fas fa-calendar-plus'
- 'Show Events', 'fas fa-calendar-week'

- 'Users', 'fa fa-users')
- 'Affiliat Users', 'fas fa-user-plus'
- 'UnAffiliat Users', 'fa fa-user-minus'
- 'Show Users detail', 'fas fa-address-card'
- 'Block User', 'fas fa-user-lock'
