<?php

namespace App\Form\BaseType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class VichAddImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('imageFile', VichImageType::class, [
            'required' => true,
            'allow_delete' => false,
            'delete_label' => 'Remove file',
            'download_uri' => false,
            /*'download_label' => 'Download file',*/
            'imagine_pattern' => $options['data']::FILTER,
            'asset_helper' => false,
            'label'=>false,
            'attr'=>[
                'onChange'=>'img_pathUrl(this);'
            ]
            //            'storage_resolve_method' => VichImageType::STORAGE_RESOLVE_PATH_ABSOLUTE,
        ]);
    }
}
