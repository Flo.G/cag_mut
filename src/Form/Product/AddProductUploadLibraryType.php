<?php

namespace App\Form\Product;

use App\Entity\File\Image\ProductImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class AddProductUploadLibraryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $image = $options['data']['image'];

        $builder
            ->add('choice', ChoiceType::class, [
            'label' => 'Make your choices ?',
            'multiple' => true,
            'required' => true,
            'expanded' => true,
            'choices' => $image,
            'label_html' => true,
            'choice_label' => function (?ProductImage $image) {
                $imagePath = 'empty';
                if (!is_null($image->getProductImages())) {
                    $imagePath = $image->getFilePath().'/'.$image->getImageName();
                    $imageName = $image->getImageName();
                }

                return strtoupper($imageName).'<img src="'.$imagePath.'">';
            },
        ])
            ->add('Submit', SubmitType::class)
        ;
    }
}
