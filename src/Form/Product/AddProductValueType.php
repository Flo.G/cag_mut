<?php

namespace App\Form\Product;

use App\Entity\Product\Line;
use App\Entity\Product\State;
use Money\Money;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Positive;
use Tbbc\MoneyBundle\Form\Type\MoneyType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AddProductValueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('depositValue', MoneyType::class, [
                'attr'=>['class'=> 'input-bot-border'],
                'required' => true,
                'data' => Money::EUR(1000), //EUR 10
                'amount_options' => [
                    'label' => 'Amount',
                ],
                'currency_options' => [
                    'label' => 'Currency',
                ],

            ])
            ->add('line', Select2EntityType::class, [
                'required' => true,
                'multiple' => false,
                'remote_route' => 'api_line_autocomplete',
                'class' => Line::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'label' => 'line',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'Choose line',
                'attr' => [
                    'class' => 'js-select2',
                ]
            ])
            ->add('state', Select2EntityType::class, [
                'required' => true,
                'multiple' => false,
                'remote_route' => 'api_state_autocomplete',
                'class' => State::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'label' => 'state',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'state',
                'attr' => [
                    'class' => 'js-select2',
                ],
            ])
        ;
    }
}
