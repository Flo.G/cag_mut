<?php

namespace App\Form\Product;

use App\Entity\Stockpile\Stockpile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class ProductChoiceStockpileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $stockpiles = $options['data'];

        $builder
            ->add('stockpile', ChoiceType::class, [
                'choices' => $stockpiles,
                //'empty_data' => $stockpiles[0],
                'label' => 'Stockpile',
                'label_attr'=>['class'=>'small-label row text-center'],
                'attr'=>['class'=> 'inputTransparant'],
                'choice_label' => function (?Stockpile $stockpile) {
                    return $stockpile ? strtoupper($stockpile->getName().' '.$stockpile->getCity().' '.$stockpile->getAddress()) : '';
                },
            ])
        ;
    }
}
