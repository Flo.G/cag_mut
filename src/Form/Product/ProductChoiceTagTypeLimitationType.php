<?php

namespace App\Form\Product;

use App\Entity\Product\TagTypeLimitation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

//TODO: add new tag
class ProductChoiceTagTypeLimitationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $propositions = $options['data'];

        $builder
            ->add('tagTypeLimitations', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'api_tag_type_autocomplete',
                'class' => TagTypeLimitation::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'label' => 'Tag Type Limitations',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'Choose limitations ',
                'attr' => [
                    'class' => 'js-select2 ',
                ],
                'label_attr'=>['class' =>'smallLabel']
            ])
            ->add('tagTypePropositions', ChoiceType::class, [
                'multiple' => true,
                'expanded' => true,
                'choices' => $propositions,
                'label_html' => true,
                'attr' => [
                    'class' => 'hiddenCheckBox text-center small' ,
                    'hidden'
                ],
                'label_attr'=>['class' =>'smallLabel'],
                'choice_label' => function (?TagTypeLimitation $tagTypeLimitation) {
                    if (!is_null($tagTypeLimitation->getDescription())) {
                        return sprintf('<div class="dropdown">
                                                    <div id="%s" class="drop_button bg-uncheck">%s</div>
                                                    <div class="dropdown-content help_over">
                                                        <h5 class="">%s</h5>
                                                        <p class="">%s</p>
                                                    </div>
                                                </div>',
                            strtoupper($tagTypeLimitation->getSlugProductLimitation()),
                            strtoupper($tagTypeLimitation->getName()),
                            strtoupper($tagTypeLimitation->getName()),
                            strtolower($tagTypeLimitation->getDescription())
                        );
                    }

                    return $tagTypeLimitation ? strtoupper($tagTypeLimitation->getName()) : '';
                },
            ])
        ;
    }
}
