<?php

namespace App\Form\Product;

use App\Cagibig\MetricBundle\Form\Type\ElectricCurrentType;
use App\Cagibig\MetricBundle\Form\Type\EnergyType;
use App\Cagibig\MetricBundle\Form\Type\LengthType;
use App\Cagibig\MetricBundle\Form\Type\MassType;
use App\Cagibig\MetricBundle\Form\Type\VolumeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Positive;

class AddProductDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('weight', MassType::class, [
                'mapped' => true,
                'required' => true,
                'attr'=>['class'=>'carct-prod']
            ])
            ->add('length', LengthType::class, [
                'mapped' => true,
                'required' => true,
                'attr'=>['class'=>'carct-prod']

            ])
            ->add('height', LengthType::class, [
                'mapped' => true,
                'required' => true,
                'attr'=>['class'=>'carct-prod']

            ])
            ->add('width', LengthType::class, [
                'mapped' => true,
                'required' => true,
                'attr'=>['class'=>'carct-prod']

            ])
            ->add('transportVolume', VolumeType::class, [
                'mapped' => true,
                'required' => true,
                'attr'=>['class'=>'carct-prod']

            ])
            ->add('preparedBy', IntegerType::class, [
                'mapped' => true,
                'required' => true,
                'constraints' => [new Positive()],
                'attr' => [
                    'min' => 1,
                    'class'=>'carct-prod'
                ],
                'data' => 1,

            ])
            ->add('current', ElectricCurrentType::class, [
                'mapped' => true,
                'required' => true,
                'attr'=>['class'=>'carct-prod']

            ])
            ->add('power', EnergyType::class, [
                'mapped' => true,
                'required' => true,
                'attr'=>['class'=>'carct-prod']

            ])
            ->add('surfaceMaterial', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false,
                ],
                'data' => true,
                'attr'=>['class'=>'carct-prod']

            ])
        ;
    }
}
