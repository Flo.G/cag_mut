<?php

namespace App\Form\Product;

use App\Entity\Setting\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Positive;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class AddProductNameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, [
                'label_attr'=>['class'=>'small-label row text-center'],
                'attr'=>['class'=> 'input-bot-border', 'placeholder'=>'Name']
            ])
            ->add('quantity', IntegerType::class, [
                'mapped' => true,
                'required' => true,
                'constraints' => [new Positive()],
                'attr' => [
                    'min' => 1,
                    'class'=> 'input-bot-border',
                     'placeholder'=>'Quantity'
                ],
                'label_attr'=>['class'=>'small-labelF row text-center'],
            ])
            ->add('description',TextType::class, [
                'label_attr'=>['class'=>'small-label row text-center'],
                'attr'=>['class'=> 'input-bot-border', 'placeholder'=>'Description']
            ])
            ->add('categories', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'api_categories_autocomplete',
                'class' => Category::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'label' => 'name',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'label_attr'=>['class'=>'small-label row text-center'],
                'placeholder' => 'Choose categories',
                'attr' => [
                    'class' => 'js-select2 input-bot-border',
                ],
            ])
        ;
    }
}
