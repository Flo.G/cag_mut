<?php

namespace App\Form\DataTransformer;

use App\Entity\Company\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CompanyToStringTransformer implements DataTransformerInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Transforms an object (Company) to a string.
     *
     * @param Company|null $company
     */
    public function transform($company): string
    {
        if (null === $company) {
            return '';
        }

        return $company->getSlug();
    }

    /**
     * Transforms a string to an object (Company).
     *
     * @param string $slug
     *
     * @throws TransformationFailedException if object (Company) is not found
     */
    public function reverseTransform($slug): ?Company
    {
        // empty Company? It's optional, so that's ok
        if (!$slug) {
            return null;
        }

        $company = $this->entityManager
            ->getRepository(Company::class)
            ->findBy(['slug' => $slug]);

        if (null === $company) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf('The company "%s" cannot be found!', $slug));
        }

        return $company;
    }
}
