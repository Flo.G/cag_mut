<?php

namespace App\Form\Event;

use App\Entity\Event\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $event = $options['data']['eventsLinked'];
        $builder
            ->add('name')
            ->add('description', TextType::class, [
                'required' => false,
            ])
            ->add('dateStart', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
            ])
            ->add('dateEnd', DateTimeType::class, [
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
            ])
            ->add('budget')
        ;
        if (isset($event) && !empty($event)) {
            $builder->add('event', ChoiceType::class, [
                'label' => 'Is he part of another event ?',
                'required' => false,
                'expanded' => false,
                'choices' => $event,
                'choice_label' => function (?Event $event) {
                    $logo = 'empty';
                    if (!is_null($event->getLogo())) {
                        $logo = $event->getLogo()->getFilePath().'/'.$event->getLogo()->getImageName();
                    }

                    return strtoupper($event->getName()).'<img src="'.$logo.'">';
                },
            ]);
        }
    }
}
