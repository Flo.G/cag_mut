<?php


namespace App\Form\Setting;



use App\Entity\File\Image\CategoryImage;
use App\Entity\Setting\Category;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null, [
                'label'=>'Name '
            ])
            ->add('parent', EntityType::class, [
                'class' => Category::class,
                'choice_label' => ' name ',
                'placeholder' => ' Choose one category parent',
                'required'=>false
            ])

            /*->add('image', EntityType::class, [
                'class' => CategoryImage::class,
                'choice_label' => 'imageName',
                'required' => false,
                'attr'=>[
                    'class' => ' collapse chooseImage',
                ]
            ])*/
        ;

    }
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}