<?php


namespace App\Form\Setting;

use App\Entity\File\Image\CategoryImage;
use App\Entity\Setting\Category;
use App\Form\BaseType\VichAddImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CategoryImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $image = new CategoryImage();
        $builder
            ->add( 'imageName')
            /*->add('imageFile', CollectionType::class, [
                'entry_type' => VichAddImageType::class,
            ] )*/
            ->add('alt');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CategoryImage::class,
        ]);
    }
}