<?php

namespace App\Form\Registration\User;

use App\Entity\User\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NicknameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'First name*',
                'label_attr'=>['class'=>'small-label'],
                'attr'=>['class'=>'input-bot-border']
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Last name*',
                'label_attr'=>['class'=>'small-label'],
                'attr'=>['class'=>'input-bot-border'],
                ])
            ->add('nickname', TextType::class, [
                'required' => false,
                'label' => 'Surname',
                'label_attr'=>['class'=>'small-label'],
                'attr'=>['class'=>'input-bot-border', 'placeholder'=>'not required']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
