<?php

namespace App\Form\Registration\User;

use App\Entity\Company\Company;
use App\Entity\Event\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class SearchCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', Select2EntityType::class, [
                'multiple' => false,
                'remote_route' => 'api_company_autocomplete',
                'class' => Company::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'label' => 'Company',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'Search a Company',
                'attr' => [
                    'class' => 'js-select2',
                ],
            ])
            ->add('eventSlug', Select2EntityType::class, [
                'multiple' => false,
                'remote_route' => 'api_event_autocomplete',
                'class' => Event::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'label' => 'Event',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'Search an Event',
                'attr' => [
                    'class' => 'js-select2',
                ],
            ])
            ->add('Submit', SubmitType::class)
        ;
    }
}
