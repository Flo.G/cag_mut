<?php

namespace App\Form\Registration\User;

use App\Entity\Setting\Role\CompanyUserRole;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class LinkCompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'api_company_user_role_autocomplete',
                'class' => CompanyUserRole::class,
                'primary_key' => 'id',
                'text_property' => 'label',
                'label' => 'Role',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'Give your role',
                'attr' => [
                    'class' => 'js-select2',
                ],
            ])
            ->add('Submit', SubmitType::class)
        ;
    }
}
