<?php

namespace App\Form\Registration\User;

use App\Entity\Company\Company;
use App\Entity\Setting\Role\EventUserRole;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class LinkEventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $company = $options['data']['company'];

        $builder
            ->add('name', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'api_event_user_role_autocomplete',
                'class' => EventUserRole::class,
                'primary_key' => 'id',
                'text_property' => 'label',
                'label' => 'Role',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'Give your role',
                'attr' => [
                    'class' => 'js-select2',
                ],
            ])
            ->add('company', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => true,
                'choices' => $company,
                'label_html' => true,
                //'choice_value' => 'name',
                'choice_label' => function (?Company $company) {
                    $logo = 'empty';
                    if (!is_null($company->getLogo())) {
                        $logo = $company->getLogo()->getFilePath().'/'.$company->getLogo()->getImageName();
                    }

                    return strtoupper($company->getName()).' with matriculation number '.$company->getSiret().' address in '.$company->getCity().'<img src="'.$logo.'">';
                },
            ])
            ->add('Submit', SubmitType::class)
        ;
    }
}
