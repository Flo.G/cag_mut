<?php

namespace App\Form\Registration\User;

use App\Entity\User\User;
use App\Form\BaseType\AddPhoneType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('address', TextType::class, [
                'label' => 'Address',
                'label_attr'=>['class'=>'small-label'],
                'attr'=>['class'=> 'input-bot-border'],
                ])
            ->add('city', TextType::class, [
                'label' => 'City',
                'label_attr'=>['class'=>'small-label'],
                'attr'=>['class'=> 'input-bot-border'],
                ])
            ->add('postalCode', TextType::class, [
                'label' => 'PostalCode',
                'label_attr'=>['class'=>'small-label'],
                'attr'=>['class'=> 'input-bot-border'],
                ])
            ->add('phones', CollectionType::class, [
                    'entry_type' => AddPhoneType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label'=>false,
                    'label_attr'=>['class'=>'small-label'],
                    'attr'=>['class'=> 'input-bot-border'],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
