<?php

namespace App\Form\Registration\User;

use App\Entity\User\User;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => false,
                'attr'=>['placeholder'=>'mail','class'=>'form-register pl-3 mt-2']
                ])
            ->add('firstName', TextType::class, [
                'label' => false,
                 'attr'=>['placeholder'=>'first name','class'=>'form-register pl-3 mt-2']
            ])

            ->add('lastName', TextType::class, [
                'label' => false,
                'attr'=>['placeholder'=>'last name','class'=>'form-register pl-3 mt-2']
            ])/*
            ->add('nickname')
            ->add('adress')
            ->add('city')
            ->add('postalCode')
            ->add('telNumber', PhoneNumberType::class, ['widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE, 'country_choices' => ['FR', 'US', 'IT', 'GB'], 'preferred_country_choices' => ['FR', 'GB'], 'required' => false])
            ->add('proTelNumber', PhoneNumberType::class, ['widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE, 'country_choices' => ['FR', 'US', 'IT', 'GB', 'ES', 'P'], 'preferred_country_choices' => ['FR', 'GB'], 'required' => false])
           */ ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]), ],
                'invalid_message' => 'Passwords do not match.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options' => ['label' => false,'attr' => ['placeholder'=>'password','class'=>'form-register pl-3 mt-2']],
                'second_options' => ['label' => false,'attr' => ['placeholder'=>'confirm password','class'=>'form-register pl-3 mt-2']],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
