<?php

namespace App\Form\Registration\Company;

use App\Entity\Company\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class VerifyMailFormType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \App\Entity\User\User $user */
        $user = $this->security->getUser();
        $userMail = $user->getEmail();
        $userId = $user->getId();

        $builder
            ->add('email', TextType::class, [
                'data' => $userMail,
            ])
            ->add('name', TextType::class, [
                'label' => 'Name',
                'disabled' => true,
                ])
            ->add('siret', TextType::class, [
                'label' => 'Siret or RNA',
                'disabled' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
