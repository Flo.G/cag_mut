<?php

namespace App\Form\Registration\Company;

use App\Entity\Company\Company;
use App\Entity\Event\Event;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class AlreadySiretType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $company = $options['data']['company'];
        $event = $options['data']['event'];

        $builder
            ->add('company', ChoiceType::class, [
                'required' => false,
                'expanded' => true,
                'choices' => $company,
                'label_html' => true,
                'choice_label' => function (?Company $company) {
                    $logo = 'empty';
                    if (!is_null($company->getLogo())) {
                        $logo = $company->getLogo()->getFilePath().'/'.$company->getLogo()->getImageName();
                    }

                    return strtoupper($company->getName()).' with matriculation number '.$company->getSiret().' address in '.$company->getCity().'<img src="'.$logo.'">';
                },
            ])
            ->add('event', ChoiceType::class, [
                'required' => false,
                'expanded' => true,
                'choices' => $event,
                'choice_label' => function (?Event $event) {
                    $logo = 'empty';
                    if (!is_null($event->getLogo())) {
                        $logo = $event->getLogo()->getFilePath().'/'.$event->getLogo()->getImageName();
                    }

                    return strtoupper($event->getName()).'<img src="'.$logo.'">';
                },
            ])

            ->add('Submit', SubmitType::class)
        ;
    }
}
