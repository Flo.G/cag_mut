<?php

namespace App\Form\Registration\Company;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Security;

class CompanyRegistrationFormType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        /** @var \App\Entity\User\User $user */
        //       $user = $this->security->getUser();
//        $userMail = $user->getEmail();
        //       $userId = $user->getId();

        $builder
            ->add('name', TextType::class, [
                'label' => 'Name', ])
            ->add('siret', TextType::class, [
                'label' => 'Siret or RNA',
            ]);
    }
}
