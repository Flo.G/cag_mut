<?php

namespace App\Form\Registration\Company;

use App\Entity\Company\Company;
use App\Form\BaseType\AddPhoneType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddAddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Association loi 1901' => 'association loi 1901',
                    'Other' => 'other',
                ],
            ])
            ->add('address', TextType::class, [
                'label' => 'Address', ])
            ->add('city', TextType::class, [
                'label' => 'City', ])
            ->add('postalCode', TextType::class, [
                'label' => 'PostalCode', ])
            ->add('phones', CollectionType::class, [
                    'entry_type' => AddPhoneType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                ]
            );

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
        ]);
    }
}
