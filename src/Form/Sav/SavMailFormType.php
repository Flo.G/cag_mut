<?php


namespace App\Form\Sav;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SavMailFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

//        foreach ($options['data'] as $data){
//            if ($data){
                $builder
                    ->add('mail', TextType::class,[
                        'required' => true,
                    ])
                    ->add('subject', TextType::class, [
                        'required' => true,
                    ])
                    ->add('content', TextareaType::class, [
                        'required' => true,
                    ]);
            }

//        else{
//        $builder
//            ->add('mail', TextType::class,[
//                'data' => $options['data']['mail'],
//            ])
//            ->add('subject', TextType::class, [
//                'data' => $options['data']['subject'],
//                ])
//            ->add('content', TextareaType::class, [
//                'required' => true,
//                ]);
//            }
//        }
//     }
}