<?php

namespace App\Form\Stockpile;

use App\Entity\Stockpile\Stockpile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddStockpileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('city')
            ->add('postalCode')
            ->add('contractContent')
            ->add('stockCommission')
            ->add('maintenanceCommission')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stockpile::class,
        ]);
    }
}
