<?php

namespace App\Form\Investment;

use App\Entity\Investment\TagTypeLimitation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class InvestmentChoiceTagTypeLimitationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tagTypeLimitations', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'api_tag_type_limitation_autocomplete',
                'class' => TagTypeLimitation::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'label' => 'Tag Type Limitations',
                'minimum_input_length' => 0,
                'page_limit' => 10,
                'allow_clear' => true,
                'language' => 'en',
                'placeholder' => 'Choose limitations',
                'attr' => [
                    'class' => 'js-select2',
                ],
            ])
        ;
    }
}
