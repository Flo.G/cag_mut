<?php

namespace App\Form\Investment;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChoiceInvestModelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //if(isset()){
            $builder
                ->add('your_investment', ChoiceType::class,
                array('choices'=>array(
                    'New open investment'=>'1',
                    'New close investment'=>'2'

                ),
                    'multiple'=>false,
                    'expanded'=>true
                ));

        //}
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
