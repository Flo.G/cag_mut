<?php

namespace App\Mail;

use App\Entity\Company\UserRole as CompUserRole;
use App\Entity\Event\UserRole as EventUserRole;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class UserToCompanyMail
{
    private VerifyEmailHelperInterface $verifyEmailHelper;
    private MailerInterface $mailer;
    private EntityManagerInterface $entityManager;

    public function __construct(VerifyEmailHelperInterface $helper, MailerInterface $mailer, EntityManagerInterface $manager)
    {
        $this->verifyEmailHelper = $helper;
        $this->mailer = $mailer;
        $this->entityManager = $manager;
    }

    public function askCompAffiliationMail(CompUserRole $userRole, TemplatedEmail $email): void
    {
        $context = $email->getContext();
        $context['user'] = $userRole->getUser()->getUsername();
        $context['company'] = $userRole->getCompany()->getName();
        $context['roles'] = implode(', ', $userRole->getRoles());

        $email->context($context);

        $this->mailer->send($email);
    }

    public function askEventAffiliationMail(EventUserRole $userRole, TemplatedEmail $email): void
    {
        $context = $email->getContext();
        $context['user'] = $userRole->getUser()->getUsername();
        $context['event'] = $userRole->getEvent()->getName();
        $context['company'] = $userRole->getCompany()->getName();
        $context['roles'] = implode(', ', $userRole->getRoles());

        $email->context($context);

        $this->mailer->send($email);
    }
}
