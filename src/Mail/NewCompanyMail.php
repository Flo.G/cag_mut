<?php

namespace App\Mail;

use App\Entity\Company\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class NewCompanyMail
{
    private $verifyEmailHelper;
    private $mailer;
    private $entityManager;

    public function __construct(VerifyEmailHelperInterface $helper, MailerInterface $mailer, EntityManagerInterface $manager)
    {
        $this->verifyEmailHelper = $helper;
        $this->mailer = $mailer;
        $this->entityManager = $manager;
    }

    //mail for SUPER_ADMIN
    public function newCompanyMail(string $verifyEmailRouteName, Company $company, TemplatedEmail $email): void
    {
        $context = $email->getContext();
        $context['content'] = $company->getName();
        $context['signedUrl'] = $verifyEmailRouteName;

        $email->context($context);

        $this->mailer->send($email);
    }

    //

    public function newmail(string $verifyEmailRouteName, TemplatedEmail $email): void
    {
        $context = $email->getContext();
        $context['signedUrl'] = $verifyEmailRouteName;

        $email->context($context);

        $this->mailer->send($email);
    }
    //
    //mail for COMPANY
    public function sendCompanyEmailConfirmation(string $verifyEmailRouteName, UserInterface $user, Company $company, TemplatedEmail $email): void
    {
        $context = $email->getContext();
        $context['content'] = $company->getName();
        $context['token'] = $company->getActivationToken();

        $email->context($context);

        $this->mailer->send($email);
    }

    /**
     * @throws VerifyEmailExceptionInterface
     *
     */
    public function handleCompanyEmailConfirmation(Request $request, UserInterface $user, Company $company): void
    {
        $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), $user->getId(), $user->getEmail());

        dd($request->getUri());
        /*        $company->setIsVerified(true);

                $this->entityManager->persist($company);
                $this->entityManager->flush();*/
    }
}
