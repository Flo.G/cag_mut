<?php

namespace App\Entity\Traits;

trait ClassName
{
    public function getClassName(): string
    {
        return get_class($this);
    }
}
