<?php

namespace App\Entity\Stockpile;

use App\Entity\Company\Company;
use App\Entity\User\User;
use App\Repository\Stockpile\UserRoleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRoleRepository::class)
 * @ORM\Table(name="role_stockpile_user")
 */
class UserRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="userStockpileRoles", fetch="EAGER")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stockpile\Stockpile", inversedBy="userRoles", fetch="EAGER")
     */
    private $stockpile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="userStockpileRoles", fetch="EAGER")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getStockpile(): ?Stockpile
    {
        return $this->stockpile;
    }

    public function setStockpile(?Stockpile $stockpile): self
    {
        $this->stockpile = $stockpile;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
