<?php

namespace App\Entity\Stockpile;

use App\Entity\Company\Company;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Stockpile\CompanyRoleRepository::class)
 * @ORM\Table(name="role_stockpile_company")
 */
class CompanyRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * roles : owner.
     *
     * @ORM\Column(type="json")
     * @Assert\Json()
     */
    private $roles = [];

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stockpile\Stockpile", inversedBy="stockpileCompanyRoles", fetch="EAGER")
     */
    private $stockpile;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="stockpileCompanyRoles")
     */
    private $company;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getStockpile(): ?Stockpile
    {
        return $this->stockpile;
    }

    public function setStockpile(?Stockpile $stockpile): self
    {
        $this->stockpile = $stockpile;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
