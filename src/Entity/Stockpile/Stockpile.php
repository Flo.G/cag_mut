<?php

namespace App\Entity\Stockpile;

use App\Entity\Company\Company;
use App\Entity\Company\UserRole;
use App\Entity\Product\Product;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Stockpile\StockpileRepository")
 */
class Stockpile
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     * @Assert\Length(max="40")
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @Gedmo\Slug (fields = {"name"})
     * @ORM\Column(type="string", length=40)
     */
    private $stockpileSlug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     * @Assert\Type("string")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\Length(max="60")
     * @Assert\Type("string")
     */
    private $city;

    /**
     * @ORM\Column(type="string")
     * @Assert\Type(type="string")
     */
    private $postalCode;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Type(type="string")
     */
    private $contractContent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $stockCommission;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $maintenanceCommission;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product\Product", mappedBy="stockpile")//TODO peut etre passer par une manytomany avec atribut
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stockpile\CompanyRole", mappedBy="stockpile")
     */
    private $stockpileCompanyRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stockpile\UserRole", mappedBy="stockpile", fetch="EAGER")
     */
    private $userRoles;

    /**
     * @ORM\OneToMany(targetEntity=OpeningTime::class, mappedBy="stockpile")
     */
    private $openingTimes;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->stockpileCompanyRoles = new ArrayCollection();
        $this->userRoles = new ArrayCollection();
        $this->openingTimes = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name.' '.$this->city.' '.$this->address;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStockpileSlug(): ?string
    {
        return $this->stockpileSlug;
    }

    public function setStockpileSlug(string $stockpileSlug): self
    {
        $this->stockpileSlug = $stockpileSlug;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getContractContent(): ?array
    {
        return $this->contractContent;
    }

    public function setContractContent(?array $contractContent): self
    {
        $this->contractContent = $contractContent;

        return $this;
    }

    public function getStockCommission(): ?int
    {
        return $this->stockCommission;
    }

    public function setStockCommission(?int $stockCommission): self
    {
        $this->stockCommission = $stockCommission;

        return $this;
    }

    public function getMaintenanceCommission(): ?int
    {
        return $this->maintenanceCommission;
    }

    public function setMaintenanceCommission(?int $maintenanceCommission): self
    {
        $this->maintenanceCommission = $maintenanceCommission;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setStockpile($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getStockpile() === $this) {
                $product->setStockpile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyRole[]
     */
    public function getStockpileCompanyRoles(): Collection
    {
        return $this->stockpileCompanyRoles;
    }

    public function addStockpileCompanyRole(CompanyRole $stockpileCompanyRole): self
    {
        if (!$this->stockpileCompanyRoles->contains($stockpileCompanyRole)) {
            $this->stockpileCompanyRoles[] = $stockpileCompanyRole;
            $stockpileCompanyRole->setStockpile($this);
        }

        return $this;
    }

    public function removeStockpileCompanyRole(CompanyRole $stockpileCompanyRole): self
    {
        if ($this->stockpileCompanyRoles->removeElement($stockpileCompanyRole)) {
            // set the owning side to null (unless already changed)
            if ($stockpileCompanyRole->getStockpile() === $this) {
                $stockpileCompanyRole->setStockpile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserRole[]
     */
    public function getUserRoles(): Collection
    {
        return $this->userRoles;
    }

    public function addUserRole(UserRole $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
            $userRole->setStockpile($this);
        }

        return $this;
    }

    public function removeUserRole(UserRole $userRole): self
    {
        if ($this->userRoles->removeElement($userRole)) {
            // set the owning side to null (unless already changed)
            if ($userRole->getStockpile() === $this) {
                $userRole->setStockpile(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OpeningTime[]
     */
    public function getOpeningTimes(): Collection
    {
        return $this->openingTimes;
    }

    public function addOpeningTime(OpeningTime $openingTime): self
    {
        if (!$this->openingTimes->contains($openingTime)) {
            $this->openingTimes[] = $openingTime;
            $openingTime->setStockpile($this);
        }

        return $this;
    }

    public function removeOpeningTime(OpeningTime $openingTime): self
    {
        if ($this->openingTimes->removeElement($openingTime)) {
            // set the owning side to null (unless already changed)
            if ($openingTime->getStockpile() === $this) {
                $openingTime->setStockpile(null);
            }
        }

        return $this;
    }

}

