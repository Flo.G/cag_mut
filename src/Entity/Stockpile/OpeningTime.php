<?php

namespace App\Entity\Stockpile;

use App\Enum\OpeningDay\OpeningDay;
use Doctrine\ORM\Mapping as ORM;
use Elao\Enum\Bridge\Symfony\Validator\Constraint\Enum;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Stockpile\OpeningTimeRepository")
 */
class OpeningTime
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @ORM\Column(type="openingDay")
     * @Enum(class="App\Enum\OpeningDay\OpeningDay")
     * @Assert\NotBlank
     */
    private $days;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Assert\NotBlank
     * @Assert\Time()
     */
    private $startTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     * @Assert\NotBlank
     * @Assert\Time()
     */
    private $endTime;

    /**
     * @ORM\ManyToOne(targetEntity=Stockpile::class, inversedBy="OpeningTimes")
     */
    private $stockpile;

    /**
     * Get id.
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function setDays(?OpeningDay $days): self
    {
        $this->days = $days;

        return $this;
    }

    public function getDays(): ?OpeningDay
    {
        return $this->days;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getStockpile(): ?Stockpile
    {
        return $this->stockpile;
    }

    public function setStockpile(?Stockpile $stockpile): self
    {
        $this->stockpile = $stockpile;

        return $this;
    }
}
