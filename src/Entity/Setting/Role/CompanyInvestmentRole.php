<?php

namespace App\Entity\Setting\Role;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Setting\CompanyInvestmentRoleRepository")
 * @ORM\Table(name="setting_company_investment_role")
 */
class CompanyInvestmentRole extends AbstractRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
