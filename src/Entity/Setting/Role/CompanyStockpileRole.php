<?php

namespace App\Entity\Setting\Role;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Setting\CompanyStockpileRoleRepository")
 * @ORM\Table(name="setting_company_stockpile_role")
 */
class CompanyStockpileRole extends AbstractRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
