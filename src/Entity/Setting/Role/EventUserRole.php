<?php

namespace App\Entity\Setting\Role;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Setting\EventUserRoleRepository")
 * @ORM\Table(name="setting_event_user_role")
 */
class EventUserRole extends AbstractRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
