<?php

namespace App\Entity\Setting\Role;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Setting\CompanyUserRoleRepository")
 * @ORM\Table(name="setting_company_user_role")
 */
class CompanyUserRole extends AbstractRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
