<?php

namespace App\Entity\Setting;

use App\Entity\Product\Product;
use App\Repository\Setting\TimeCoefficientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TimeCoefficientRepository::class)
 * @ORM\Table(name="setting_time_coefficient")
 */
class TimeCoefficient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="array")
     * LINEAR => $dayStart, $coefX
     * LOGARITHM => $dayStart, $coefX , $coefY
     * STEPPED => $dayStart, $coefX , $step
     */
    private $data = [];

    /**
     * @ORM\Column(type="coefficientCalcul")
     */
    private $calculFunction;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="timeCoefficient")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getCalculFunction()
    {
        return $this->calculFunction;
    }

    public function setCalculFunction($calculFunction): self
    {
        $this->calculFunction = $calculFunction;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setTimeCoefficient($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getTimeCoefficient() === $this) {
                $product->setTimeCoefficient(null);
            }
        }

        return $this;
    }
}
