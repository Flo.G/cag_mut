<?php

namespace App\Entity\Setting;

use App\Entity\File\Image\CategoryImage;
use App\Entity\Product\Product;
use App\Entity\Product\TagTypeLimitation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Setting\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\Length(max="50")
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @Gedmo\Slug (fields= {"name"})
     * @ORM\Column(type="string", length=50)
     */
    private $categorySlug;

    //-----connect-construct-------------------------------------------------

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="App\Entity\Setting\Category", inversedBy="children", fetch="EAGER")
     * @ORM\JoinColumn (nullable=true, referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Setting\Category", mappedBy="parent", fetch="EAGER")
     * @ORM\OrderBy({"name"="ASC"})
     */
    private $children;

    /**
     * @ORM\OneToOne (targetEntity="App\Entity\File\Image\CategoryImage", inversedBy="categories",cascade={"remove"}).
     */
    private $image;

    /**
     * @ORM\ManyToMany (targetEntity="App\Entity\Product\Product", mappedBy="categories").
     */
    private $products;

    /**
     * @ORM\ManyToMany (targetEntity="App\Entity\Product\TagTypeLimitation", inversedBy="categories").
     */
    private $productTagTypeLimitations;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->productTagTypeLimitations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getCategorySlug()
    {
        return $this->categorySlug;
    }


    public function setCategorySlug($categorySlug): void
    {
        $this->categorySlug = $categorySlug;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Category $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Category $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }
    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->addCategory($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            $product->removeCategory($this);
        }

        return $this;
    }

    /**
     * @return Collection|TagTypeLimitation[]
     */
    public function getProductTagTypeLimitations(): Collection
    {
        return $this->productTagTypeLimitations;
    }

    public function addProductTagTypeLimitation(TagTypeLimitation $productTagTypeLimitation): self
    {
        if (!$this->productTagTypeLimitations->contains($productTagTypeLimitation)) {
            $this->productTagTypeLimitations[] = $productTagTypeLimitation;
            $productTagTypeLimitation->addCategory($this);
        }

        return $this;
    }

    public function removeProductTagTypeLimitation(TagTypeLimitation $productTagTypeLimitation): self
    {
        if ($this->productTagTypeLimitations->removeElement($productTagTypeLimitation)) {
            $productTagTypeLimitation->removeCategory($this);
        }
        return $this;
    }

    public function getImage(): ?CategoryImage
    {
        return $this->image;
    }

    public function setImage(?CategoryImage $image): self
    {
        $this->image = $image;

        return $this;
    }

}
