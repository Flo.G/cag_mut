<?php

namespace App\Entity\File;

use App\Entity\Traits\ClassName;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Serializable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 */
class File implements Serializable
{
    use TimestampableEntity;
    use ClassName;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max="255")
     * @Assert\Type("string")
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="string",nullable=true , length=255)
     * Assert\Length (max="255")
     */
    private $filePath;

    /**
     * @ORM\Column(type="string",nullable=true ,length=255)
     * @Assert\Length(max="255")
     * @Assert\Type("string")
     */
    private $alt;

    /**
     * @ORM\Column(type="string",nullable=true ,length=255)
     * @Assert\Length(max="255")
     * @Assert\Type("string")
     */
    private $description;

    public function setImageName($imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function serialize(): string
    {
        return serialize([
            $this->getId(),
            $this->imageName,
        ]);
    }

    public function unserialize($serialized): array
    {
        return list(
            $this->id,
            $this->imageName) = unserialize($serialized, ['allowed_classes' => false]);
    }

}
