<?php

namespace App\Entity\File\Image;

use App\Entity\Company\Company;
use App\Entity\Investment\Investment;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\File\Image\LogoRepository")
 * @ORM\Table(name="img_logo")
 * @Vich\Uploadable
 */
class Logo extends \App\Entity\File\File
{
    public const DESCRIPTION = 'logo';
    public const FILTER = 'logo';
    public const URI = 'logo_image';
    public const PATH = '/uploads/images/logos';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="logo_image", fileNameProperty="imageName")
     * @Assert\File(
     *      maxSize = "1024k",
     *      mimeTypesMessage = "Please upload a file less than 1024k, we would love you."
     * )
     */
    private $imageFile;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToOne (targetEntity="App\Entity\Company\Company", mappedBy="logo").//TODO reglage pour que ça la BDD se mette bien a jour, effacer ou updater ancienn
     */
    private $company;

    /**
     * @ORM\OneToOne (targetEntity="App\Entity\Investment\Investment", mappedBy="logo").//TODO reglage pour que ça la BDD se mette bien a jour, effacer ou updater ancienn
     */
    private $investment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setImageFile(File $imageName = null): void
    {
        $this->imageFile = $imageName;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($imageName) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getInvestment(): ?Investment
    {
        return $this->investment;
    }

    public function setInvestment(?Investment $investment): self
    {
        $this->investment = $investment;

        return $this;
    }
}
