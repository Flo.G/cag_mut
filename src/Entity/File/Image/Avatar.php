<?php

namespace App\Entity\File\Image;

use App\Entity\User\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\File\Image\AvatarRepository")
 * @ORM\Table(name="img_avatar")
 * @Vich\Uploadable
 */
class Avatar extends \App\Entity\File\File
{
    public const DESCRIPTION = 'avatar';
    public const FILTER = 'avatar';
    public const URI = 'avatar_image';
    public const PATH = '/uploads/images/avatars'; //TODO recuperer dans le service param/uploads/images/avatars

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @Vich\UploadableField(mapping="avatar_image", fileNameProperty="imageName")
     * @Assert\File(
     *      maxSize = "1024k",
     *      mimeTypesMessage = "Please upload a file less than 1024k, we would love you."
     * )
     *
     * @var File|null
     */
    private $imageFile;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToOne (targetEntity="App\Entity\User\User", mappedBy="avatar")//TODO reglage pour que ça la BDD se mette bien a jour, effacer ou updater ancienne itération.
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setImageFile(File $imageName = null): void
    {
        $this->imageFile = $imageName;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($imageName) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
