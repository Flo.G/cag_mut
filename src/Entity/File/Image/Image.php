<?php

namespace App\Entity\File\Image;

use App\Entity\Company\Setting;
use App\Entity\User\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

class Image
{
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length (max="255")
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="string",nullable=true, length=255)
     * @Assert\Length (max="255")
     */
    private $filePath;

    /**
     * @ORM\Column(type="string",nullable=true ,length=255)
     */
    private $alt;

    /**
     * @ORM\Column(type="string",nullable=true ,length=255)
     */
    private $description;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    protected $updatedAt;

    //-----connect-construct-------------------------------------------------

    /**
     * ORM\OneToMany(targetEntity="App\Entity\Setting\Category", mappedBy="image").
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->updatedAt = new DateTime();
    }

    public function getAlt(): ?string
    {
        return $this->alt;
    }

    public function setAlt(string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompanyImages(): ?Setting
    {
        return $this->companyImages;
    }

    public function setCompanyImages(?Setting $companyImages): self
    {
        $this->companyImages = $companyImages;

        return $this;
    }
}
