<?php

namespace App\Entity\File\Image;

use App\Entity\Company\Company;
use App\Entity\Product\Product;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\File\Image\ProductImageRepository")
 * @ORM\Table(name="img_product")
 * @Vich\Uploadable
 */
class ProductImage extends \App\Entity\File\File
{
    public const DESCRIPTION = 'product_image';
    public const FILTER = 'product_img';
    public const URI = 'product_images'; //TODO si ça marche faire correspondreURI prefix pour les autres
    public const PATH = '/uploads/images/products'; //TODO fonctionne pas avec le filter
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName")
     * @Assert\File(
     *      maxSize = "1024k",
     *      mimeTypesMessage = "Please upload a file less than 1024k, we would love you."
     * )
     *
     * @var File|null
     */
    private $imageFile;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product\Product", mappedBy="images")
     */
    private $productImages;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="productImages")
     */
    private $company;

    public function __construct()
    {
        $this->productImages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setImageFile(File $imageName = null): void
    {
        $this->imageFile = $imageName;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($imageName) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProductImages(): Collection
    {
        return $this->productImages;
    }

    public function addProductImage(Product $productImage): self
    {
        if (!$this->productImages->contains($productImage)) {
            $this->productImages[] = $productImage;
            $productImage->addImage($this);
        }

        return $this;
    }

    public function removeProductImage(Product $productImage): self
    {
        if ($this->productImages->removeElement($productImage)) {
            $productImage->removeImage($this);
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
