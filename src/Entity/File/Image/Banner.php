<?php

namespace App\Entity\File\Image;

use App\Entity\Company\Company;
use App\Entity\Investment\Investment;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\File\Image\BannerRepository")
 * @ORM\Table(name="img_banner")
 * @Vich\Uploadable
 */
class Banner extends \App\Entity\File\File
{
    public const DESCRIPTION = 'banner';
    public const FILTER = 'banner';
    public const URI = 'banner_image';
    public const PATH = '/uploads/images/banners';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="banner_image", fileNameProperty="imageName")
     * @Assert\File(
     *      maxSize = "1024k",
     *      mimeTypesMessage = "Please upload a file less than 1024k, we would love you."
     * )
     */
    private $imageFile;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToOne (targetEntity="App\Entity\Company\Company", mappedBy="banner").
     */
    private $company;

    /**
     * @ORM\OneToOne (targetEntity="App\Entity\Investment\Investment", mappedBy="banner").
     */
    private $investment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setImageFile(File $imageName = null): void
    {
        $this->imageFile = $imageName;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($imageName) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getInvestment(): ?Investment
    {
        return $this->investment;
    }

    public function setInvestment(?Investment $investment): self
    {
        $this->investment = $investment;

        return $this;
    }
}
