<?php

namespace App\Entity\File\Image;

use App\Entity\Setting\Category;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\File\Image\CategoryImageRepository")
 * @ORM\Table(name="img_category")
 * @Vich\Uploadable
 */
class CategoryImage extends \App\Entity\File\File
{
    public const DESCRIPTION = 'category';
    public const FILTER = 'category';
    public const URI = 'category_image';
    public const PATH = '/uploads/images/categories'; //TODO recuperer dans le service param/uploads/images/categories

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Vich\UploadableField(mapping="category_image", fileNameProperty="imageName")
     * @Assert\File(
     *      maxSize = "1024k",
     *      mimeTypesMessage = "Please upload a file less than 1024k, we would love you."
     * )
     *
     * @var File|null
     */
    private $imageFile;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Setting\Category", mappedBy="image" ,cascade={"persist"})
     *
     */
    private $categories;

    public function __toString(): string
    {
        return $this->getImageName();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setImageFile(File $imageName = null): void
    {
        $this->imageFile = $imageName;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
         if ($imageName) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new DateTime('now');
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function getCategories(): ?Category
    {
        return $this->categories;
    }

    public function setCategories(?Category $categories): self
    {
        // unset the owning side of the relation if necessary
        if ($categories === null && $this->categories !== null) {
            $this->categories->setImage(null);
        }

        // set the owning side of the relation if necessary
        if ($categories !== null && $categories->getImage() !== $this) {
            $categories->setImage($this);
        }

        $this->categories = $categories;

        return $this;
    }
}
