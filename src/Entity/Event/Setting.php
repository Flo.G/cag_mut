<?php

namespace App\Entity\Event;

use App\Entity\File\Image\Banner;
use App\Entity\File\Image\Logo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class Setting
{
    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Image\Logo", inversedBy="company", cascade={"persist"}, fetch="EAGER")
     */
    private $logo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\File\Image\Banner", inversedBy="company", cascade={"persist"}, fetch="EAGER")
     */
    private $banner;

    public function getLogo(): ?Logo
    {
        return $this->logo;
    }

    public function setLogo(?Logo $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getBanner(): ?Banner
    {
        return $this->banner;
    }

    public function setBanner(?Banner $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    /*    public function removeLogo( $logo): self
        {
            if ($this->logo->removeElement($logo)) {
                // set the owning side to null (unless already changed)
                if ($logo->getCompanyImages() === $this) {
                    $logo->setCompanyImages(null);
                }
            }

            return $this;
        }*/
}
