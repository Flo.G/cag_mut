<?php

namespace App\Entity\Event;

use App\Entity\Company\Company;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Event\CompanyRoleRepository::class)
 * @ORM\Table(name="role_event_company")
 */
class CompanyRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * roles : owner, viewer.
     *
     * @ORM\Column(type="json")
     * @Assert\Json()
     */
    private $roles = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $budget;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Event\Event", inversedBy="companyRoles", fetch="EAGER")
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="eventsRoles", fetch="EAGER")
     */
    private $company;

    public function __toString()
    {
        return $this->getCompany()->getSlug();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getBudget(): ?int
    {
        return $this->budget;
    }

    public function setBudget(?int $budget): self
    {
        $this->budget = $budget;

        return $this;
    }
}
