<?php

namespace App\Entity\Event;

use App\Entity\Traits\BlameableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Event\EventRepository")
 */
class Event extends Setting
{
    use BlameableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     */
    private $description;

    /**
     * @Gedmo\Slug (fields={"name"})
     * @ORM\Column(type="string", length=80)
     */
    private $eventSlug;

    /**
     * @ORM\Column(type="datetime", precision=6)
     */
    private $dateStart;

    /**
     * @ORM\Column(type="datetime", precision=6)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allDay = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $budget;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $background_color;

    //-----connect-construct-------------------------------------------------

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne (targetEntity="App\Entity\Event\Event", inversedBy="children")
     * @ORM\JoinColumn (nullable=true, referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Event\Event", mappedBy="parent"))
     * TODO MaxDepth(maxDepth=2)
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $children;

    /**
     * @ORM\OneToMany (targetEntity="CompanyRole", mappedBy="event", fetch="EAGER")
     */
    private $companyRoles;

    /**
     * @ORM\OneToMany (targetEntity="App\Entity\Event\UserRole", mappedBy="event", fetch="EAGER")
     */
    private $eventUser;

    /**
     *@ORM\OneToMany (targetEntity="App\Entity\Event\Stage", mappedBy="event")
     */
    private $stages;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->companyRoles = new ArrayCollection();
        $this->eventUser = new ArrayCollection();
        $this->stages = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->eventSlug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEventSlug(): ?string
    {
        return $this->eventSlug;
    }

    public function setEventSlug(string $eventSlug): self
    {
        $this->eventSlug = $eventSlug;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getBudget(): ?int
    {
        return $this->budget;
    }

    public function setBudget(?int $budget): self
    {
        $this->budget = $budget;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->background_color;
    }

    public function setBackgroundColor(string $background_color): self
    {
        $this->background_color = $background_color;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(Event $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(Event $child): self
    {
        if ($this->children->removeElement($child)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyRole[]
     */
    public function getCompanyRoles(): Collection
    {
        return $this->companyRoles;
    }

    public function addCompanyRoles(CompanyRole $companyRoles): self
    {
        if (!$this->companyRoles->contains($companyRoles)) {
            $this->companyRoles[] = $companyRoles;
            $companyRoles->setEvent($this);
        }

        return $this;
    }

    public function removeCompanyRoles(CompanyRole $companyRole): self
    {
        if ($this->companyRoles->removeElement($companyRole)) {
            // set the owning side to null (unless already changed)
            if ($companyRole->getEvent() === $this) {
                $companyRole->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserRole[]
     */
    public function getEventUser(): Collection
    {
        return $this->eventUser;
    }

    public function addEventUser(UserRole $eventUser): self
    {
        if (!$this->eventUser->contains($eventUser)) {
            $this->eventUser[] = $eventUser;
            $eventUser->setEvent($this);
        }

        return $this;
    }

    public function removeEventUser(UserRole $eventUser): self
    {
        if ($this->eventUser->removeElement($eventUser)) {
            // set the owning side to null (unless already changed)
            if ($eventUser->getEvent() === $this) {
                $eventUser->setEvent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stage[]
     */
    public function getStages(): Collection
    {
        return $this->stages;
    }

    public function addStage(Stage $stage): self
    {
        if (!$this->stages->contains($stage)) {
            $this->stages[] = $stage;
            $stage->setEvent($this);
        }

        return $this;
    }

    public function removeStage(Stage $stage): self
    {
        if ($this->stages->removeElement($stage)) {
            // set the owning side to null (unless already changed)
            if ($stage->getEvent() === $this) {
                $stage->setEvent(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function addCompanyRole(CompanyRole $companyRole): self
    {
        if (!$this->companyRoles->contains($companyRole)) {
            $this->companyRoles[] = $companyRole;
            $companyRole->setEvent($this);
        }

        return $this;
    }

    public function removeCompanyRole(CompanyRole $companyRole): self
    {
        if ($this->companyRoles->removeElement($companyRole)) {
            // set the owning side to null (unless already changed)
            if ($companyRole->getEvent() === $this) {
                $companyRole->setEvent(null);
            }
        }

        return $this;
    }

    public function getAllDay(): ?bool
    {
        return $this->allDay;
    }

    public function setAllDay(?bool $allDay): self
    {
        $this->allDay = $allDay;

        return $this;
    }
}
