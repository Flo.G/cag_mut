<?php

namespace App\Entity\Event;

use App\Entity\Company\Company;
use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Event\UserRoleRepository")
 * @ORM\Table(name="role_event_user")
 */
class UserRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     * @Assert\Json()
     */
    private $roles = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $isVerified = false;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Event\Event", inversedBy="eventUser", fetch="EAGER")
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="userEventRoles", fetch="EAGER")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="userEventRoles", fetch="EAGER")
     */
    private $user;

    public function __toString(): string
    {
        return $this->getUser()->getFirstName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(array $roles): UserRole
    {
        foreach ($roles as $role) {
            if (!in_array($role, $this->getRoles(), true)) {
                $this->roles[] = $role;
            }
        }

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getEvent(): ?Event
    {
        return $this->event;
    }

    public function setEvent(?Event $event): self
    {
        $this->event = $event;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
