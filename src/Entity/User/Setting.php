<?php

namespace App\Entity\User;

use App\Entity\File\Image\Avatar;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class Setting
{
    /**
     * @ORM\OneToOne  (targetEntity="App\Entity\File\Image\Avatar", inversedBy="user", cascade={"persist"}, fetch="EAGER")//TODO reglage pour que ça la BDD se mette bien a jour, effacer ou updater ancienne itération
     */
    private $avatar;

    public function getAvatar(): ?Avatar
    {
        return $this->avatar;
    }

    public function setAvatar(?Avatar $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }
}
