<?php

namespace App\Entity\User;

use App\Entity\Company\UserRole;
use App\Entity\ContactData\Email;
use App\Entity\ContactData\Phone;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\User\UserRepository")
 * @UniqueEntity(fields={"email"}, message="registration.email.already_exist")
 * UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User extends Setting implements UserInterface, EquatableInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\Slug (fields = {"firstName","lastName"})
     * @ORM\Column(type="string", length=200, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=80, unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=180, unique=true, nullable=true)
     */
    private $apiToken;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Type("string")
     * @Assert\Length(max="100")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Length(max="100")
     * @Assert\Type("string")
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=80, nullable=true)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     */
    private $nickname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Assert\Type("string")
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Assert\Length(max="50")
     * @Assert\Type("string")
     */
    private $city;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     */
    private $postalCode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled = true; //je confirme que je suis pas d'accrord par défaut enable doit etre sur true pour un user contrairement a company

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ContactData\Phone", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $phones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ContactData\Email", mappedBy="user", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $emails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Company\UserRole", mappedBy="user")
     */
    private $userCompanyRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Event\UserRole", mappedBy="user")
     */
    private $userEventRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stockpile\UserRole", mappedBy="user", fetch="EAGER")
     */
    private $userStockpileRoles;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->userCompanyRoles = new ArrayCollection();
        $this->userEventRoles = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->userStockpileRoles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    public function setApiToken(?string $apiToken): self
    {
        $this->apiToken = $apiToken;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function addRole(string $role): User
    {
        if (!in_array($role, $this->getRoles(), true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(string $role): User
    {
        unset($this->roles[array_search($role, $this->roles, true)]);
        sort($this->roles);

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(?string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|Phone[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    public function addPhone(Phone $phone): self
    {
        if (!$this->phones->contains($phone)) {
            $this->phones[] = $phone;
            $phone->setUser($this);
        }

        return $this;
    }

    public function removePhone(Phone $phone): self
    {
        if ($this->phones->removeElement($phone)) {
            // set the owning side to null (unless already changed)
            if ($phone->getUser() === $this) {
                $phone->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserRole[]
     */
    public function getUserCompanyRoles(): Collection
    {
        return $this->userCompanyRoles;
    }

    public function addUserCompanyRole(UserRole $userCompanyRole): self
    {
        if (!$this->userCompanyRoles->contains($userCompanyRole)) {
            $this->userCompanyRoles[] = $userCompanyRole;
            $userCompanyRole->setUser($this);
        }

        return $this;
    }

    public function removeUserCompanyRole(UserRole $userCompanyRole): self
    {
        if ($this->userCompanyRoles->removeElement($userCompanyRole)) {
            // set the owning side to null (unless already changed)
            if ($userCompanyRole->getUser() === $this) {
                $userCompanyRole->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|\App\Entity\Event\UserRole[]
     */
    public function getUserEventRoles(): Collection
    {
        return $this->userEventRoles;
    }

    public function addUserEventRole(\App\Entity\Event\UserRole $userEventRole): self
    {
        if (!$this->userEventRoles->contains($userEventRole)) {
            $this->userEventRoles[] = $userEventRole;
            $userEventRole->setUser($this);
        }

        return $this;
    }

    public function removeUserEventRole(\App\Entity\Event\UserRole $userEventRole): self
    {
        if ($this->userEventRoles->removeElement($userEventRole)) {
            // set the owning side to null (unless already changed)
            if ($userEventRole->getUser() === $this) {
                $userEventRole->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Email[]
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(Email $email): self
    {
        if (!$this->emails->contains($email)) {
            $this->emails[] = $email;
            $email->setUser($this);
        }

        return $this;
    }

    public function removeEmail(Email $email): self
    {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getUser() === $this) {
                $email->setUser(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function isEqualTo(UserInterface $user): bool
    {
        if ($this->id !== $user->getId()) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->email !== $user->getEmail()) {
            return false;
        }

        return true;
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return Collection|\App\Entity\Stockpile\UserRole[]
     */
    public function getUserStockpileRoles(): Collection
    {
        return $this->userStockpileRoles;
    }

    public function addUserStockpileRole(\App\Entity\Stockpile\UserRole $userStockpileRole): self
    {
        if (!$this->userStockpileRoles->contains($userStockpileRole)) {
            $this->userStockpileRoles[] = $userStockpileRole;
            $userStockpileRole->setUser($this);
        }

        return $this;
    }

    public function removeUserStockpileRole(\App\Entity\Stockpile\UserRole $userStockpileRole): self
    {
        if ($this->userStockpileRoles->removeElement($userStockpileRole)) {
            // set the owning side to null (unless already changed)
            if ($userStockpileRole->getUser() === $this) {
                $userStockpileRole->setUser(null);
            }
        }

        return $this;
    }
}
