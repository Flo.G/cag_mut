<?php

namespace App\Entity\Product;

use App\Entity\File\Image\ProductImage;
use App\Entity\Investment\Investment;
use App\Entity\Rent\Rent;
use App\Entity\Setting\Category;
use App\Entity\Setting\TimeCoefficient;
use App\Entity\Stockpile\Stockpile;
use App\Entity\Traits\BlameableEntity;
use App\Entity\Traits\ClassName;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\ProductRepository")
 */
class Product
{
    use BlameableEntity;
    use MesureTrait;
    use TimestampableEntity;
    use ClassName;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     */
    private $name;

    /**
     * @Gedmo\Slug (fields = {"globalReference"})
     * @ORM\Column(type="string", length=80)
     */
    private $productReference;

    /**
     * **********
     * @ORM\Column(type="string", length=30 )
     */
    private $globalReference;

    /**
     * @ORM\Column(type="string", length=350, nullable=true)
     * @Assert\Length(max="350")
     * @Assert\Type("string")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disponibility;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     * @Assert\PositiveOrZero
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $geograficLimitation;

    /**
     * Always rounded up value.
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     * @Assert\PositiveOrZero
     */
    private $bigCost;

    /**
     * @ORM\Column(type="money", nullable=true)
     */
    private $depositValue;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $tvaOption;

    /**
     * @ORM\Column(type="json", nullable=true)
     * // tablo json : volt | watt | preparé par nbr personne| ratio mesure/unité | materiel de surface
     */
    private $controlDocumentation = [];

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\State", inversedBy="products")
     *
     */
    private $state;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Line", inversedBy="products")
     *
     */
    private $line;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Stockpile\Stockpile", inversedBy="products")
     */
    private $stockpile;

    /**
     * !!! tant que non lié le produit n'est pas fini d'enregistré !!!
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Investment\Investment", inversedBy="products")
     */
    private $investment;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Setting\Category", inversedBy="products")
     */
    private $categories;

    /**
     * @ORM\ManyToMany (targetEntity="App\Entity\File\Image\ProductImage", inversedBy="productImages", cascade={"persist", "remove"})//TODO inversdebBy
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity=TagTypeLimitation::class, inversedBy="products")
     */
    private $tagTypeLimitations;

    /**
     * @ORM\ManyToMany(targetEntity=Rent::class, mappedBy="products")
     */
    private $rents;

    /**
     * @ORM\ManyToOne(targetEntity=TimeCoefficient::class, inversedBy="products")
     */
    private $timeCoefficient;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->tagTypeLimitations = new ArrayCollection();
        $this->rents = new ArrayCollection();
    }

    /**
     * ORM\OneToMany(targetEntity="App\Entity\Product\PackRelationShip", mappedBy="parentProduct")
     * ORM\JoinTable(name="mothers_product_pack").
     */
    //private $asParentPacks;

    /**
     * ORM\OneToMany(targetEntity="App\Entity\Product\PackRelationShip", mappedBy="childrenProduct")
     * ORM\JoinTable(name="mothers_product_pack").
     */
    //private $asChildrenPacks;

    public function __toString()
    {
        return $this->name;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProductReference(): ?string
    {
        return $this->productReference;
    }

    public function setProductReference(string $productReference): self
    {
        $this->productReference = $productReference;

        return $this;
    }

    public function getGlobalReference(): ?string
    {
        return $this->globalReference;
    }

    public function setGlobalReference(string $globalReference): self
    {
        $this->globalReference = $globalReference;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDisponibility(): ?bool
    {
        return $this->disponibility;
    }

    public function setDisponibility(?bool $disponibility): self
    {
        $this->disponibility = $disponibility;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getGeograficLimitation(): ?string
    {
        return $this->geograficLimitation;
    }

    public function setGeograficLimitation(?string $geograficLimitation): self
    {
        $this->geograficLimitation = $geograficLimitation;

        return $this;
    }

    public function getBigCost(): ?int
    {
        return $this->bigCost;
    }

    public function setBigCost(?int $bigCost): self
    {
        $this->bigCost = $bigCost;

        return $this;
    }

    public function getDepositValue()
    {
        return $this->depositValue;
    }

    public function setDepositValue($depositValue): self
    {
        $this->depositValue = $depositValue;

        return $this;
    }

    public function getTvaOption(): ?int
    {
        return $this->tvaOption;
    }

    public function setTvaOption(?int $tvaOption): self
    {
        $this->tvaOption = $tvaOption;

        return $this;
    }

    public function getControlDocumentation(): ?array
    {
        return $this->controlDocumentation;
    }

    public function setControlDocumentation(?array $controlDocumentation): self
    {
        $this->controlDocumentation = $controlDocumentation;

        return $this;
    }

    public function getState(): ?State
    {
        return $this->state;
    }

    public function setState(?State $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getLine(): ?Line
    {
        return $this->line;
    }

    public function setLine(?Line $line): self
    {
        $this->line = $line;

        return $this;
    }

    public function getStockpile(): ?Stockpile
    {
        return $this->stockpile;
    }

    public function setStockpile(?Stockpile $stockpile): self
    {
        $this->stockpile = $stockpile;

        return $this;
    }

    public function getInvestment(): ?Investment
    {
        return $this->investment;
    }

    public function setInvestment(?Investment $investment): self
    {
        $this->investment = $investment;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(ProductImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }

        return $this;
    }

    public function removeImage(ProductImage $image): self
    {
        $this->images->removeElement($image);

        return $this;
    }

    /**
     * @return Collection|TagTypeLimitation[]
     */
    public function getTagTypeLimitations(): Collection
    {
        return $this->tagTypeLimitations;
    }

    public function addTagTypeLimitation(TagTypeLimitation $tagTypeLimitation): self
    {
        if (!$this->tagTypeLimitations->contains($tagTypeLimitation)) {
            $this->tagTypeLimitations[] = $tagTypeLimitation;
        }

        return $this;
    }

    public function addTagTypeLimitations(array $array): void
    {
        foreach ($array as $value) {
            $this->addTagTypeLimitation($value);
        }
    }

    public function removeTagTypeLimitation(TagTypeLimitation $tagTypeLimitation): self
    {
        $this->tagTypeLimitations->removeElement($tagTypeLimitation);

        return $this;
    }

    /**
     * @return Collection|Rent[]
     */
    public function getRents(): Collection
    {
        return $this->rents;
    }

    public function addRent(Rent $rent): self
    {
        if (!$this->rents->contains($rent)) {
            $this->rents[] = $rent;
            $rent->addProduct($this);
        }

        return $this;
    }

    public function removeRent(Rent $rent): self
    {
        if ($this->rents->removeElement($rent)) {
            $rent->removeProduct($this);
        }

        return $this;
    }

    public function getTimeCoefficient(): ?TimeCoefficient
    {
        return $this->timeCoefficient;
    }

    public function setTimeCoefficient(?TimeCoefficient $timeCoefficient): self
    {
        $this->timeCoefficient = $timeCoefficient;

        return $this;
    }
}
