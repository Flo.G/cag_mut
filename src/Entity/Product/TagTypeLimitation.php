<?php

namespace App\Entity\Product;

use App\Entity\Setting\Category;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\TagTypeLimitationRepository")
 * @ORM\Table(name="setting_prod_tag_type_limitation")
 */
class TagTypeLimitation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug (fields={"name"})
     * @ORM\Column(type="string", length=80)
     */
    private $slugProductLimitation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, mappedBy="tagTypeLimitations")
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Setting\Category", mappedBy="productTagTypeLimitations")
     */
    private $categories;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlugProductLimitation(): ?string
    {
        return $this->slugProductLimitation;
    }

    public function setSlugProductLimitation(string $slugProductLimitation): self
    {
        $this->slugProductLimitation = $slugProductLimitation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }
    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);
        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addProductTagTypeLimitation($this);
        }
        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            $category->removeProductTagTypeLimitation($this);
        }
        return $this;
    }
}
