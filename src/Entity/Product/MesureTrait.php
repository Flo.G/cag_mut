<?php

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;

trait MesureTrait
{
    /**
     * @ORM\Column(type="mass", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="length", nullable=true)
     */
    private $length;

    /**
     * @ORM\Column(type="length", nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="length", nullable=true)
     */
    private $width;

    /**
     * @ORM\Column(type="volume", nullable=true)
     */
    private $transportVolume;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $preparedBy;

    /**
     * @ORM\Column(type="current", nullable=true)
     */
    private $current;

    /**
     * @ORM\Column(type="power", nullable=true)
     */
    private $power;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $surfaceMaterial;

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($length): self
    {
        $this->length = $length;

        return $this;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getTransportVolume()
    {
        return $this->transportVolume;
    }

    public function setTransportVolume($transportVolume): self
    {
        $this->transportVolume = $transportVolume;

        return $this;
    }

    public function getPreparedBy(): ?int
    {
        return $this->preparedBy;
    }

    public function setPreparedBy(?int $preparedBy): self
    {
        $this->preparedBy = $preparedBy;

        return $this;
    }

    public function getCurrent()
    {
        return $this->current;
    }

    public function setCurrent($current): self
    {
        $this->current = $current;

        return $this;
    }

    public function getPower()
    {
        return $this->power;
    }

    public function setPower($power): self
    {
        $this->power = $power;

        return $this;
    }

    public function getSurfaceMaterial(): ?bool
    {
        return $this->surfaceMaterial;
    }

    public function setSurfaceMaterial(?bool $surfaceMaterial): self
    {
        $this->surfaceMaterial = $surfaceMaterial;

        return $this;
    }
}
