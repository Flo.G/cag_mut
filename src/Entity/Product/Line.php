<?php

namespace App\Entity\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\LineRepository")
 */
class Line
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * //Nom de la gamme d'utilisation.
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * //Durée d'amortisssement.
     *
     * @ORM\Column(type="integer")
     */
    private $amortization_year;

    /**
     * //Valeur du taux d'utilisation annuelle.
     *
     * @ORM\Column(type="integer")
     */
    private $percentage;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product\Product", mappedBy="line")
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAmortizationYear(): ?int
    {
        return $this->amortization_year;
    }

    public function setAmortizationYear(int $amortization_year): self
    {
        $this->amortization_year = $amortization_year;

        return $this;
    }

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    public function setPercentage(int $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setLine($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getLine() === $this) {
                $product->setLine(null);
            }
        }

        return $this;
    }
}
