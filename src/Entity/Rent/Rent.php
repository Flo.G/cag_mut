<?php

namespace App\Entity\Rent;

use App\Entity\Event\Stage;
use App\Entity\Product\Product;
use App\Entity\Traits\BlameableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Rent\RentRepository")
 */
class Rent
{
    use BlameableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\Slug (fields={"title"})
     * @ORM\Column(type="string", length=80)
     */
    private $rentSlug;

    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     */
    private $title;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $internRent;

    /**
     * @ORM\Column(type="datetime", precision=6)
     */
    private $dateStart;

    /**
     * @ORM\Column(type="datetime", precision=6)
     */
    private $dateEnd;

    //-----connect-construct-------------------------------------------------

    /**
     *@ORM\ManyToOne (targetEntity="App\Entity\Event\Stage", inversedBy="rents").
     */
    private $stage;

    /**
     * @ORM\ManyToMany(targetEntity=Product::class, inversedBy="rents")
     */
    private $products;

    /**
     * @ORM\OneToOne(targetEntity=Contract::class, mappedBy="rent")
     */
    private $contract;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRentSlug(): ?string
    {
        return $this->rentSlug;
    }

    public function setRentSlug(string $slug): self
    {
        $this->rentSlug = $slug;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getInternRent(): ?bool
    {
        return $this->internRent;
    }

    public function setInternRent(?bool $internRent): self
    {
        $this->internRent = $internRent;

        return $this;
    }

    public function getStage(): ?Stage
    {
        return $this->stage;
    }

    public function setStage(?Stage $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        $this->products->removeElement($product);

        return $this;
    }

    public function getContract(): ?Contract
    {
        return $this->contract;
    }

    public function setContract(?Contract $contract): self
    {
        // unset the owning side of the relation if necessary
        if (null === $contract && null !== $this->contract) {
            $this->contract->setRent(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $contract && $contract->getRent() !== $this) {
            $contract->setRent($this);
        }

        $this->contract = $contract;

        return $this;
    }
}
