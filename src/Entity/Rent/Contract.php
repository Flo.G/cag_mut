<?php

namespace App\Entity\Rent;

use App\Entity\Company\Company;
use App\Entity\Traits\BlameableEntity;
use App\Repository\Rent\ContractRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\Timestampable;

/**
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 */
class Contract
{
    use BlameableEntity;
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $bigValue;

    /**
     * @ORM\Column(type="date")
     */
    private $validationDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $cagibigCommission;

    /**
     * @ORM\Column(type="integer")
     */
    private $stockCommission;

    /**
     * @ORM\Column(type="integer", nullable= true)
     */
    private $maintenanceCommission;


    /**
     * @ORM\Column(type="text")
     */
    private $contractContent;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToOne(targetEntity=Rent::class, inversedBy="contract")
     */
    private $rent;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="lenderContracts")
     */
    private $lender;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="borrowerContracts")
     */
    private $borrower;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBigValue(): ?int
    {
        return $this->bigValue;
    }

    public function setBigValue(int $bigValue): self
    {
        $this->bigValue = $bigValue;

        return $this;
    }

    public function getValidationDate(): ?\DateTimeInterface
    {
        return $this->validationDate;
    }

    public function setValidationDate(\DateTimeInterface $validationDate): self
    {
        $this->validationDate = $validationDate;

        return $this;
    }

    public function getCagibigCommission(): ?int
    {
        return $this->cagibigCommission;
    }

    public function setCagibigCommission(int $cagibigCommission): self
    {
        $this->cagibigCommission = $cagibigCommission;

        return $this;
    }

    public function getStockCommission(): ?int
    {
        return $this->stockCommission;
    }

    public function setStockCommission(int $stockCommission): self
    {
        $this->stockCommission = $stockCommission;

        return $this;
    }
    public function getMaintenanceCommission()
    {
        return $this->maintenanceCommission;
    }

    public function setMaintenanceCommission($maintenanceCommission): void
    {
        $this->maintenanceCommission = $maintenanceCommission;
    }

    public function getContractContent(): ?string
    {
        return $this->contractContent;
    }

    public function setContractContent(string $contractContent): self
    {
        $this->contractContent = $contractContent;

        return $this;
    }

    public function getRent(): ?Rent
    {
        return $this->rent;
    }

    public function setRent(?Rent $rent): self
    {
        $this->rent = $rent;

        return $this;
    }

    public function getLender(): ?Company
    {
        return $this->lender;
    }

    public function setLender(?Company $lender): self
    {
        $this->lender = $lender;

        return $this;
    }

    public function getBorrower(): ?Company
    {
        return $this->borrower;
    }

    public function setBorrower(?Company $borrower): self
    {
        $this->borrower = $borrower;

        return $this;
    }
}
