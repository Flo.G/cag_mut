<?php

namespace App\Entity\Company;

use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\UserRoleRepository")
 * @ORM\Table(name="role_company_user")
 */
class UserRole
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="userRoles", fetch="EAGER")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User\User", inversedBy="userCompanyRoles", fetch="EAGER")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function addRole(array $roles): UserRole
    {
        foreach ($roles as $role) {
            if (!in_array($role, $this->getRoles(), true)) {
                $this->roles[] = $role;
            }
        }

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
