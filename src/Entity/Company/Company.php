<?php

namespace App\Entity\Company;

use App\Entity\ContactData\Email;
use App\Entity\ContactData\Phone;
use App\Entity\Event\CompanyRole;
use App\Entity\Event\UserRole;
use App\Entity\File\Image\ProductImage;
use App\Entity\Investment\TagTypeLimitation;
use App\Entity\Rent\Contract;
use App\Entity\Stockpile;
use App\Entity\Traits\BlameableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\CompanyRepository")
 */
class Company extends Setting
{
    use BlameableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     * @Assert\Email
     */
    private $email = null;

    /**
     * @ORM\Column(type="string", length=80)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     */
    private $name = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     */
    private $description = null;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     * @Assert\Length(max="40")
     * @Assert\Type("string")
     */
    private $type = null;

    /**
     * @Gedmo\Slug (fields = {"name","postalCode"})
     * @ORM\Column(type="string", length=80, unique=true)
     */
    private $companySlug;

    /**
     * @ORM\Column(type="string", length=80, unique=true, nullable=true)
     * @Assert\Length(max="80")
     * @Assert\Type("string")
     */
    private $clientId = null;

    /**
     * A big is the balance of company like Money.
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $big = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\Type(type="integer")
     */
    private $credit = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Assert\Type("string")
     */
    private $address = null;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     * @Assert\Length(max="60")
     * @Assert\Type("string")
     */
    private $city = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Type("string")
     */
    private $postalCode = null;

    /**
     * @ORM\Column(type="string", length=25, unique=true, nullable=true)
     * @Assert\Length(max="25")
     * @Assert\Type("string")
     */
    private $siret = null;

    /**
     * @ORM\Column(type="tvarate", nullable=true, precision=2, scale=2)//TODO Unknown column type "TVARate" requested. Any Doctrine type that you use has to be registered with \Doctrine\DBAL\Types\Type::addType(). You can get a list of al.
    he type name is empty you might have a problem with the cache or forgot some mapping information.
     */
    private $tvaRate = null;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max="15")
     * @Assert\Type("string")
     */
    private $tvaCode = null;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max="15")
     * @Assert\Type("string")
     */
    private $codeApe = null;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     * @Assert\Iban()
     */
    private $iban = null;

    /**
     * @ORM\Column(type="string", length=34, nullable=true)
     * @Assert\Length(max="34")
     * @Assert\Type("string")
     * @Assert\Bic
     */
    private $bic = null;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     * @Assert\Length(max="15")
     * @Assert\Type("string")
     */
    private $currency = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activationToken = null;

    /**
     * @ORM\Column(type="boolean", options={"default":false}, nullable=true)
     */
    private $enabled = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;


    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stockpile\CompanyRole", mappedBy="company")
     */
    private $stockpileCompanyRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Stockpile\UserRole", mappedBy="company", fetch="EAGER")
     */
    private $userStockpileRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Company\UserRole", mappedBy="company", fetch="EAGER")
     */
    private $userRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Investment\CompanyRole", mappedBy="company")
     */
    private $investmentCompanyRoles;

    /**
     * @ORM\OneToMany (targetEntity="App\Entity\Event\CompanyRole", mappedBy="company")
     */
    private $eventsRoles;

    /**
     * @ORM\OneToMany (targetEntity="App\Entity\Event\UserRole", mappedBy="company")
     */
    private $userEventRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ContactData\Phone", mappedBy="company", cascade={"persist"} )
     */
    private $phones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ContactData\Email", mappedBy="company")
     */
    private $emails;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\File\Image\ProductImage", mappedBy="company")
     */
    private $productImages;

    /**
     * @ORM\OneToMany(targetEntity=Contract::class, mappedBy="lender")
     */
    private $lenderContracts;

    /**
     * @ORM\OneToMany(targetEntity=Contract::class, mappedBy="borrower")
     */
    private $borrowerContracts;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Investment\TagTypeLimitation", mappedBy="company" )
     */
    private $investTagTypeLimitations;

    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->stockpileCompanyRoles = new ArrayCollection();
        $this->userRoles = new ArrayCollection();
        $this->eventsRoles = new ArrayCollection();
        $this->emails = new ArrayCollection();
        $this->investmentCompanyRoles = new ArrayCollection();
        $this->userEventRoles = new ArrayCollection();
        $this->productImages = new ArrayCollection();
        $this->userStockpileRoles = new ArrayCollection();
        $this->lenderContracts = new ArrayCollection();
        $this->borrowerContracts = new ArrayCollection();
        $this->investTagTypeLimitations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->companySlug;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCompanySlug(): ?string
    {
        return $this->companySlug;
    }

    public function setCompanySlug(string $companySlug): self
    {
        $this->companySlug = $companySlug;

        return $this;
    }

    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getBig(): ?int
    {
        return $this->big;
    }

    public function setBig(?int $big): self
    {
        $this->big = $big;

        return $this;
    }

    public function getCredit(): ?int
    {
        return $this->credit;
    }

    public function setCredit(?int $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getTvaRate()
    {
        return $this->tvaRate;
    }

    public function setTvaRate($tvaRate): self
    {
        $this->tvaRate = $tvaRate;

        return $this;
    }

    public function getTvaCode(): ?string
    {
        return $this->tvaCode;
    }

    public function setTvaCode(?string $tvaCode): self
    {
        $this->tvaCode = $tvaCode;

        return $this;
    }

    public function getCodeApe(): ?string
    {
        return $this->codeApe;
    }

    public function setCodeApe(?string $codeApe): self
    {
        $this->codeApe = $codeApe;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getBic(): ?string
    {
        return $this->bic;
    }

    public function setBic(?string $bic): self
    {
        $this->bic = $bic;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(?string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getActivationToken(): ?string
    {
        return $this->activationToken;
    }

    public function setActivationToken(?string $activationToken): self
    {
        $this->activationToken = $activationToken;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /*    public function removePhone(Phone $phone): self
        {
            if ($this->phones->removeElement($phone)) {
                // set the owning side to null (unless already changed)
                if ($phone->getCompany() === $this) {
                    $phone->setCompany(null);
                }
            }

            return $this;
        }*/

    /**
     * @return Collection|Stockpile\Stockpile[]
     */
    public function getStockpiles(): Collection
    {
        return $this->stockpileCompanyRoles;
    }

    public function addStockpile(Stockpile\Stockpile $stockpile): self
    {
        if (!$this->stockpileCompanyRoles->contains($stockpile)) {
            $this->stockpileCompanyRoles[] = $stockpile;
        }

        return $this;
    }

    public function removeStockpile(Stockpile\Stockpile $stockpile): self
    {
        $this->stockpileCompanyRoles->removeElement($stockpile);

        return $this;
    }

    /**
     * @return Collection|UserRole[]
     */
    public function getUserRoles(): Collection
    {
        return $this->userRoles;
    }

    public function addUserRole(UserRole $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
            $userRole->setCompany($this);
        }

        return $this;
    }

    public function removeUserRole(UserRole $userRole): self
    {
        if ($this->userRoles->removeElement($userRole)) {
            // set the owning side to null (unless already changed)
            if ($userRole->getCompany() === $this) {
                $userRole->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyRole[]
     */
    public function getEventsRole(): Collection
    {
        return $this->eventsRoles;
    }

    public function addEventsRole(CompanyRole $eventsRole): self
    {
        if (!$this->eventsRoles->contains($eventsRole)) {
            $this->eventsRoles[] = $eventsRole;
            $eventsRole->setCompany($this);
        }

        return $this;
    }

    public function removeEventsRole(CompanyRole $eventsRole): self
    {
        if ($this->eventsRoles->removeElement($eventsRole)) {
            // set the owning side to null (unless already changed)
            if ($eventsRole->getCompany() === $this) {
                $eventsRole->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Phone[]
     */
    public function getPhones(): Collection
    {
        return $this->phones;
    }

    /*    public function addPhone(Phone $phone): self
        {
            if (!$this->phones->contains($phone)) {
                $this->phones[] = $phone;
                $phone->setCompany($this);
            }

            return $this;
        }*/

    /**
     * @return Collection|Email[]
     */
    public function getEmails(): Collection
    {
        return $this->emails;
    }

    public function addEmail(Email $email): self
    {
        if (!$this->emails->contains($email)) {
            $this->emails[] = $email;
            $email->setCompany($this);
        }

        return $this;
    }

    public function removeEmail(Email $email): self
    {
        if ($this->emails->removeElement($email)) {
            // set the owning side to null (unless already changed)
            if ($email->getCompany() === $this) {
                $email->setCompany(null);
            }
        }

        return $this;
    }

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?string $updatedBy): self
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function addPhone(Phone $phone): self
    {
        if (!$this->phones->contains($phone)) {
            $this->phones[] = $phone;
            $phone->setCompany($this);
        }

        return $this;
    }

    public function removePhone(Phone $phone): self
    {
        if ($this->phones->removeElement($phone)) {
            // set the owning side to null (unless already changed)
            if ($phone->getCompany() === $this) {
                $phone->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|\App\Entity\Stockpile\CompanyRole[]
     */
    public function getStockpileCompanyRoles(): Collection
    {
        return $this->stockpileCompanyRoles;
    }

    public function addStockpileCompanyRole(Stockpile\CompanyRole $stockpileCompanyRole): self
    {
        if (!$this->stockpileCompanyRoles->contains($stockpileCompanyRole)) {
            $this->stockpileCompanyRoles[] = $stockpileCompanyRole;
            $stockpileCompanyRole->setCompany($this);
        }

        return $this;
    }

    public function removeStockpileCompanyRole(Stockpile\CompanyRole $stockpileCompanyRole): self
    {
        if ($this->stockpileCompanyRoles->removeElement($stockpileCompanyRole)) {
            // set the owning side to null (unless already changed)
            if ($stockpileCompanyRole->getCompany() === $this) {
                $stockpileCompanyRole->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|\App\Entity\Investment\CompanyRole[]
     */
    public function getInvestmentCompanyRoles(): Collection
    {
        return $this->investmentCompanyRoles;
    }

    public function addInvestmentCompanyRole(\App\Entity\Investment\CompanyRole $investmentCompanyRole): self
    {
        if (!$this->investmentCompanyRoles->contains($investmentCompanyRole)) {
            $this->investmentCompanyRoles[] = $investmentCompanyRole;
            $investmentCompanyRole->setCompany($this);
        }

        return $this;
    }

    public function removeInvestmentCompanyRole(\App\Entity\Investment\CompanyRole $investmentCompanyRole): self
    {
        if ($this->investmentCompanyRoles->removeElement($investmentCompanyRole)) {
            // set the owning side to null (unless already changed)
            if ($investmentCompanyRole->getCompany() === $this) {
                $investmentCompanyRole->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyRole[]
     */
    public function getEventsRoles(): Collection
    {
        return $this->eventsRoles;
    }

    /**
     * @return Collection|UserRole[]
     */
    public function getUserEventRoles(): Collection
    {
        return $this->userEventRoles;
    }

    public function addUserEventRole(UserRole $userEventRole): self
    {
        if (!$this->userEventRoles->contains($userEventRole)) {
            $this->userEventRoles[] = $userEventRole;
            $userEventRole->setCompany($this);
        }

        return $this;
    }

    public function removeUserEventRole(UserRole $userEventRole): self
    {
        if ($this->userEventRoles->removeElement($userEventRole)) {
            // set the owning side to null (unless already changed)
            if ($userEventRole->getCompany() === $this) {
                $userEventRole->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getProductImages(): Collection
    {
        return $this->productImages;
    }

    public function addProductImage(ProductImage $productImage): self
    {
        if (!$this->productImages->contains($productImage)) {
            $this->productImages[] = $productImage;
            $productImage->setCompany($this);
        }

        return $this;
    }

    public function removeProductImage(ProductImage $productImage): self
    {
        if ($this->productImages->removeElement($productImage)) {
            // set the owning side to null (unless already changed)
            if ($productImage->getCompany() === $this) {
                $productImage->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|\App\Entity\Stockpile\UserRole[]
     */
    public function getUserStockpileRoles(): Collection
    {
        return $this->userStockpileRoles;
    }

    public function addUserStockpileRole(\App\Entity\Stockpile\UserRole $userStockpileRole): self
    {
        if (!$this->userStockpileRoles->contains($userStockpileRole)) {
            $this->userStockpileRoles[] = $userStockpileRole;
            $userStockpileRole->setCompany($this);
        }

        return $this;
    }

    public function removeUserStockpileRole(\App\Entity\Stockpile\UserRole $userStockpileRole): self
    {
        if ($this->userStockpileRoles->removeElement($userStockpileRole)) {
            // set the owning side to null (unless already changed)
            if ($userStockpileRole->getCompany() === $this) {
                $userStockpileRole->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getLenderContracts(): Collection
    {
        return $this->lenderContracts;
    }

    public function addLenderContract(Contract $lenderContract): self
    {
        if (!$this->lenderContracts->contains($lenderContract)) {
            $this->lenderContracts[] = $lenderContract;
            $lenderContract->setLender($this);
        }

        return $this;
    }

    public function removeLenderContract(Contract $lenderContract): self
    {
        if ($this->lenderContracts->removeElement($lenderContract)) {
            // set the owning side to null (unless already changed)
            if ($lenderContract->getLender() === $this) {
                $lenderContract->setLender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Contract[]
     */
    public function getBorrowerContracts(): Collection
    {
        return $this->borrowerContracts;
    }

    public function addBorrowerContract(Contract $borrowerContract): self
    {
        if (!$this->borrowerContracts->contains($borrowerContract)) {
            $this->borrowerContracts[] = $borrowerContract;
            $borrowerContract->setBorrower($this);
        }

        return $this;
    }

    public function removeBorrowerContract(Contract $borrowerContract): self
    {
        if ($this->borrowerContracts->removeElement($borrowerContract)) {
            // set the owning side to null (unless already changed)
            if ($borrowerContract->getBorrower() === $this) {
                $borrowerContract->setBorrower(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TagTypeLimitation[]
     */
    public function getInvestTagTypeLimitations(): Collection
    {
        return $this->investTagTypeLimitations;
    }

    public function addInvestTagTypeLimitation(TagTypeLimitation $investTagTypeLimitation): self
    {
        if (!$this->investTagTypeLimitations->contains($investTagTypeLimitation)) {
            $this->investTagTypeLimitations[] = $investTagTypeLimitation;
            $investTagTypeLimitation->addCompany($this);
        }

        return $this;
    }

    public function removeInvestTagTypeLimitation(TagTypeLimitation $investTagTypeLimitation): self
    {
        if ($this->investTagTypeLimitations->removeElement($investTagTypeLimitation)) {
            $investTagTypeLimitation->removeCompany($this);
        }

        return $this;
    }
}
