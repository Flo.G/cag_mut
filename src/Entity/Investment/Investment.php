<?php

namespace App\Entity\Investment;

use App\Entity\Product\Product;
use App\Entity\Setting\InvestModel;
use App\Entity\Traits\BlameableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Investment\InvestmentRepository")
 */
class Investment extends Setting
{
    use BlameableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column (type="string", length=200, nullable=true)
     */
    private $description;

    /**
     * @Gedmo\Slug (fields ={"name"})
     * @ORM\Column(type="string", length=100)
     */
    private $investmentSlug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $geograficLimitation;

    /**
     * @ORM\Column(type="integer", nullable=true)//TODO class money
     */
    private $endCost;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $deadLine;

    /**
     * @ORM\Column(type="dateinterval", nullable=true)
     */
    private $creditLifetime;

    /**
     * workflow state.
     *
     * @ORM\Column(type="json", nullable=true)//TODO enlever le nullable aprés le debugage du workflow
     */
    private $state = [];

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\OneToMany (targetEntity="App\Entity\Product\Product", mappedBy="investment").
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="CompanyRole", mappedBy="investment")
     */
    private $investmentCompanyRoles;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Investment\TagTypeLimitation", mappedBy="investments")
     */
    private $tagTypeLimitations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Setting\InvestModel", inversedBy="investments")
     */
    private $investModel;


    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->investmentCompanyRoles = new ArrayCollection();
        $this->tagTypeLimitations = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getInvestmentSlug(): ?string
    {
        return $this->investmentSlug;
    }

    public function setInvestmentSlug(string $investmentSlug): self
    {
        $this->investmentSlug = $investmentSlug;

        return $this;
    }

    public function getGeograficLimitation(): ?string
    {
        return $this->geograficLimitation;
    }

    public function setGeograficLimitation(?string $geograficLimitation): self
    {
        $this->geograficLimitation = $geograficLimitation;

        return $this;
    }

    public function getEndCost(): ?int
    {
        return $this->endCost;
    }

    public function setEndCost(int $endCost): self
    {
        $this->endCost = $endCost;

        return $this;
    }

    public function getState(): ?array
    {
        return $this->state;
    }

    public function setState(array $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setInvestment($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getInvestment() === $this) {
                $product->setInvestment(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CompanyRole[]
     */
    public function getInvestmentCompanyRoles(): Collection
    {
        return $this->investmentCompanyRoles;
    }

    public function addInvestmentCompanyRole(CompanyRole $investmentCompanyRole): self
    {
        if (!$this->investmentCompanyRoles->contains($investmentCompanyRole)) {
            $this->investmentCompanyRoles[] = $investmentCompanyRole;
            $investmentCompanyRole->setInvestment($this);
        }

        return $this;
    }

    public function removeInvestmentCompanyRole(CompanyRole $investmentCompanyRole): self
    {
        if ($this->investmentCompanyRoles->removeElement($investmentCompanyRole)) {
            // set the owning side to null (unless already changed)
            if ($investmentCompanyRole->getInvestment() === $this) {
                $investmentCompanyRole->setInvestment(null);
            }
        }

        return $this;
    }


    public function getDeadLine(): ?\DateTimeInterface
    {
        return $this->deadLine;
    }

    public function setDeadLine(?\DateTimeInterface $deadLine): self
    {
        $this->deadLine = $deadLine;

        return $this;
    }

    public function getCreditLifetime(): ?\DateInterval
    {
        return $this->creditLifetime;
    }

    public function setCreditLifetime(?\DateInterval $creditLifetime): self
    {
        $this->creditLifetime = $creditLifetime;

        return $this;
    }

    /**
     * @return Collection|TagTypeLimitation[]
     */
    public function getTagTypeLimitations(): Collection
    {
        return $this->tagTypeLimitations;
    }

    public function addTagTypeLimitation(TagTypeLimitation $tagTypeLimitation): self
    {
        if (!$this->tagTypeLimitations->contains($tagTypeLimitation)) {
            $this->tagTypeLimitations[] = $tagTypeLimitation;
            $tagTypeLimitation->addInvestment($this);
        }

        return $this;
    }

    public function removeTagTypeLimitation(TagTypeLimitation $tagTypeLimitation): self
    {
        if ($this->tagTypeLimitations->removeElement($tagTypeLimitation)) {
            $tagTypeLimitation->removeInvestment($this);
        }

        return $this;
    }

    public function getInvestModel(): ?InvestModel
    {
        return $this->investModel;
    }

    public function setInvestModel(?InvestModel $investModel): self
    {
        $this->investModel = $investModel;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
