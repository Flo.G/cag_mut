<?php

namespace App\Entity\Investment;

use App\Entity\Company\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Investment\TagTypeLimitationRepository")
 * @ORM\Table(name="setting_invest_tag_type_limitation")
 */
class TagTypeLimitation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug (fields ={"name"})
     * @ORM\Column(type="string", length=100)
     */
    private $slugLimitationInvestment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToMany(targetEntity=Investment::class, inversedBy="tagTypeLimitations")
     */
    private $investments;

    /**
     * @ORM\ManyToMany(targetEntity=Company::class, inversedBy="investTagTypelimitations")
     */
    private $company;

    public function __construct()
    {
        $this->investments = new ArrayCollection();
        $this->company = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlugLimitationInvestment(): ?string
    {
        return $this->slugLimitationInvestment;
    }

    public function setSlugLimitationInvestment(string $slugLimitationInvestment): self
    {
        $this->slugLimitationInvestment = $slugLimitationInvestment;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Investment[]
     */
    public function getInvestments(): Collection
    {
        return $this->investments;
    }

    public function addInvestment(Investment $investment): self
    {
        if (!$this->investments->contains($investment)) {
            $this->investments[] = $investment;
        }

        return $this;
    }

    public function removeInvestment(Investment $investment): self
    {
        $this->investments->removeElement($investment);

        return $this;
    }

    /**
     * @return Collection|Company[]
     */
    public function getCompany(): Collection
    {
        return $this->company;
    }

    public function addCompany(Company $company): self
    {
        if (!$this->company->contains($company)) {
            $this->company[] = $company;
        }

        return $this;
    }

    public function removeCompany(Company $company): self
    {
        $this->company->removeElement($company);

        return $this;
    }
}
