<?php

namespace App\Entity\Investment;

use App\Entity\Company\Company;
use App\Entity\Traits\BlameableEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=App\Repository\Investment\CompanyRoleRepository::class)
 * @ORM\Table(name="role_investment_company")
 */
class CompanyRole
{
    use BlameableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * roles: owner, investor.
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $credit;

    /**
     * @ORM\Column(type="dateinterval", nullable=true)
     */
    private $creditLifetime;

    //-----connect-construct-------------------------------------------------

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company\Company", inversedBy="investmentCompanyRoles")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Investment\Investment", inversedBy="investmentCompanyRoles")
     */
    private $investment;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getCredit(): ?int
    {
        return $this->credit;
    }

    public function setCredit(int $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getInvestment(): ?Investment
    {
        return $this->investment;
    }

    public function setInvestment(?Investment $investment): self
    {
        $this->investment = $investment;

        return $this;
    }

    public function getCreditLifetime(): ?\DateInterval
    {
        return $this->creditLifetime;
    }

    public function setCreditLifetime(?\DateInterval $creditLifetime): self
    {
        $this->creditLifetime = $creditLifetime;

        return $this;
    }
}
