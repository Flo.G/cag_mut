<?php

namespace App\Cagibig\MetricBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('cagibig_metric');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('base_units')->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('area')->defaultValue('m^2')->end()
                        ->scalarNode('electric_current')->defaultValue('A')->end()
                        ->scalarNode('energy')->defaultValue('J')->end()
                        ->scalarNode('length')->defaultValue('m')->end()
                        ->scalarNode('luminous_intensity')->defaultValue('cd')->end()
                        ->scalarNode('mass')->defaultValue('kg')->end()
                        ->scalarNode('volume')->defaultValue('m^3')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
