<?php

namespace App\Cagibig\MetricBundle\DependencyInjection;

use App\Cagibig\MetricBundle\Doctrine\Types as DoctrineTypes;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class CagibigMetricExtension extends Extension
{
    public static $types = [
        DoctrineTypes\AreaType::TYPE_NAME => DoctrineTypes\AreaType::class,
        DoctrineTypes\ElectricCurrentType::TYPE_NAME => DoctrineTypes\ElectricCurrentType::class,
        DoctrineTypes\EnergyType::TYPE_NAME => DoctrineTypes\EnergyType::class,
        DoctrineTypes\LengthType::TYPE_NAME => DoctrineTypes\LengthType::class,
        DoctrineTypes\LuminousIntensityType::TYPE_NAME => DoctrineTypes\LuminousIntensityType::class,
        DoctrineTypes\MassType::TYPE_NAME => DoctrineTypes\MassType::class,
        DoctrineTypes\VolumeType::TYPE_NAME => DoctrineTypes\VolumeType::class,
    ];

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        /*        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
                $loader->load('services.xml');*/

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        $loader->load('services.yaml');

        foreach (self::$types as $name => $doctrine_class) {
            //dd($container);
            $container->getDefinition('cagibig_metric.form.type.'.$name)->replaceArgument('$standard_unit', (string) $config['base_units'][$name]);
        }
    }
}
