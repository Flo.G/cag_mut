<?php

namespace App\Cagibig\MetricBundle;

use App\Cagibig\MetricBundle\DependencyInjection\CagibigMetricExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CagibigMetricBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        /** @var $metricExtension CagibigMetricExtension */
        $metricExtension = $container->getExtension('cagibig_metric');
    }
}
