<?php

namespace App\Cagibig\MetricBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

abstract class AbstractPhysicalQuantityType extends AbstractType
{
    /**
     * The standard unit to use with this form type.
     */
    protected $standard_unit;

    /**
     * Returns the unit class, usually a member of the
     * PhpUnitsOfMeasure\PhysicalQuantity namespace, but can be any class that
     * extends the PhpUnitsOfMeasure\AbstractPhysicalQuantity class.
     */
    abstract public function getUnitClass();

    /**
     * Construct a new instance of the form type.
     */
    public function __construct(?string $standard_unit = '')
    {
        $this->standard_unit = $standard_unit;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $unit_class = $this->getUnitClass();

        // Create available choices for units.
        $units = $unit_class::getUnitDefinitions();
        $unit_choices = [];
        foreach ($units as $unit) {
            $unit_choices[$unit->getName()] = $unit->getName();
        }

        // Build form element.
        $builder
            ->add('value', NumberType::class, [
                'required' => false,
            ])
            ->add('unit', ChoiceType::class, [
                'choices' => $unit_choices,
                'empty_data' => $this->standard_unit,
                'translation_domain' => false,
            ]);

        // Simple callback transformer for going back and forth.

        $standard_unit = $this->standard_unit;
        $builder->addModelTransformer(new CallbackTransformer(
            function ($value) use ($standard_unit) {
                //dd($value, $value->toNativeUnit(), $value['unit'], $value->getUnitDefinitions());
                if (null === $value) {
                    return null;
                }

                return [
                    'value' => $value->toNativeUnit(),
                    'unit' => $standard_unit,
                ];
            /*                return [
                                'value' => $value->toUnit($standard_unit),
                                'unit' => $standard_unit,
                            ];*/
            },
            function ($value) use ($unit_class) {
                if (null === $value || !isset($value['value'])) {
                    return null;
                }

                return new $unit_class($value['value'], $value['unit']);
            }
        ));
    }
}
