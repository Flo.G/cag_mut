<?php

namespace App\Cagibig\MetricBundle\Form\Type;

use PhpUnitsOfMeasure\PhysicalQuantity;

class AreaType extends AbstractPhysicalQuantityType
{
    public const UNIT_CLASS = PhysicalQuantity\Area::class;

    /**
     * {@inheritdoc}
     */
    public function getUnitClass(): string
    {
        return self::UNIT_CLASS;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'area';
    }
}
