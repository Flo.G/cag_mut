<?php

/*
 * This file is part of the MidnightLukePhpUnitsOfMeasureBundle package.
 *
 * (c) Luke Bainbridge <http://www.lukebainbridge.ca/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Cagibig\MetricBundle\Doctrine\Types;

use PhpUnitsOfMeasure\PhysicalQuantity;

class LuminousIntensityType extends AbstractPhysicalQuantityType
{
    public const UNIT_CLASS = PhysicalQuantity\LuminousIntensity::class;
    public const STANDARD_UNIT = 'cd';
    public const TYPE_NAME = 'luminous_intensity';

    /**
     * {@inheritdoc}
     */
    public function getUnitClass(): string
    {
        return self::UNIT_CLASS;
    }

    /**
     * {@inheritdoc}
     */
    public function getStandardUnit(): string
    {
        return self::STANDARD_UNIT;
    }

    /**
     * {@inheritdoc}
     */
    public function getTypeName(): string
    {
        return self::TYPE_NAME;
    }
}
