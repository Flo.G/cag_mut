<?php

namespace App\Repository\Company;

use App\Entity\Company\UserRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserRole[]    findAll()
 * @method UserRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserRole::class);
    }

    /**
     * @param $user_id
     * @return int|mixed|string
     */
    public function findByConnectedUser($user_id)
    {
        $qb = $this->createQueryBuilder('userRole')
            ->leftjoin('userRole.user', 't')
            ->where('t.id = :user_id')
            ->setParameter('user_id', $user_id);

        return $qb->getQuery()->getResult();
    }

    /**
     * take company by id and return all, selected by roleTarget
     * @param int $id_company
     * @param array $roleTarget
     * @return int|mixed|string
     */
    public function findAdminCompanyByIdAndRoleTarget(int $id_company, $roleTarget)
    {
        $req = $this->createQueryBuilder('cmpA')
            ->join('cmpA.company', 'c')
            ->where(  'cmpA.roles = :roles')
            ->andWhere('c.id = :id_company')
            ->setParameter('roles', $roleTarget)
            ->setParameter('id_company', $id_company);

        return $req->getQuery()->getResult();
    }
}
