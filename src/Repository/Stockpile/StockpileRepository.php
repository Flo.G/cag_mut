<?php

namespace App\Repository\Stockpile;

use App\Entity\Stockpile\Stockpile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stockpile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stockpile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stockpile[]    findAll()
 * @method Stockpile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockpileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stockpile::class);
    }

    /**
     * Returns an array of Event name and id from value, this is for autocomplete searchBar.
     */
    public function autocomplete($value)
    {
        return $this->createQueryBuilder('e')
            ->select(['e.id', 'e.name as text'])
            ->where('e.name LIKE :searchString')
            ->setParameter('searchString', $value.'%')
            ->getQuery()
            ->getArrayResult();
    }

    // /**
    //  * @return Stockpile[] Returns an array of Stockpile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stockpile
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
