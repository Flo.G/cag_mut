<?php

namespace App\Repository\Setting;

use App\Entity\Setting\InvestModel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InvestModel|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvestModel|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvestModel[]    findAll()
 * @method InvestModel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvestModelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvestModel::class);
    }

    // /**
    //  * @return SettingInvestModel[] Returns an array of SettingInvestModel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SettingInvestModel
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
