<?php

namespace App\Repository\Setting;

use App\Entity\Setting\TimeCoefficient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TimeCoefficient|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeCoefficient|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeCoefficient[]    findAll()
 * @method TimeCoefficient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeCoefficientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeCoefficient::class);
    }

    // /**
    //  * @return TimeCoefficient[] Returns an array of TimeCoefficient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TimeCoefficient
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
