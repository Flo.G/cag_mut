<?php

namespace App\Repository\Setting;

use App\Entity\Setting\Role\CompanyStockpileRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyStockpileRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyStockpileRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyStockpileRole[]    findAll()
 * @method CompanyStockpileRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyStockpileRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyStockpileRole::class);
    }

    // /**
    //  * @return CompanyStockpileRole[] Returns an array of CompanyStockpileRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyStockpileRole
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
