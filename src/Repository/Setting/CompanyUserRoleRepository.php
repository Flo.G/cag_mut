<?php

namespace App\Repository\Setting;

use App\Entity\Setting\Role\CompanyUserRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyUserRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyUserRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyUserRole[]    findAll()
 * @method CompanyUserRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyUserRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyUserRole::class);
    }

    /**
     * Returns an array of CompanyUserRole name and id from value, this is for autocomplete searchBar.
     */
    public function autocomplete($value)
    {
        return $this->createQueryBuilder('r')
            ->select(['r.id', 'r.name as text'])
            ->where('r.name LIKE :searchString')
            ->setParameter('searchString', $value.'%')
            ->getQuery()
            ->getArrayResult();
    }

    // /**
    //  * @return CompanyUserRole[] Returns an array of CompanyUserRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyUserRole
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
