<?php

namespace App\Repository\Setting;

use App\Entity\Setting\Role\CompanyEventRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyEventRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyEventRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyEventRole[]    findAll()
 * @method CompanyEventRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyEventRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyEventRole::class);
    }

    // /**
    //  * @return CompanyEventRole[] Returns an array of CompanyEventRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyEventRole
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
