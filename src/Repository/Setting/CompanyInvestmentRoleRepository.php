<?php

namespace App\Repository\Setting;

use App\Entity\Setting\Role\CompanyInvestmentRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CompanyInvestmentRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyInvestmentRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyInvestmentRole[]    findAll()
 * @method CompanyInvestmentRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyInvestmentRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyInvestmentRole::class);
    }

    // /**
    //  * @return CompanyInvestmentRole[] Returns an array of CompanyInvestmentRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CompanyInvestmentRole
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
