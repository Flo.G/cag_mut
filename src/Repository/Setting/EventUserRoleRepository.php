<?php

namespace App\Repository\Setting;

use App\Entity\Setting\Role\EventUserRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EventUserRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventUserRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventUserRole[]    findAll()
 * @method EventUserRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventUserRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventUserRole::class);
    }

    /**
     *  Returns an array of EventUserRole name and id from value, this is for autocomplete searchBar.
     */
    public function autocomplete($value)
    {
        return $this->createQueryBuilder('r')
            ->select(['r.id', 'r.name as text'])
            ->where('r.name LIKE :searchString')
            ->setParameter('searchString', $value.'%')
            ->getQuery()
            ->getArrayResult();
    }

    // /**
    //  * @return EventUserRole[] Returns an array of EventUserRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventUserRole
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
