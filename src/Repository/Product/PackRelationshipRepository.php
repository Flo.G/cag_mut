<?php

namespace App\Repository\Product;

use App\Entity\Product\PackRelationship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PackRelationship|null find($id, $lockMode = null, $lockVersion = null)
 * @method PackRelationship|null findOneBy(array $criteria, array $orderBy = null)
 * @method PackRelationship[]    findAll()
 * @method PackRelationship[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PackRelationshipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PackRelationship::class);
    }

    // /**
    //  * @return PackRelationship[] Returns an array of PackRelationship objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PackRelationship
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
