<?php

namespace App\Repository\Investment;

use App\Entity\Investment\TagTypeLimitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TagTypeLimitation|null find($id, $lockMode = null, $lockVersion = null)
 * @method TagTypeLimitation|null findOneBy(array $criteria, array $orderBy = null)
 * @method TagTypeLimitation[]    findAll()
 * @method TagTypeLimitation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TagTypeLimitationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TagTypeLimitation::class);
    }

    /**
     * Returns an array of Event name and id from value, this is for autocomplete searchBar.
     */
    public function autocomplete($value)
    {
        return $this->createQueryBuilder('e')
            ->select(['e.id', 'e.name as text'])
            ->where('e.name LIKE :searchString')
            ->setParameter('searchString', $value.'%')
            ->getQuery()
            ->getArrayResult();
    }

    // /**
    //  * @return TagTypeLimitation[] Returns an array of TagTypeLimitation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TagTypeLimitation
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
