<?php

namespace App\Service\Investment;

use App\Entity\Company\Company;
use App\Entity\Investment\Investment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class InvestmentManager
{
    private $company;
    private $entityManager;
    private $investments;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
    }

    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }

    private function getInvestments(Investment $investment)
    {
        $this->investments->contains($investment);
    }

    //TODO je change le nommage
    public function getRoleByCompany(Company $company): ?array
    {
        return $this->entityManager->getRepository(\App\Entity\Investment\CompanyRole::class)->findBy(['company' => $company->getId()]);
    }

    //TODO je change le nommage
    public function getIDByCompany(Company $company): ?array
    {
        $relations = $this->getRoleByCompany($company);
        foreach ($relations as $relation) {
            $id[] = $relation->getInvestment()->getId();
        }

        return $id;
    }
}
