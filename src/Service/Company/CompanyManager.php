<?php

namespace App\Service\Company;

use App\Entity\Company\Company;
use App\Entity\Company\UserRole;
use App\Entity\Event\Event;
use App\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class CompanyManager
{
    private $entityManager;
    private $security;
    private $companys;
    public $company;
    public $companyUserRoles;
    public $companyUserMenu;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        //$this->companyUserRoles = new ArrayCollection();
        $this->companyUserRoles = [];
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function setUser()
    {
        return $this->security->getUser();
    }

    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }

    //TODO add get by id or email
    public function getUnique(string $slug): ?Company
    {
        return $this->entityManager->getRepository(Company::class)->findOneBy(['companySlug' => $slug]);
    }

    public function getByRequest(Request $request): Company
    {
        $companyId = $request->query->get('company');

        return $this->entityManager->getRepository(Company::class)->find($companyId);
    }

    /**
     * Return array of !!! Company\UserRoles !!! linked to connected user if empty or to the $user object in param.
     */
    public function getByUser(?User $user = null): array
    {
        if (null === $user) {
            return $this->entityManager->getRepository(UserRole::class)->findBy(['user' => $this->setUser()->getId()]);
        }

        return $this->entityManager->getRepository(UserRole::class)->findBy(['user' => $user->getId()]);
    }

    /**
     * Return array of !!! Company !!! with similar name of Company object.
     */
    public function getByName(Company $company): ?array
    {
        return $this->entityManager->getRepository(Company::class)->findBy(['name' => $company->getName()]);
    }

    /**
     * Return array of !!! Company !!! with similar SIRET of Company object.
     */
    public function getBySiret(Company $company): ?array
    {
        return $this->entityManager->getRepository(Company::class)->findBy(['siret' => $company->getSiret()]);
    }

    public function getCompFromEvent(Event $event): ?array
    {
        $datas = $this->entityManager->getRepository(\App\Entity\Event\CompanyRole::class)->findBy(['event' => $event->getId()]);
        if (isset($datas) && !empty($datas)) {
            foreach ($datas as $data) {
                $result[] = $data->getCompany();
            }

            return $result;
        }

        return null;
    }

    public function getRoleByEvent(Event $event): ?array//TODO tester s'appeler  getByEvent avant
    {
        $result = $this->entityManager->getRepository(\App\Entity\Event\CompanyRole::class)->findBy(['event' => $event->getId()]);

        return $result[0]->getRoles();
    }

    /**
     * @return mixed
     */
    public function getUserRoles(Company $company)
    {
        $result = $this->entityManager->getRepository(\App\Entity\Company\UserRole::class)->findBy(['company' => $company->getId(), 'user' => $this->setUser()->getId()]);

        return $result[0]->getRoles();
    }

    /**
     * Check if an affiliation between User and Event and specific role.
     *
     * @param array $role
     *                    The array role have to be with ONE key
     */
    public function alreadyAffiliated(Company $company, array $role, ?User $user = null): ?UserRole
    {
        if (null === $user) {
            $userRoles = $this->entityManager->getRepository(\App\Entity\Company\UserRole::class)->findBy(['company' => $company->getId(), 'user' => $this->setUser()->getId()]);
        } else {
            $userRoles = $this->entityManager->getRepository(\App\Entity\Company\UserRole::class)->findBy(['company' => $company->getId(), 'user' => $user->getId()]);
        }

        foreach ($userRoles as $userRole) {
            if (in_array(implode($role), $userRole->getRoles(), true)) {
                return $userRole;
            }
        }

        return null;
    }

    /**
     * ShowByUser subFunction.
     */
    public function joinRoles(array $array): array
    {
        foreach ($array as $userRoleShow) {
            foreach ($array as $userRole) {
                if ($userRoleShow->getCompany()->getId() === $userRole->getCompany()->getId()) {
                    $userRoleShow->addRole($userRole->getRoles());
                }
            }
        }

        return $array;
    }

    /**
     * ShowByUser subFunction.
     */
    public function showUniqueCompany(array $array): array
    {
        $temp_array = [];
        $i = 0;
        $key_array = [];

        foreach ($array as $val) {
            if (!in_array($val->getCompany(), $key_array, false)) {
                $key_array[$i] = $val->getCompany();
                $temp_array[$i] = $val;
            }
            ++$i;
        }

        return $temp_array;
    }

    public function cleanDouble(array $array): array
    {
        $temp_array = [];
        $i = 0;
        $key_array = [];

        foreach ($array as $val) {
            if (!in_array($val->getId(), $key_array, false)) {
                $key_array[$i] = $val->getId();
                $temp_array[$i] = $val;
            }
            ++$i;
        }

        return $temp_array;
    }

    /**
     * Render Company\UserRole for DashboardUser template.
     *
     * @return array
     *
     * by merging relation with mutual companys and link role to them
     * ***example**************************
     *
     * foo<--ROLE_COMPANY_ADMIN-->bar
     * foo<--ROLE_ADMIN_EVENTS-->gor
     * foo<--ROLE_ADMIN_EVENTS-->bar
     * foo<--ROLE_COMPANY_ADMIN-->gor
     *
     * => became =>
     *
     * foo<--ROLE_COMPANY_ADMIN--ROLE_ADMIN_EVENTS---->bar
     * foo<--ROLE_COMPANY_ADMIN--ROLE_ADMIN_EVENTS---->gor
     */
    public function showByUser(): array
    {
        $this->companyUserRoles = $this->getByUser();
        $this->companyUserRoles = $this->joinRoles($this->companyUserRoles);

        return $this->companyUserRoles = $this->showUniqueCompany($this->companyUserRoles);
    }
}
