<?php

namespace App\Service\Product;

use App\Entity\Company\Company;
use App\Entity\Investment\Investment;
use App\Entity\Product\Product;
use App\Entity\Setting\Category;
use App\Service\Investment\InvestmentManager;
use Doctrine\ORM\EntityManagerInterface;

class  ProductManager
{
    private $entityManager;
    private $investment;
    private $category;
    private $investmentManager;

    public function __construct(EntityManagerInterface $entityManager, InvestmentManager $investmentManager)
    {
        $this->entityManager = $entityManager;
        $this->investmentManager = $investmentManager;
    }

    public function setInvestment(Investment $investment): void
    {
        $this->investment = $investment;
    }

    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    public function getByInvestment(Investment $investment): ?array
    {
        return $this->entityManager->getRepository(Investment::class)->findBy(['id' => $investment->getId()]);
    }

    public function getByCategory(Category $category): ?array
    {
        return $this->entityManager->getRepository(Category::class)->findBy(['id' => $category->getId()]);
    }

    //TODO add get by id, by reference
    public function getUnique(string $productReference): ?Product
    {
        return $this->entityManager->getRepository(Product::class)->findOneBy(['productReference' => $productReference]);
    }

    /**
     * Return array of Product object from company by default get back all, if second argument is on false it only Product
     * which disponibility on false, else if on true get back all on true.
     */
    public function getAllByCompany(Company $company, ?bool $disponibility = null): ?array
    {
        $investmentsID = $this->investmentManager->getIDByCompany($company);

        if (false === $disponibility) {
            foreach ($investmentsID as $id) {
                $investmentSlug = $this->entityManager->getRepository(Investment::class)->find($id)->getInvestmentSlug();
                $result[$investmentSlug] = $this->entityManager->getRepository(Product::class)->findBy(['investment' => $id, 'disponibility' => false]);
            }

            return $result;
        }
        if (true === $disponibility) {
            foreach ($investmentsID as $id) {
                $investmentSlug = $this->entityManager->getRepository(Investment::class)->find($id)->getInvestmentSlug();
                $result[$investmentSlug] = $this->entityManager->getRepository(Product::class)->findBy(['investment' => $id, 'disponibility' => true]);
            }

            return $result;
        }
        foreach ($investmentsID as $id) {
            $investmentSlug = $this->entityManager->getRepository(Investment::class)->find($id)->getInvestmentSlug();
            $result[$investmentSlug] = $this->entityManager->getRepository(Product::class)->findBy(['investment' => $id]);
        }

        return $result;
    }


    /**
     * Return array of Product object on all platform by default get back all, if second argument is on false it only Product
     *  which disponibility on false, else if on true get back all on true.
     * @param bool|null $disponibility
     * @return array
     */
    public function getAllPlatform(?bool $disponibility = null): array
    {
        if (false === $disponibility) {
            return $this->entityManager->getRepository(Product::class)->findBy(['disponibility' => false]);
        }
        if (true === $disponibility) {
            return $this->entityManager->getRepository(Product::class)->findBy(['disponibility' => true]);
        }

        return $this->entityManager->getRepository(Product::class)->findAll();
    }

    public function generateCatalogueData(?bool $disponibility = null): ?array
    {
        $products = $this->getAllPlatform($disponibility);

        if(!isset($products[0]) || empty($products)){
            return null;
        }

        $data[] = $products[0];
        foreach ($products as $key => $product){//TODO  faire commencer a [1]
            if ($key < 1) {
                continue;
            }

            if($product->getGlobalReference() === end($data)->getGlobalReference()){
                end($data)->setQuantity(end($data)->getQuantity()+1);
            }else{
                $data[] = $product;
            }
        }
        return $data;
    }

    /**
     * Return String reference to global product linked to many product of same type
     * use to refer to image and to create unique reference for each product.
     */
    public function createGlobalReference(Company $company, Product $product): string
    {
        //get array of globalReference unique
        $all = $this->getAllPlatform();
        $allRef = [];
        foreach ($all as $refName) {
            $allRef[] = $refName->getGlobalReference();
        }
        $allRef = array_unique($allRef);

        // Get 3 first letter from company and from product, create reference
        $refArray[] = substr($company->getCompanySlug(), 0, 3); //TODO add secu si nom company inferieur a 3
        $refArray[] = substr($product->getName(), 0, 3); //TODO add secu si nom product inferieur a 3

    // Get category 3 first to add reference
        $categories = $product->getCategories()->getValues();
        foreach ($categories as $category) {
            $refArray[] = substr($category->getCategorySlug(), 0, 3); //TODO add secu si nom category inferieur a 3
        }

        // Add array length of $allRef + 1

        $globalReference = implode($refArray).(count($allRef) + 1);

        // Control if not in $allRef and increment if it's in
        $validate = false;
        $i = 0;
        while (false === $validate) {
            ++$i;
            if (in_array($globalReference, $allRef, true)) {
                $globalReference .= $i;
            } else {
                $validate = true;
            }
        }

        return $globalReference;
    }

    /**
     * Used to register similar products in BDD by using a generic object.
     * @param Product $generic
     * @param Investment $investment
     * @return array
     */
    public function addSimilar(Product $generic, Investment $investment): array
    {
        $quantity = $generic->getQuantity();
        $products = [];

        for ($i = 1; $i <= $quantity; ++$i) {
            $product = new Product();
            $product->setName($generic->getName());
            $product->setDescription($generic->getDescription());
            $product->setDisponibility($generic->getDisponibility());
            $product->setQuantity(1);
            $product->setGlobalReference($generic->getGlobalReference());
            foreach ($generic->getTagTypeLimitations() as $tagTypeLimitation) {
                $product->addTagTypeLimitation($tagTypeLimitation);
            }
            $product->setGeograficLimitation($generic->getGeograficLimitation());
            $product->setBigCost($generic->getBigCost());
            $product->setDepositValue($generic->getDepositValue());
            $product->setTvaOption($generic->getTvaOption());
            $product->setWeight($generic->getWeight());
            $product->setLength($generic->getLength());
            $product->setHeight($generic->getHeight());
            $product->setWidth($generic->getWidth());
            $product->setTransportVolume($generic->getTransportVolume());
            $product->setPreparedBy($generic->getPreparedBy());
            $product->setCurrent($generic->getCurrent());
            $product->setPower($generic->getPower());
            $product->setSurfaceMaterial($generic->getSurfaceMaterial());
            $product->setControlDocumentation($generic->getControlDocumentation());
            $product->setState($generic->getState());
            $product->setLine($generic->getLine());
            $product->setStockpile($generic->getStockpile());
            $product->setInvestment($investment);
            $product->setDisponibility(1);
            foreach ($generic->getCategories() as $category) {
                $product->addCategory($category);
            }
            foreach ($generic->getImages() as $image) {
                $product->addImage($image);
            }
            $product->setTimeCoefficient($generic->getTimeCoefficient());

            $this->entityManager->persist($product);
            $this->entityManager->flush();

            $products[] = $product;
        }

        $this->entityManager->remove($generic);
        $this->entityManager->flush();

        return $products;
    }
}
