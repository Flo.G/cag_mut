<?php

namespace App\Service\Product;

use App\Entity\Product\Product;
use App\Entity\Setting\Category;
use Doctrine\ORM\EntityManagerInterface;

class TagTypeManager
{
    private $tagTypeLimitations;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->tagTypeLimitations = [];
    }

    public function getByCategory(Category $category): ?array
    {
        foreach ($category->getProductTagTypeLimitations()->getValues() as $value) {
            $this->tagTypeLimitations[] = $value;
        }

        return $this->tagTypeLimitations;
    }

    public function getByCategories(?array $categories): ?array
    {
        if (null !== $categories) {
            foreach ($categories as $category) {
                $this->getByCategory($category);
            }

            return $this->tagTypeLimitations;
        }

        return null;
    }

    public function getFromProduct(Product $product): ?array
    {
        $categories = $product->getCategories()->getValues();
        if (null !== $categories) {
            return $this->getByCategories($categories);
        }

        return null;
    }
}
