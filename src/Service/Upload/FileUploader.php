<?php

namespace App\Service\Upload;

use App\Entity\Company\Company;
use App\Entity\User\User;
use Gedmo\Sluggable\Util\Urlizer;
use Liip\ImagineBundle\Service\FilterService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private string $uploadsPath;
    private FilterService $filterService;
    private Filesystem $filesystem;
    //private const AVATAR_IMAGE = 'avatar';
    //private const LOGO_IMAGE = 'logo';
    //public const BANNER_IMAGE = 'banner';

    public function __construct(string $uploadsPath, FilterService $filterService, Filesystem $filesystem)
    {
        $this->uploadsPath = $uploadsPath;
        $this->filterService = $filterService;
        $this->filesystem = $filesystem;
    }

    /**
     * FIRST create the sub file class with the parameters
     * - Don't forget the the FILTER const in your file entity, it correspond to the filter name in config/packages/liip_imagine.yaml
     * - Don't forget the the URI const in your file entity, it correspond to the mappings in config/packages/vich_uploader.yaml
     *       ->it will call the namer services
     * - Don't forget the the update function in your class,
     * ***********code*******************************************************************************
     *     public const DESCRIPTION = 'foo';
     *     public const FILTER = 'foo';
     *     public const URI = 'foo_image';
     *     public const PATH = '/uploads/images/foos';.
     *
     *      Vich\UploadableField(mapping="foo_image", fileNameProperty="imageName")
     *      Assert\File(
     *          maxSize = "1024k",
     *          mimeTypesMessage = "Please upload a file less than 1024k, we would love you."
     *      )
     *      private $imageFile;
     *
     *     public function setImageFile(File $imageName = null): void
     *      {
     *        $this->imageFile = $imageName;
     *         if ($imageName) {
     *             $this->updatedAt = new DateTime('now');
     *         }
     *       }
     *
     *
     * ************************************************************************************************************
     * Give a look on Entity/File/Image/Banner.php
     *
     *
     * //Check if already file register or not
     **************code****************************************
     *  if ($company->getFoo()) {
     *      $foo = $company->getFoo();
     *  } else {
     *      $foo = new Foo();
     * }
     *
     ***********************************************************
     *
     * //The form to call is Form/BaseType/VichAddImageType.php
     *******************code******************************************
     * $form = $this->createForm(VichAddImageType::class, $foo);
     *
     * *********************************************************
     *
     * //Use the filter after the manager persist
     ***********************code**************************************
     *
     *         if ($form->isSubmitted() && $form->isValid()) {
     *              $logo->setCompany($company);
     *              $logo->setFilePath($logo::PATH);        <=!!!
     *              $logo->setDescription($logo::DESCRIPTION);      <=!!!
     *              $company->setLogo($logo);
     *              $manager->persist($logo);
     *              $fileUploader->filter($logo);       <=!!!
     *              $manager->flush();
     *
     * @param $file
     */
    public function filter($file): void
    {
        $path = $file->getFilePath().'/'.$file->getImageName();
        $ath = trim($path, '/');

        $this->filterService->getUrlOfFilteredImage($path, $file::FILTER);
        // TODO get "media/cache" from liip imagine

        $this->filesystem->copy('media/cache/'.$file::FILTER.$path, $ath, true);
        $this->filesystem->remove('media/cache/'.$file::FILTER.$path);
    }

    /**
     * Dont use it , use the vich and liip way.
     */
    public function uploadAvatar(UploadedFile $uploadedFile, User $user): string
    {
        $destination = $this->uploadsPath.'/'.self::AVATAR_IMAGE;

        $originalFileName = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);

        //KEEP as exemple of uniqueid renaming
        //$newFileName = Urlizer::urlize(self::AVATAR_IMAGE).'-'.$user->getUserName().'-'.uniqid().'.'.$uploadedFile->guessExtension();
        $newFileName = Urlizer::urlize(self::AVATAR_IMAGE).'-'.$user->getUserName().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move($destination, $newFileName);
        $this->filterService->getUrlOfFilteredImage('uploads/avatar/'.$newFileName, self::AVATAR_IMAGE);
        $this->filesystem->copy('media/cache/avatar/uploads/avatar/'.$newFileName, 'uploads/avatar/'.$newFileName, true);
        $this->filesystem->remove('media/cache/avatar/uploads/avatar/'.$newFileName);

        return $newFileName;
    }

    /**
     * Dont use it , use the vich and liip way.
     */
    public function uploadLogo(UploadedFile $uploadedFile, Company $company): string
    {
        $destination = $this->uploadsPath.'/'.self::LOGO_IMAGE;

        $originalFileName = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);

        $newFileName = Urlizer::urlize(self::LOGO_IMAGE).'-'.$company->getSlug().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move($destination, $newFileName);
        $this->filterService->getUrlOfFilteredImage('uploads/logo/'.$newFileName, self::LOGO_IMAGE);
        $this->filesystem->copy('media/cache/logo/uploads/logo/'.$newFileName, 'uploads/logo/'.$newFileName, true);
        $this->filesystem->remove('media/cache/logo/uploads/logo/'.$newFileName);

        return $newFileName;
    }

    /**
     * Dont use it , use the vich and liip way.
     */
    public function uploadBanner(UploadedFile $uploadedFile, Company $company): string
    {
        $destination = $this->uploadsPath.'/'.self::BANNER_IMAGE;

        $originalFileName = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);

        $newFileName = Urlizer::urlize(self::BANNER_IMAGE).'-'.$company->getSlug().'.'.$uploadedFile->guessExtension();

        $uploadedFile->move($destination, $newFileName);
        $this->filterService->getUrlOfFilteredImage('uploads/banner/'.$newFileName, self::BANNER_IMAGE);
        $this->filesystem->copy('media/cache/banner/uploads/banner/'.$newFileName, 'uploads/banner/'.$newFileName, true);
        $this->filesystem->remove('media/cache/banner/uploads/banner/'.$newFileName);

        return $newFileName;
    }
}
