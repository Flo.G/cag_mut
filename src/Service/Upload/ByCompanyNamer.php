<?php

namespace App\Service\Upload;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class ByCompanyNamer implements NamerInterface
{
    /**
     * own service file namer for vichuploader from company slug
     * https://github.com/dustin10/VichUploaderBundle/blob/master/docs/namers.md.
     *
     * @param object $object
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $name = $object->getCompany()->getCompanySlug();
        $file = $mapping->getFile($object);
        if (null === $file) {
            return $name;
        }
        $originalName = $file->getClientOriginalName();
        $extension = \strtolower(\pathinfo($originalName, PATHINFO_EXTENSION));

        if (is_string($extension) && '' !== $extension) {
            return sprintf('%s.%s', $name, $extension);
        }

        return $originalName;
    }
}
