<?php

namespace App\Service\Upload;

use Symfony\Component\Security\Core\Security;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class ByUserNamer implements NamerInterface
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * own service file namer for vichuploader from user connected slug
     * https://github.com/dustin10/VichUploaderBundle/blob/master/docs/namers.md.
     *
     * @param object $object
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $file = $mapping->getFile($object);
        $originalName = $file->getClientOriginalName();
        $extension = \strtolower(\pathinfo($originalName, PATHINFO_EXTENSION));
        $name = $this->security->getUser()->getUserName();

        if (is_string($extension) && '' !== $extension) {
            $name = sprintf('%s.%s', $name, $extension);

            return $name;
        }

        return $originalName;
    }
}
