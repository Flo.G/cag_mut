<?php

namespace App\Service\Upload;



use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

class ForCategoryImageNamer implements NamerInterface
{
    /**
     * own service file namer for vichuploader from company slug
     * https://github.com/dustin10/VichUploaderBundle/blob/master/docs/namers.md.
     *
     * @param object $object
     */

    public function name($object, PropertyMapping $mapping): string
    {
        $categorySlug = $object->getCategories()->getCategorySlug();

        $file = $mapping->getFile($object);

        $originalName = $file->getClientOriginalName();
        $extension = \strtolower(\pathinfo($originalName, PATHINFO_EXTENSION));

        return $categorySlug.'.'.$extension;
    }
}