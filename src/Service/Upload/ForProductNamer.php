<?php

namespace App\Service\Upload;

use App\Entity\Product\Product;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;


class ForProductNamer implements NamerInterface
{
    /**
     * own service file namer for vichuploader from company slug
     * https://github.com/dustin10/VichUploaderBundle/blob/master/docs/namers.md.
     *
     * Cagibig-mutualisation-materiel-REFERENCEPRODUIT-nom-du-produit-enregistre-NUMEROIMAGE
     * ( exemple = Cagibig-mutualisation-materiel-287093-scene-modulaire-PMR-01
     * (02, 03, 04, etc en cas de plusieurs images))
     *
     * @param object $object
     */
    public function name($object, PropertyMapping $mapping): string
    {
        $companySlug = $object->getCompany()->getCompanySlug();
        $products = $object->getProductImages();
        //TODO use arrayCollection
        $products = end($products);
        $product = end($products);

        $productName = str_replace(' ', '-', $product->getName());

        $productReference = str_replace(' ', '-', $product->getGlobalReference());

        $imageName = $companySlug.''.$productName.'-'.$productReference;
        $file = $mapping->getFile($object);
        $originalName = $file->getClientOriginalName();
        $extension = \strtolower(\pathinfo($originalName, PATHINFO_EXTENSION));




        if (is_string($extension) && '' !== $extension) {
            $number = count( $product->getImages()->getValues()) +1;
            return sprintf('%s.%s.%s', $imageName,$number , $extension);

        }

        return $originalName;
    }
}
