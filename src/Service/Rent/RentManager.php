<?php

namespace App\Service\Rent;

use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Entity\Event\Stage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;


class RentManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Return an array of Rents linked to the array of Event by stages, if there is double they are merged.
     * @param array|null $events
     * @return array|null
     */
    public function getByEvents(?array $events): ?array
    {
        $data = [];
        if (isset($events) && !empty($events)) {
            foreach ($events as $event) {
                $stages = $this->entityManager->getRepository(Stage::class)->findBy(['event' => $event->getId()]);
                foreach ($stages as $stage) {
                    $data = array_merge($data, $this->entityManager->getRepository(\App\Entity\Rent\Rent::class)->findBy(['stage' => $stage->getId()]));
                }
            }

            return $data;
        }

        return null;
    }

    /**
     * TODO doc
     * @param array|null $events
     * @return array|null
     */
    public function internal(?array $events): ?array
    {
        $data = [];
        $rents = $this->getByEvents($events);

        if( $rents !== null){
            foreach ( $rents as $rent){
                if ($rent->getInternRent()){
                    $data[] = $rent;
                }
            }
            return $data;
        }
        return null;
    }

    /**
     * TODO doc
     * @param Company $company
     * @return array
     */
    public function in(Company $company): array
    {
        $contracts = $company->getBorrowerContracts()->toArray();
        $rents = [];
        if (isset($contracts) && !empty($contracts)) {
            foreach ($contracts as $contract) {
                $rents[] = $contract->getRent();
            }
        }
        return $rents;
    }

    /**
     * TODO doc
     * @param Company $company
     * @return array
     */
    public function out(Company $company): array
    {
        $contracts = $company->getlenderContracts()->toArray();
        $rents = [];
        if (isset($contracts) && !empty($contracts)) {
            foreach ($contracts as $contract) {
                $rents[] = $contract->getRent();
            }
        }
        return $rents;
    }
}
