<?php

namespace App\Service\Rent;

use App\Entity\Rent\Contract;
use App\Entity\Rent\Rent;
use App\Service\Provider\CoefficientCalculProvider;

class ContractManager
{

    private $contract;
    private $coefficientCalculProvider;

    public function construct(CoefficientCalculProvider $coefficientCalculProvider): void
    {
        $this->contract = new Contract();
        $this->coefficientCalculProvider = $coefficientCalculProvider;
    }

    public function generate(Rent $rent): void
    {
        dd($this->coefficientCalculProvider->evaluate($rent));
    }
}
