<?php

namespace App\Service\Provider;

use App\Repository\Product\LineRepository;
use App\Repository\Product\StateRepository;
use Money\Money;

class BigValueProvider
{
    public const RATE = 1000000;

    private LineRepository $lineRepository;
    private StateRepository $stateRepository;

    public function __construct(LineRepository $lineRepository, StateRepository $stateRepository)
    {
        $this->lineRepository = $lineRepository;
        $this->stateRepository = $stateRepository;
    }

    /**
     * Retribution en big = arround Sup (Prix achat * Taux d'utilisation annuel / Durée d'amortissement * Valeur de l'état)
     *    ceil($result)   =             ($depositValue * $lineRate      /           $lineTime   * $stateRate.
     */
    public function value(Money $depositValue, int $line, int $state): int
    {
        $line = $this->lineRepository->find($line);

        $lineRate = $line->getPercentage();
        $lineTime = $line->getAmortizationYear();
        $stateRate = $this->stateRepository->find($state)->getValue();

        $result = $depositValue->multiply($lineRate)->divide($lineTime)->multiply($stateRate);

        return (int) ceil($result->getAmount() / self::RATE);
    }
}
