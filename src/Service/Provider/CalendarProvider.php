<?php

namespace App\Service\Provider;

use App\Entity\Rent\Rent;
use App\Entity\User\User;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

class CalendarProvider
{
    private $translator;
    private $entityManager;

    public function __construct(TranslatorInterface $translator, EntityManagerInterface $entityManager)
    {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
    }

    /**
     *  Format events data in good json array understandable for fullCallendar for standard usage,
     *  without rentIn and rentOut
     *  $events is array of object Event
     *  return array $data['resources']json and $data['events']json format for fullCalendar.
     *
     * @param $events
     * @return array
     */
    public function eventData($events): array
    {
        foreach ($events as $event) {
            $eventCal[] = [
                'id' => $event->getId(),
                'resourceId' => $event->getId(),
                'title' => $event->getName(),
                'description' => $event->getDescription(),
                'start' => $event->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $event->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#FFE128',
            ];
            $resourceCal[] = [
                'id' => $event->getId(),
                'resourceId' => $event->getId(),
                'title' => $event->getName(),
                'description' => $event->getDescription(),
                'start' => $event->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $event->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#FFE128',
            ];
            $resourceCal[] = [
                'id' => 'rent' . $event->getId(),
                'parentId' => $event->getId(),
                'title' => $this->translator->trans('Rents for ') . $event->getName(),
            ];
        }
        $resourceCal[] = [
            'id' => 'rent_out',
            'title' => $this->translator->trans('Rents out'),
        ];

        $data['resources'] = json_encode($resourceCal);

        $data['events'] = json_encode($eventCal);

        return $data;
    }

    /**
     *  Format events data in in good json array understandable for full callendar for ressource usage,.
     *
     *  $events is array of object Event,
     *  rentsIn is array of object Rent linked to Event by staged, show the rentIn linked to there event //TODO link from stage
     *  rentsOut is array of object Rent linked to company by product>investment, it show the rentOut linked to there event
     *
     *  return array $data['resources']json and $data['events']json format for fullCalendar
     *  $data [
     *  "resources" => "[{"id":296,"resourceId":296,"title":"event_2","start":"2020-12-12 00:00:00","end":"2021-12-12 00:00:00","backgroundColor":"blue"},
     *                  {"id":"rent296","parentId":296,"title":"Rent in event_2"},
     *                  {"id":297,"resourceId":297,"title":"event_3","start":"2021-05-15 00:00:00","end":"2021-07-12 00:00:00","backgroundColor":"blue"},
     *                  {"id":"rent297","parentId":297,"title":"Rent in event_3"},
     *                  {"id":298,"resourceId":298,"title":"event_4","start":"2020-11-20 00:00:00","end":"2021-02-14 00:00:00","backgroundColor":"blue"},
     *                  {"id":"rent298","parentId":298,"title":"Rent in event_4"},
     *                  {"id":"rent_out","title":"rent Out"}] "
     *
     *  "events" =>     "[{"id":296,"resourceId":296,"title":"event_2","start":"2020-12-12 00:00:00","end":"2021-12-12 00:00:00","backgroundColor":"blue"},
     *                  {"id":297,"resourceId":297,"title":"event_3","start":"2021-05-15 00:00:00","end":"2021-07-12 00:00:00","backgroundColor":"blue"},
     *                  {"id":298,"resourceId":298,"title":"event_4","start":"2020-11-20 00:00:00","end":"2021-02-14 00:00:00","backgroundColor":"blue"},
     *                  {"id":105,"resourceId":"rent296","title":"rent_event_2","start":"2020-12-12 00:00:00","end":"2021-12-12 00:00:00","backgroundColor":"yellow","textColor":"green"},
     *                  {"id":106,"resourceId":"rent296","title":"rent_event_2","start":"2020-12-12 00:00:00","end":"2021-12-12 00:00:00","backgroundColor":"yellow","textColor":"green"},
     *                  {"id":107,"resourceId":"rent296","title":"rent_event_2","start":"2020-12-12 00:00:00","end":"2021-12-12 00:00:00","backgroundColor":"yellow","textColor":"green"},
     *                  {"id":108,"resourceId":"rent297","title":"rent_event_3","start":"2021-05-15 00:00:00","end":"2021-07-12 00:00:00","backgroundColor":"yellow","textColor":"green"},
     *                  {"id":109,"resourceId":"rent298","title":"rent_event_4","start":"2020-11-20 00:00:00","end":"2021-02-14 00:00:00","backgroundColor":"yellow","textColor":"green"}
     *
     * @param array $events
     * @param array $rentsIn
     * @param array $rentsOut
     * @param array $rentsInternal
     * @return array
     */
    public function fullData(Array $events, Array $rentsIn, Array $rentsOut, Array $rentsInternal): array
    {
    // if no event to show
        if (!isset($events) || empty($events)) {
            $eventCal[] = [
                'id' => '0',
                'resourceId' => '0',
                'title' => '0',
            ];

            $resourceCal[] = [
                'id' => 'rent_out',
                'title' => $this->translator->trans('Rents out'),
            ];

            $data['resources'] = json_encode($resourceCal);
            $data['events'] = json_encode($eventCal);

            return $data;
        }
        // TODO missing data sur $rentOut

    // if event to show
        // Table that send the event data
        foreach ($events as $event) {
            $eventCal[] = [
                'id' => $event->getId(),
                'resourceId' => $event->getId(),
                'title' => $event->getName(),
                'description' => $event->getDescription(),
                'start' => $event->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $event->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#FFE128',
                'textColor' => 'black',
                'stage' => $event->getStages()->getValues()[0]->getName(),
                'resourceType' => 'event'
            ];

            $resourceCal[] = [
                'id' => $event->getId(),
                'resourceId' => $event->getId(),
                'title' => $event->getName(),
                'description' => $event->getDescription(),
                'start' => $event->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $event->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#FFE128',
                'textColor' => 'black',
                'stage' => $event->getStages()->getValues()[0]->getName(),
            ];

            $resourceCal[] = [
                'id' => 'rent'.$event->getId(),
                'parentId' => $event->getId(),
                'title' => $this->translator->trans('Rents for ').$event->getName(),
            ];
        }
        $resourceCal[] = [
            'id' => 'rent_out',
            'title' => $this->translator->trans('Rents out'),
        ];

    // Table that send the rentIn data
        foreach ($rentsIn as $rentIn) {
            $eventCal[] = [
                'id' => $rentIn->getId(),
                'resourceId' => 'rent' . $rentIn->getStage()->getEvent()->getId(),
                'title' => $rentIn->getTitle(),
                'description' => $rentIn->getStage()->getEvent()->getDescription(),
                'start' => $rentIn->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $rentIn->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#FF0000',
                'textColor' => 'black',
                'stage' => $rentIn->getStage()->getName(),
                'productsName' => $this->productName($rentIn),
                'productsQuantity' => $this->productQuantity($rentIn),
                'userPhones' => $this->getRentPhones($rentIn),
                'userFirstName' => $this->getRentUserFirstName($rentIn),
                'userEmail' => $this->getRentUserMail($rentIn),
                'userLastName' => $this->getRentUserLastName($rentIn),
                'resourceType' => 'rent_in'
            ];
        }

    // Table that send the rentOut data
        foreach ($rentsOut as $rentOut) {
            $eventCal[] = [
                'id' => $rentOut->getId(),
                'resourceId' => 'rent_out',
                'title' => $rentOut->getTitle(),
                'description' => $rentOut->getStage()->getEvent()->getDescription(),
                'start' => $rentOut->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $rentOut->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#287AB1',
                'textColor' => 'black',
                'stage' => $rentOut->getStage()->getName(),
                'productsName' => $this->productName($rentOut),
                'productsQuantity' => $this->productQuantity($rentOut),
                'userPhones' => $this->getRentPhones($rentOut),
                'userFirstName' => $this->getRentUserFirstName($rentOut),
                'userEmail' => $this->getRentUserMail($rentOut),
                'userLastName' => $this->getRentUserLastName($rentOut),
                'resourceType' => 'rent_out'
            ];
        }

        // Table that send the rentInternal data
        foreach ($rentsInternal as $rentInternal) {
            $eventCal[] = [
                'id' => $rentInternal->getId(),
                'resourceId' => 'rent' . $rentInternal->getStage()->getEvent()->getId(),// TODO get id
                'title' => $rentInternal->getTitle(),
                'description' => $rentInternal->getStage()->getEvent()->getDescription(),
                'start' => $rentInternal->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $rentInternal->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => 'black',
                'textColor' => 'green',
                'stage' => $rentInternal->getStage()->getName(),
                'productsName' => $this->productName($rentInternal),
                'productsQuantity' => $this->productQuantity($rentInternal),
                'resourceType' => 'rent_internal'
            ];
        }

        $data['resources'] = $this->jsonEncode($resourceCal);
        $data['events'] = $this->jsonEncode($eventCal);

        return $data;
    }

    /**
     * @param $events
     * @param $rentsIn
     * @param $rentsOut
     * @return array
     */
    public function fromRentData($events, $rentsIn, $rentsOut): array
    {
        // Condition if there is no events to display
        if (!isset($events) || empty($events)) {
            $eventCal[] = [
                'id' => '0',
                'resourceId' => '0',
                'title' => '0',
            ];

            $resourceCal[] = [
                'id' => 'rent_out',
                'title' => $this->translator->trans('Rents out'),
            ];

            $data['resources'] = json_encode($resourceCal);
            $data['events'] = json_encode($eventCal);

            return $data;
        }

        // Condition if there is events to display
        foreach ($events as $event) {
            $eventCal[] = [
                'id' => $event->getId(),
                'resourceId' => $event->getId(),
                'title' => $event->getName(),
                'description' => $event->getDescription(),
                'start' => $event->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $event->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#FF0000',
                'stage' => $event->getStages()->getValues()[0]->getName(),
                'resourceType' => 'event'
            ];

            $resourceCal[] = [
                'id' => $event->getId(),
                'resourceId' => $event->getId(),
                'title' => $event->getName(),
                'description' => $event->getDescription(),
                'start' => $event->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $event->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#FF0000',
                'stage' => $event->getStages()->getValues()[0]->getName(),
            ];

            $resourceCal[] = [
                'id' => 'rent' . $event->getId(),
                'parentId' => $event->getId(),
                'title' => $this->translator->trans('Rents for ') . $event->getName(),
            ];

            $resourceCal[] = [
                'id' => 'rent_out' . $event->getId(),
                'parentId' => $event->getId(),
                'title' => $this->translator->trans('Rents out') . $event->getName(),
            ];
        }


        // Table that send the rentIn data
        $data['resources'] = json_encode($resourceCal);
        foreach ($rentsIn as $rentIn) {
            $eventCal[] = [
                'id' => $rentIn->getId(),
                'resourceId' => 'rent' . $rentIn->getStage()->getEvent()->getId(),
                'title' => $rentIn->getTitle(),
                'description' => $rentIn->getStage()->getEvent()->getDescription(),
                'start' => $rentIn->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $rentIn->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => '#287AB1',
                'textColor' => 'black',
                'stage' => $rentIn->getStage()->getName(),
                'productsName' => $this->productName($rentIn),
                'productsQuantity' => $this->productQuantity($rentIn),
                'userPhones' => $this->getRentPhones($rentIn),
                'userFirstName' => $this->getRentUserFirstName($rentIn),
                'userEmail' => $this->getRentUserMail($rentIn),
                'userLastName' => $this->getRentUserLastName($rentIn),
                'resourceType' => 'rent_in'

            ];
        }

        // Table that send the rentOut data
        $data['resources'] = json_encode($resourceCal);
        foreach ($rentsOut as $rentOut) {
            $eventCal[] = [
                'id' => $rentOut->getId(),
                'resourceId' => 'rent_out' . $rentOut->getStage()->getEvent()->getId(),
                'title' => $rentOut->getTitle(),
                'description' => $rentOut->getStage()->getEvent()->getDescription(),
                'start' => $rentOut->getDateStart()->format('Y-m-d H:i:s'),
                'end' => $rentOut->getDateEnd()->format('Y-m-d H:i:s'),
                'backgroundColor' => 'black',
                'textColor' => 'orange',
                'stage' => $rentOut->getStage()->getName(),
                'productsName' => $this->productName($rentOut),
                'productsQuantity' => $this->productQuantity($rentOut),
                'userPhones' => $this->getRentPhones($rentOut),
                'userFirstName' => $this->getRentUserFirstName($rentOut),
                'userEmail' => $this->getRentUserMail($rentOut),
                'userLastName' => $this->getRentUserLastName($rentOut),
                'resourceType' => 'rent_out'
            ];

        }
        $data['events'] = json_encode($eventCal );
        return $data;
    }

    /**
     * Give product quantity
     * @param Rent $rent
     * @return array
     */
    public function productQuantity(Rent $rent): array
    {
        $products = $rent->getProducts();
        $productsGlRef = [];
        foreach ($products as $product) {
            $productsGlRef[] = $product->getGlobalReference();
        }

        /*It allows to count the number of times the value returns, it return un this style {"GRef" => "counts"} */
        $arrayQuantity = array_count_values($productsGlRef);
        /* It allows to retrieve just the values (counts) without the keys */
        return array_values($arrayQuantity);
    }

    /**
     * Give product names
     * @param Rent $rent
     * @return array
     */
    public function productName(Rent $rent): array
    {
        $products = $rent->getProducts();
        $productsName = [];
        foreach ($products as $product) {
            $productsName [] = $product->getName();
        }
        /* remove duplicates */
        return array_unique($productsName);
    }

    /**
     * @param Rent $rent
     * @return string
     */
    public function getRentPhones(Rent $rent): string
    {
        $phoneNumber = "Phone number is not available";
        $usernameRent = $rent->getCreatedBy();
        $userRent = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $usernameRent]);
        $userPhones = $userRent->getPhones()->getvalues();
        $companyPhones = $rent->getContract()->getBorrower()->getPhones()->getValues();

        if ($userPhones) {
            foreach ($userPhones as $phone) {
                $phoneNumber = $this->getPhones($userPhones);
            }
        } elseif ($companyPhones) {
            $phoneNumber = $this->getPhones($companyPhones);
        }
        return $phoneNumber;
    }

    /**
     * @param array $phones
     * @return string
     */
    public function getPhones(Array $phones):string
    {
        $result = $phones[0]->getPhoneNumber()->getNationalNumber();
        foreach ($phones as $phone) {   //if the user or the Company have two number, one personal and one professional, it will show only the pro
            if ($phone->getType() === "pro") {
                $result = $phone->getPhoneNumber()->getNationalNumber();
                //it allows to keep the pro number, if it is first in the list
            }
        }
        return $result;
    }

    /**
     * @param Rent $rent
     * @return mixed
     */
    public function getRentUserMail(Rent $rent)
    {
        $usernameRent = $rent->getCreatedBy();
        $userRent = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $usernameRent]);

        return $userRent->getEmail();
    }

    public function getRentUserFirstName(Rent $rent)
    {
        $usernameRent = $rent->getCreatedBy();
        $userRent = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $usernameRent]);

        return $userRent->getFirstName();
    }

    public function getRentUserLastName(Rent $rent)
    {
        $usernameRent = $rent->getCreatedBy();
        $userRent = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $usernameRent]);

        return $userRent->getLastName();
    }

    public function jsonEncode(array $data)
    {
        return json_encode($data, JSON_HEX_QUOT | JSON_HEX_APOS | JSON_UNESCAPED_SLASHES);
    }

}
