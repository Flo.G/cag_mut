<?php


namespace App\Service\Provider;


use App\Entity\Rent\Contract;
use App\Entity\Rent\Rent;
use App\Entity\Setting\TimeCoefficient;
use App\Enum\CoefficientCalcul\CoefficientCalcul;
use App\Repository\Product\LineRepository;
use App\Repository\Product\StateRepository;
use Money\Money;

class CoefficientCalculProvider
{

    public function evaluate(Rent $rent): int
    {
        $total = (float)0;
        $duration = $rent->getDateStart()->diff($rent->getDateEnd(), true);

        foreach ($rent->getProducts()->getValues() as $product) {
            $dailyCost = $product->getBigCost();
            $timeCoefficient = $product->getTimeCoefficient();
            $calculFunction = $timeCoefficient->getCalculFunction();
            $data = $timeCoefficient->getData();

            switch ($calculFunction) {
                case 0:
                    $productCost = $this->linear($duration, $dailyCost, $data[0], $data[1]);
                    break;
                case 1:
                    $productCost = $this->logarithm($duration, $dailyCost, $data[0], $data[1]);
                    break;
                case 2:
                    $productCost = $this->stepped($duration, $dailyCost, $data[0], $data[1]);
                    break;
                default:
                    $productCost = 0;// TODO return error no calculFunction found
            }
            $total += $productCost;
        }
        return $total;
    }

    public function linear($duration, $dailyCost, $startDay, $coefX): float
    {
        return ((($duration - $startDay) * $coefX * $dailyCost) / 100 ) + ($startDay * $dailyCost);
    }

    public function logarithm($duration, $dailyCost, $startDay, $coefX, $coefY): float
    {
        // TODO logarithm
        return ((($duration - $startDay) * $coefX * $dailyCost) / 100 ) + ($startDay * $dailyCost);
    }

    public function stepped($duration, $dailyCost, $startDay, $coefX, $step): float
    {
        // TODO stepped
        $iterate = ($duration- $startDay)/$step;
        return ((($iterate) * $coefX * $dailyCost) / 100 ) + ($startDay * $dailyCost);
    }
}