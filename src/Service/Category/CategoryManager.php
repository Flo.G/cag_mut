<?php

namespace App\Service\Category;

use App\Entity\Investment\Investment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class CategoryManager
{
    private EntityManagerInterface $entityInterface;
    private Security $security;
    private Investment $investment;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
    }
}
