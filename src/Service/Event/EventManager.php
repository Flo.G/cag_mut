<?php

namespace App\Service\Event;

use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Entity\Event\UserRole;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class EventManager
{
    private $company;
    private $entityManager;
    private $security;
    private $events;
    private $eventUserRoles;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function setCompany(Company $company): void
    {
        $this->company = $company;
    }

    public function setUser()
    {
        return $this->security->getUser();
    }

    public function getByRents(array $rentsIn, array $rentsOut): array
    {
        $events = [];
        if ($rentsIn) {
            foreach ($rentsIn as $rentIn) {
                $events [] = $rentIn->getStage()->getEvent();
            }
        }
        if ($rentsOut) {
            foreach ($rentsOut as $rentOut) {
                $events [] = $rentOut->getStage()->getEvent();
            }
        }

        return array_unique($events);
    }

    private function getEvents(Event $event)
    {
        $this->events->contains($event);
    }

    /**
     * Return array of !!! Event\UserRoles !!! linked to connected user if empty or to the $user object in param.
     */
    public function getByUser(?User $user = null): array
    {
        if (null === $user) {
            return $this->entityManager->getRepository(UserRole::class)->findBy(['user' => $this->setUser()->getId()]);
        }

        return $this->entityManager->getRepository(UserRole::class)->findBy(['user' => $user->getId()]);
    }

    /**
     * @return mixed
     */
    public function getUserRoles(Event $event)
    {
        $result = $this->entityManager->getRepository(\App\Entity\Event\UserRole::class)->findBy(['event' => $event->getId(), 'user' => $this->setUser()->getId()]);

        return $result[0]->getRoles();
    }

    /**
     * Check if an affiliation between User and Event and specific role.
     *
     * @param array $role
     *                    The array role have to be with ONE key
     */
    public function alreadyAffiliated(Event $event, array $role, User $user): ?UserRole
    {
        if (null === $user) {
            $userRoles = $this->entityManager->getRepository(UserRole::class)->findBy(['event' => $event->getId(), 'user' => $this->setUser()->getId()]);
        } else {
            $userRoles = $this->entityManager->getRepository(UserRole::class)->findBy(['event' => $event->getId(), 'user' => $user->getId()]);
        }

        foreach ($userRoles as $userRole) {
            if (in_array(implode($role), $userRole->getRoles(), true)) {
                return $userRole;
            }
        }

        return null;
    }

    /**
     * Return array of !!! CompanyRoles !!! linked to this company.
     */
    public function getByCompany(Company $company): array
    {
        return $this->entityManager->getRepository(\App\Entity\Event\CompanyRole::class)->findBy(['company' => $company->getId()]);
    }

    /**
     * Return array of Event from the array of RelationRole give as arguments.
     */
    public function filTerE(array $relations): array
    {
        $result = [];
        foreach ($relations as $relation) {
            $result[] = $relation->getEvent();
        }

        return $result;
    }

    public function getByName(string $name): array
    {
        return $this->entityManager->getRepository(Event::class)->findBy(['name' => $name]);
    }

    /**
     * Return an array of Event which are only linked by the user And the company we are sluged to.
     */
    public function getByCompanyNUser(Company $company): ?array
    {
        $byCompany = $this->getByCompany($company);
        foreach ($byCompany as $event) {
            $byCompanyEvent[] = $event->getEvent()->getEventSlug();
        }

        $byUser = $this->getByUser();
        foreach ($byUser as $event) {
            $byUserEvent[] = $event->getEvent()->getEventSlug();
        }

        if (isset($byUserEvent, $byCompanyEvent)) {
            $intersect = array_intersect($byUserEvent, $byCompanyEvent);

            return $this->entityManager->getRepository(\App\Entity\Event\Event::class)->findBy(['eventSlug' => $intersect]);
        }

        return null;
    }

    /**
     * ShowByUser subFunction.
     */
    public function joinRoles(array $array): array
    {
        foreach ($array as $userRoleShow) {
            foreach ($array as $userRole) {
                if ($userRoleShow->getEvent()->getId() === $userRole->getEvent()->getId()) {
                    $userRoleShow->addRole($userRole->getRoles());
                }
            }
        }

        return $array;
    }

    /**
     * ShowByUser subFunction.
     */
    public function showUniqueEvent(array $array): array
    {
        $temp_array = [];
        $i = 0;
        $key_array = [];

        foreach ($array as $val) {
            if (!in_array($val->getEvent(), $key_array, false)) {
                $key_array[$i] = $val->getEvent();
                $temp_array[$i] = $val;
            }
            ++$i;
        }

        return $temp_array;
    }

    /**
     * Render Event\UserRole for DashboardUser template
     * by merging relation with mutual events and link role to them
     * ***example**************************.
     *
     * foo<--ROLE_COMPANY_ADMIN-->bar
     * foo<--ROLE_ADMIN_EVENTS-->gor
     * foo<--ROLE_ADMIN_EVENTS-->bar
     * foo<--ROLE_COMPANY_ADMIN-->gor
     *
     * => became =>
     *
     * foo<--ROLE_COMPANY_ADMIN--ROLE_ADMIN_EVENTS---->bar
     * foo<--ROLE_COMPANY_ADMIN--ROLE_ADMIN_EVENTS---->gor
     */
    public function showByUser(): array
    {
        $this->eventUserRoles = $this->getByUser();
        $this->eventUserRoles = $this->joinRoles($this->eventUserRoles);

        return $this->eventUserRoles = $this->showUniqueEvent($this->eventUserRoles);
    }
}
