<?php

namespace App\Service\Event;

use App\Entity\Event\Event;
use App\Entity\Event\Stage;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class StageManager
{
    private $company;
    private $entityManager;
    private $events;
    private $eventUserRoles;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Return an array of Stage linked to the array of Event, if there is double they are merged.
     */
    public function getByEvent(Event $event): ?array
    {
        return $this->entityManager->getRepository((Stage::class))->findBy(['event' => $event->getId()]);
    }

    /**
     * Return an array of Stage linked to the array of Event, if there is double they are merged.
     */
    public function getByEvents(?array $events): ?array
    {
        $data = [];
        if (isset($events) && !empty($events)) {
            foreach ($events as $event) {
                $data = array_merge($data, $this->entityManager->getRepository(Stage::class)->findBy(['event' => $event->getId()]));
            }

            return $data;
        }

        return null;
    }
}
