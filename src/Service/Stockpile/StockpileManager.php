<?php

namespace App\Service\Stockpile;

use App\Entity\Company\Company;
use App\Entity\Stockpile\Stockpile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class StockpileManager
{
    private $company;
    private $entityManager;
    private $security;
    private $stockpiles;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    public function setUser()
    {
        return $this->security->getUser();
    }

    private function getStockpiles(Stockpile $stockpile)
    {
        $this->stockpiles->contains($stockpile);
    }

    public function getByCompany(Company $company): array
    {
        return $this->entityManager->getRepository(\App\Entity\Stockpile\CompanyRole::class)->findBy(['company' => $company->getId()]);
    }

    /**
     * Return array of Stockpile from the array of RelationRole give as arguments.
     * If double it's filtered.
     */
    public function filTerE(array $relations): array
    {
        $result = [];
        foreach ($relations as $relation) {
            $result[] = $relation->getStockpile();
        }

        return array_unique($result);
    }

    //TODO add get by id, by reference
    public function getUnique(string $stockpileSlug): ?Stockpile
    {
        return $this->entityManager->getRepository(Stockpile::class)->findOneBy(['stockpileSlug' => $stockpileSlug]);
    }

    /*    public function getStockpilesByCompNUser(Company $company): array
        {
            $byCompany = $this->getStockpilesByCompany($company);
            foreach ($byCompany as $stockpile){
                $byCompanyStockpile[] = $stockpile->getStockpile()->getSlug();
            }

            $byUser = $this->getStockpilesByUser();
            foreach ($byUser as $stockpile){
                $byUserEvent[] = $stockpile->getEvent()->getSlug();
            }

            $intersect = array_intersect($byUserEvent, $byStructEvent);

            return $this->entityManager->getRepository(\App\Entity\Event\Event::class)->findBy(['slug' => $intersect]);
        }*/
}
/*        $event = $this->getEvent($event);
        //$this->
        //$events = [];
        $eventStructureRoles = [];
        $eventUserRoles = [];
        $eventsRoles = $this->getDoctrine()->getRepository(\App\Entity\Event\StructureRole::class)->findBy(['structure' => $this->structure->getId()]);
        foreach ($eventsRoles as $eventRole) {
            $events[] = $eventRole->getEvent();
            $eventStructureRoles[] = $eventRole->getRoles();
            $eventUserRole = $this->getDoctrine()->getRepository(\App\Entity\Event\UserRole::class)->findBy(['event' => $eventRole->getEvent()]);
            if (true// userRole->user == this->user //) {
                $eventUserRoles[] = $eventUserRole;
            } else {
                $eventUserRoles[] = 'empty';
            }

            //in eventUserRole where $eventRole->getEvent()->getId and $this->getUser()->getId();
        }*/

/*$stockpiles = [];
$stockpileRoles = [];
$stockpilesRoles = $this->getDoctrine()->getRepository(\App\Entity\Stockpile\StructureRole::class)->findBy(['structure' => $this->structure->getId()]);
foreach ($stockpilesRoles as $stockpileRole) {
    $stockpiles[] = $stockpileRole->getStockpile();
    $stockpileRoles[] = $stockpileRole->getRoles();
}*/
