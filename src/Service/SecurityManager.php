<?php

namespace App\Service;

use App\Entity\Company\Company;
use App\Repository\Event\UserRoleRepository;
use App\Repository\Company\UserRoleRepository as CompanyUserRoleRepository;
use App\Service\Company\CompanyManager;
use App\Service\Event\EventManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityManager
{
    private $companyManager = null;
    private $security;
    private $translator;
    private $flash;
    private $eventManager = null;
    private $router;

    public function __construct(CompanyManager $companyManager,
                                EventManager $eventManager,
                                Security $security,
                                FlashBagInterface $flash,
                                TranslatorInterface $translator,
                                UrlGeneratorInterface $router,
                                CompanyUserRoleRepository $userRoleRepository

    )
    {
        $this->companyManager = $companyManager;
        $this->eventManager = $eventManager;
        $this->security = $security;
        $this->flash = $flash;
        $this->translator = $translator;
        $this->router = $router;
        $this->CompanyUserRoleRepository = $userRoleRepository;
    }

    public function setUser(): ?UserInterface
    {
        return $this->security->getUser();
    }

    /**
     * This service work for relation with user, an other service is needed for others relation
     * its auto check if the relation have to be check with an event or a company...
     *
     * $this->securityManager->is($this->company, ['ROLE_COMPANY_ADMIN'])
     *
     * return true if in else false
     *
     * @param $link
     */
    public function is($link, array $roles): bool
    {
        $className = substr(strrchr(get_class($link), '\\'), 1);

        if ('Company' === $className) {
            $userRole = $this->companyManager->getUserRoles($link);

            foreach ($roles as $role) {
                if (in_array($role, $userRole, true)) {
                    return true;
                }
            }
        }

        if ('Event' === $className) {
            $userRole = $this->eventManager->getUserRoles($link);

            foreach ($roles as $role) {
                if (in_array($role, $userRole, true)) {
                    return true;
                }
            }
        }

        $message = $this->translator->trans('You don\'t have access to this zone');

        $this->flash->add('error', $message);

        return false;
    }

    /**
     * Control if user is enabled,
     * Control user mail has been verified
     * Control user password in change method.
     *
     *  $securityManager->ownSecurity();
     * !!! Return NULL if everything OK !!!
     *
     * If something wrong send flash message linked to the error and a ROLE_LIMIT to the user without pushing it
     */
    public function ownSecurity(?Company $company = null): ?Response
    {
        if ($company !== null)
        {
            $this->checkAdmin_company_Roles($company);
        }
        if (!$this->setUser()->getEnabled()) {
            $this->setUser()->addRole('ROLE_LIMIT');
            $message = $this->translator->trans('It appears you are deactivated, please contact CAGIBIG');
            $this->setUser()->addRole('ROLE_LIMIT');

            return $this->flash->add('error', $message);
            //return $this->twig->render('easyAdmin/user/error/disabled.html.twig');
        }
        if (!$this->setUser()->getIsVerified()) {
            $this->setUser()->addRole('ROLE_LIMIT');
            $notverif = $this->translator->trans('Your email has not been verified, check your mailbox');
            $sendmes = $this->translator->trans('Send a new mail verification');
            $sendLink = sprintf('<a href="/verify/email_reset">%s</a>', $sendmes);
            $message = $notverif.': '.$sendLink;

            return $this->flash->add('error', $message);
            //return $this->twig->render('easyAdmin/user/error/not_verified.html.twig');
        }
        if ($this->security->isGranted('ROLE_PWD_CHANGE'))
        {
            $this->setUser()->addRole('ROLE_LIMIT');
            $message = $this->translator->trans('Password change pending');

            return $this->flash->add('error', $message);
            //return $this->redirectToRoute('password_wait');
        }

        return null;
    }

    public function checkAdmin_company_Roles(Company $company): void
    {
        if ($this->companyManager->getUserRoles($company) === ["ROLE_COMPANY_ADMIN"] && !$company->getIsVerified())
        {
            $companyId = $company->getId();
            $notverif = $this->translator->trans('Your company email address hasn\'t been verified.');
            $sendmes = $this->translator->trans('Send a new mail verification');
            $sendlink = sprintf("<a href=".$this->router->generate('app_company_register_email_reset',['company'=> $companyId]).">%s</a>", $sendmes);

            $message = $notverif.$sendlink;
            $this->flash->add('error',$message);
        }
    }


    /**
     * call to find admin company user
     * browse to the roles of the company if match to the current user this will return true
     * else false
     * @param int $idCompany
     * @param array $roles
     * @return bool
     */
    public function comparisonTargetRoleByCompagnyToCurrenUserRole( int $idCompany, array $roles): bool
    {
        $result = false;
        foreach ($roles as $role){
            $rolesCompany = $this->CompanyUserRoleRepository->findAdminCompanyByIdAndRoleTarget($idCompany,$role);
            foreach ($rolesCompany as $roleCompany) {
                if ( $roleCompany->getUser() === $this->setUser()) {
                     $result = true;
                }
            }
        }
        return $result;
    }
}
