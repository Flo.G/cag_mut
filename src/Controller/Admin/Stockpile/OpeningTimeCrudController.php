<?php

namespace App\Controller\Admin\Stockpile;

use App\Entity\Stockpile\OpeningTime;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class OpeningTimeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return OpeningTime::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
