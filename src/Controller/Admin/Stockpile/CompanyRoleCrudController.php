<?php

namespace App\Controller\Admin\Stockpile;

use App\Entity\Stockpile\CompanyRole;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CompanyRoleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CompanyRole::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
