<?php

namespace App\Controller\Admin\Stockpile;

use App\Entity\Stockpile\Stockpile;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class StockpileCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Stockpile::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
