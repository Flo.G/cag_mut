<?php

namespace App\Controller\Admin;

use App\Entity\Phone;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PhoneCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Phone::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
