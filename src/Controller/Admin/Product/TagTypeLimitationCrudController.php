<?php

namespace App\Controller\Admin\Product;

use App\Entity\Product\TagTypeLimitation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TagTypeLimitationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return TagTypeLimitation::class;
    }
    public function configureCrud(Crud $crud): Crud
    {

        return $crud->setDefaultSort(['name'=> "ASC"]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('slugProductLimitation'),
            TextField::new('description'),
            AssociationField::new('categories')
                ->setFormTypeOptions([
                    'by_reference' => false,
                ])
        ];
    }

}
