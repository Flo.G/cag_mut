<?php

namespace App\Controller\Admin\Product;

use App\Entity\Product\Product;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            //IdField::new('id'),
            TextField::new('name'),
            TextEditorField::new('description'),
            //State
            BooleanField::new('disponibility'),
            //reference
            //image
            AssociationField::new('stockpile')->autocomplete(),
            AssociationField::new('category'),
            //sub_category_id
            //tag_type_usage_limitation
            //geografic_limitation
            //return_default_engageent_id
            MoneyField::new('value')->setCurrency('EUR'),
            MoneyField::new('big_cost')->setCurrency('BIF'),
            //degressive_rate_id
            //delivery_category_id
            //weight
            //TODO ArrayField::new()
            //reservation_dates_id
            //control_documentation
        ];

    }
    */
}
