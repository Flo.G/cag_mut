<?php

namespace App\Controller\Admin\Product;

use App\Entity\Product\PackRelationship;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class PackRelationshipCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PackRelationship::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
