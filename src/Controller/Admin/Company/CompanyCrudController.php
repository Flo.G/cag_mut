<?php

namespace App\Controller\Admin\Company;

use App\Entity\Company\Company;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CompanyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Company::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Name'),
            EmailField::new('email', 'Email of the company'), //TODO bug complé, ne renvoie pas objet "mail admnistrateur" et n'affiche pas dans le dashboard
            //AssociationField::new('admin_users', 'Email from the administrator')->onlyOnDetail(), //->setValue($this->getUser()->getEmail())//TODO automatiser enregistrement valeur mail admin company
            TextEditorField::new('description', 'Description'),
            MoneyField::new('big')->setCurrency('BIF')->onlyOnDetail(), //TODO setCurrency BIG
            // TODO compare
            TelephoneField::new('phones', 'Phone'),
            AssociationField::new('phones', 'Phone'),
            //..
            //AssociationField::new('stockpiles', 'Stockpile'),
            AssociationField::new('stockpileCompanyRoles', 'Stockpile'),
            //..
            TextField::new('address', 'Adress'),
            TextField::new('city', 'City'),
            IntegerField::new('postalCode', 'PostalCode'),
            IntegerField::new('siret', 'Company Number'),
            ChoiceField::new('type')->setChoices(['event' => 'event', 'health' => 'health', 'commune' => 'commune']), //TODO pas de allowMultipleChoice renvoie string et faire un table de conf pour "tout le monde"
            DateTimeField::new('createdAt', 'Created at')->onlyOnDetail(),
            ImageField::new('logo', 'Logo'),
            //ImageField::new('image'), //TODO how to send for a string with imagefield
            IntegerField::new('tvaRate', 'Tva rate'),
        ];
    }
}
