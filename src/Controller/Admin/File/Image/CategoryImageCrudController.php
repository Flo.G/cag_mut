<?php

namespace App\Controller\Admin\File\Image;

use App\Entity\File\Image\CategoryImage;
use App\Form\BaseType\VichAddImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CategoryImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CategoryImage::class;
    }


    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            // this will forbid to create or delete entities in the backend
            ->disable(Action::EDIT)
            ;
    }

    public function configureFields(string $pageName): iterable
    {

        return [
            $imageName=TextField::new('imageName'),
            TextField::new('description'),
            TextField::new('alt'),
            //   TextField::new('imageFile')->setFormType(VichImageType::class)->setFormTypeOption('image_pattern', )
            // ImageField::new('filePath')->setUploadDir('public/uploads/images/categories')
            TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex(),
            ImageField::new('imageFile')->setBasePath('')->onlyOnIndex(),

        ];
}}
