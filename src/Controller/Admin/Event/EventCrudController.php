<?php

namespace App\Controller\Admin\Event;

use App\Entity\Event\Event;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class EventCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Event::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            AssociationField::new('company','Company organisatrice'),
            AssociationField::new('stageman', 'mail du regisseur'),//admin_user_id->admin_user_email || admin_user_fullname pour definir???
            DateTimeField::new('date_start'),
            DateTimeField::new('date_end'),
            MoneyField::new('budget')->setCurrency('BIF')
        ];

    }
    */
}
