<?php

namespace App\Controller\Admin\Setting;

use App\Entity\Setting\Category;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CategoryCrudController extends AbstractCrudController
{


    public static function getEntityFqcn(): string
    {
        return Category::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud->setDefaultSort(['name'=> "ASC"]);
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            // this will forbid to create or delete entities in the backend
            ->disable(Action::NEW)
            ;
    }

    public function configureFields(string $pageName): iterable
    {
        return [

            AssociationField::new('productTagTypeLimitations','Tag'),
            AssociationField::new('products'),
            TextField::new('name','Name'),
            TextField::new('categorySlug','Slug'),
            AssociationField::new('image','Images')->onlyWhenUpdating(),
            ImageField::new('image')->setBasePath('/uploads/images/categories')->onlyOnIndex()
        ];
    }

}
