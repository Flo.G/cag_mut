<?php

namespace App\Controller\Admin\Setting;

use App\Controller\Dashboard\Settings\SuperAdminTrait;
use App\Controller\Dashboard\SuperAdminDashboardController;
use App\Entity\File\Image\CategoryImage;
use App\Entity\Setting\Category;
use App\Form\BaseType\VichAddImageType;
use App\Form\Setting\CategoryImageType;
use App\Form\Setting\CategoryType;
use App\Service\Upload\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CategoryController extends AbstractDashboardController
{
    use SuperAdminTrait;

    /**
     * @Route("/super_admin/category", name="super_admin_category")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(CategoryCrudController::class)->generateUrl());
    }

    /**
     * @Route("/super_admin/category/add", name="super_admin_addCategory")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addCategory(Request $request, EntityManagerInterface $manager): Response
    {
        $category= new Category();

        $categoryForm = $this->createForm(CategoryType::class, $category);
        $categoryForm->handleRequest($request);

        if($categoryForm->isSubmitted() && $categoryForm->isValid())
        {
            $manager->persist($category);
            $manager->flush();

            return $this->redirectToRoute('super_admin_add_categoryImage',['categorySlug'=> $category->getCategorySlug()]);
        }
        return $this->render('easyAdmin/admin/setting/category/addCategory.html.twig', [
            'user'=> $this->getUser(),
            'categoryForm'=>$categoryForm->createView(),
        ]);
    }

    /**
     * @Route("/super_admin/category/add_image/{categorySlug}", name="super_admin_add_categoryImage")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addCategoryImage(Category $category,
                                     Request $request,
                                     EntityManagerInterface $manager,
                                     FileUploader $fileUploader,
                                     AdminUrlGenerator $adminUrlGenerator
    ):Response
    {
        $image = new CategoryImage();
        $imageForm =  $this->createForm(VichAddImageType::class,$image);
        $imageForm->handleRequest($request);

        $url = $adminUrlGenerator
            ->setController(CategoryCrudController::class)
            ->setDashboard(SuperAdminDashboardController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();

        if ($imageForm->isSubmitted() && $imageForm->isValid())
        {
            $category->setImage($image);
            $image->setCategories($category);
            $image->setFilePath($image::PATH);
            $image->setDescription($image::DESCRIPTION);
            $manager->persist($image);
            $fileUploader->filter($image);
            $manager->flush();
            return $this->redirectToRoute('super_admin_category');
            //return $this->redirect($url);
        }

        return $this->render('easyAdmin/admin/setting/category/addCategoryImage.html.twig',[
            'user'=>$this->getUser(),
            'imageForm' =>$imageForm->createView()
        ]);
    }

}
