<?php

namespace App\Controller\Admin\User;

use App\Entity\User\User;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            //IntegerField::new('id')->hideOnForm(),
            //BooleanField::new('enabled')->hideOnForm(),
            //BooleanField::new('isVerified')->hideOnForm(),
            TextField::new('firstName', 'First Name'),
            TextField::new('lastName', 'Last name'),
            TextField::new('nickname', 'Surname'),
            EmailField::new('email', 'Email'), //TODO POUR le changer dois passer par verification de mail
            ChoiceField::new('roles')->setChoices(['Super admin' => 'ROLE_SUPER_ADMIN', 'Administrateur de company' => 'ROLE_ADMIN', 'Regisseur' => 'ROLE_STAGEMAN'])->allowMultipleChoices(), //allowMultipleChoice renvoie tableau sinon strin
            TextField::new('password', 'Password'), //TODO POUR le changer dois passer par verification de mail
            TextField::new('address', 'Address'),
            TextField::new('city', 'City'),
            IntegerField::new('postal_code', 'PostalCode'),
            //TelephoneField::new('tel_number'),
            //TelephoneField::new('pro_tel_number'),
            //TODO ImageField::new('image'), // ImageField permet de browse dans le formulaire
            ArrayField::new('emails', 'Emails'),
            //TODO AssociationField::new('admined_structurs')->hideOnForm(), // n'est pas visible dans formulaire
            //TODO AssociationField::new('stageman_structurs')->hideOnForm(), // n'est pas visible dans formulaire
        ];
    }
}
