<?php

namespace App\Controller\Admin\Investment;

use App\Entity\Investment\Investment;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class InvestmentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Investment::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
