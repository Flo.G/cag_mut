<?php

namespace App\Controller\Stockpile;

use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Entity\Investment\Investment;
use App\Entity\Product\Product;
use App\Entity\Stockpile\CompanyRole;
use App\Entity\Stockpile\Stockpile;
use App\Entity\Stockpile\UserRole;
use App\Form\Stockpile\AddStockpileType;
use App\Service\Product\ProductManager;
use App\Service\SecurityManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddStockpileController extends abstractDashboardController
{
    use CompanyTrait;

    /**
     * @Route("/company/{companySlug}/add/stockpile", name="app_company_add_stockpile")
     */
    public function addStockpile(Company $company,
                                 SecurityManager $securityManager,
                                 EntityManagerInterface $manager,
                                 Request $request): Response
    {//TODO a revoir les role, le mec doit etre ROLE_ADMIN_STOCKPILES ou ROLE_COMPANY_ADMIN (Company\UserRole) pour pouvoir ajouter un stockpile,
        // c'est pa le fait d'en ajouter 1 qui lui donne, par contre ça lui donne le ROLE_STOCKPILE_ADMIN (Stockpile\UserRole) si c'est personne d'autre
        $nextPage = 'app_company_products';

        $user = $this->getUser();
        $this->company = $company;



        if (!$securityManager->is($this->company, ['ROLE_COMPANY_ADMIN', 'ROLE_ADMIN_STOCKPILES'])) {
            return $this->redirectToRoute('app_company_products', ['companySlug' => $this->company->getCompanySlug()]);
        }

        $stockpile = new Stockpile();

        $form = $this->createform(AddStockpileType::class, $stockpile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $roleOwnCompany = new CompanyRole();
            $roleOwnCompany->setRoles(['ROLE_OWNER']);
            $roleOwnCompany->setCompany($company);
            $roleOwnCompany->setStockpile($stockpile);

            $roleAdmCompany = new CompanyRole();
            $roleAdmCompany->setRoles(['ROLE_ADMIN']);
            $roleAdmCompany->setCompany($company);
            $roleAdmCompany->setStockpile($stockpile);

            $roleStoCompany = new CompanyRole();
            $roleStoCompany->setRoles(['ROLE_STOCKING']);
            $roleStoCompany->setCompany($company);
            $roleStoCompany->setStockpile($stockpile);

            $roleUserCompany = new UserRole();
            $roleUserCompany->setRoles(['ROLE_STOCKPILE_ADMIN']);
            $roleUserCompany->setStockpile($stockpile);
            $roleUserCompany->setCompany($company);
            $roleUserCompany->setUser($user);

            $manager->persist($stockpile);
            $manager->persist($roleOwnCompany);
            $manager->persist($roleAdmCompany);
            $manager->persist($roleStoCompany);
            $manager->persist($roleUserCompany);

            $manager->flush();

                return $this->redirectToRoute($nextPage, [
                    'companySlug' => $this->company,
                ]);
        }

        return $this->render('easyAdmin/stockpile/add_stockpile.html.twig', [
            'formStockpile' => $form->createView(),
            'nextPage' => $nextPage,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }
}
