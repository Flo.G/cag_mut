<?php

namespace App\Controller\Stockpile;

use App\Repository\Stockpile\StockpileRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController
{
    /**
     * @Route("stockpile_autocomplete.json", name="api_stockpile_autocomplete")
     */
    public function StockpileAutocomplete(Request $request, StockpileRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }
}
