<?php

namespace App\Controller\Api;

use App\Entity\Event\Event;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiCalendarController extends AbstractController
{
    /**
     * @Route("/api/calendar", name="api_calendar")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('app_user');
    }

    /**
     * @Route("/api/calendar/{id}/edit", name="api_calendar_edit", methods={"PUT"})
     *
     * @throws \JsonException
     */
    public function majEvent(?Event $event, Request $request): Response
    {
        $datas = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);

        if (
            isset($datas->title) && !empty($datas->title) &&
            isset($datas->start) && !empty($datas->start)/* &&
            isset($datas->description) && !empty($datas->description) &&*/
            /*isset($datas->backgroundColor) && !empty($datas->backgroundColor)*/
            ) {
            $code = 200;
            if (!$event) {
                $event = new Event();
                $code = 201;
            }

            $event->setName($datas->title);
            //$event->setDescription($datas->description);
            $event->setDateStart(new DateTime($datas->start));
            if (!$datas->allDay) {
                $event->setDateEnd(new DateTime($datas->start));
            } else {
                $event->setDateEnd(new DateTime($datas->end));
            }
            $event->setAllDay($datas->allDay);
            /*$event->setBackgroundColor($datas->backgroundColor);*/

            $manager = $this->getDoctrine()->getManager();
            $manager->flush();

            return new Response('Ok', $code);
        }

        return new Response($datas->title, 202);
    }
}
