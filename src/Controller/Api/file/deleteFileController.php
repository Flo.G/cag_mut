<?php


namespace App\Controller\Api\file;

use App\Repository\Company\CompanyRepository;
use App\Service\SecurityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;


class deleteFileController extends AbstractController
{

    /**
     * @Route("/img/delete", name="app_delete_img")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function deleteImg(
        SecurityManager $securityManager,
        CompanyRepository $companyRepository,
        Request $request,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ):Response
    {
        $imgTarget = $request->query->get('imgNameToDelete');
        $urlToRedirect = $request->query->get('urlOrigin');
        $imageTypeClass = $request->query->get('imageType');
        $companySlug = $request->query->get('company');
        $companyOfimg = $companyRepository->findOneBy(['companySlug'=>$companySlug]);
        if ($securityManager->comparisonTargetRoleByCompagnyToCurrenUserRole($companyOfimg->getId(),['["ROLE_COMPANY_ADMIN"]'])){
            $imageRepository = $entityManager->getRepository($imageTypeClass);
            $imgProduct = $imageRepository->findOneBy(['imageName'=>$imgTarget]);
            $entityManager->remove($imgProduct);
            $entityManager->flush();
            $this->addFlash('sucess', $translator->trans('your image as been deleted'));
            return $this->redirect($urlToRedirect);
        }
        $this->addFlash('error',$translator->trans('something wrong happen'));
        return $this->redirect($urlToRedirect);
    }

}