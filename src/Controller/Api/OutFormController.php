<?php

namespace App\Controller\Api;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OutFormController extends abstractController
{
    /**
     * @Route("/out_form/{route}", name="api_out_form")
     */
    public function index($route, Request $request, EntityManagerInterface $entityManager): Response
    {
        //Data part
        if (null !== $request->query->get('formValue')) {
            $formValue = $request->query->get('formValue');
            $formClassName = $request->query->get('formClassName');

            $repository = $this->getDoctrine()->getRepository($formClassName);
            $object = $repository->find($formValue);
            $entityManager->remove($object);
            $entityManager->flush();

            $this->addFlash('error', 'the registered datas has been erased.');
        }

        //Redirection part
        if ('app_company_products' === $route) {
            return $this->redirectToRoute($route, ['companySlug' => $request->query->get('companySlug')]);
        }

        return $this->redirectToRoute('app_user_dashboard');
    }
}
