<?php

namespace App\Controller\Api;

use App\Repository\Company\CompanyRepository;
use App\Repository\Setting\CategoryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchSettingController
{
    /**
     * @Route("/role_autocomplete.json", name="api_role_autocomplete")
     */
    public function RoleAutocomplete(Request $request, CompanyRepository $repository): JsonResponse //TODO change pour role
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }

    /**
     * @Route("/categories_autocomplete.json", name="api_categories_autocomplete")
     */
    public function CategoriesAutocomplete(Request $request, CategoryRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }
}
