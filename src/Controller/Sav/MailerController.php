<?php


namespace App\Controller\Sav;

use App\Controller\Dashboard\Settings\SavTrait;
use App\Form\Sav\SavMailFormType;
use App\Mail\NewCompanyMail;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class MailerController extends abstractDashboardController
{
    use SavTrait;
    private $newCompanyMail;
    private $superAdminEmail;
    private $noReplyEmail;

    public function __construct(NewCompanyMail $newCompanyMail)
    {
        $this->newCompanyMail = $newCompanyMail;
        $this->superAdminEmail = $_ENV['SUPER_ADMIN_EMAIL'];
        $this->noReplyEmail = $_ENV['NO_REPLY_EMAIL'];
    }


    /**
     * @Route("/sav/email/{subject}", name="app_sav_email")
     */

    public function savMail(string $subject,TranslatorInterface $trans,  Request $request): Response
    {

        if($this->getUser() !== null) {
            $datas['mail'] = $this->getUser()->getEmail();
        }
        $datas['subject'] = $subject;

        $form = $this->createForm(SavMailFormType::class, $datas);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            // generate a signed url and email it to the SUPER_ADMIN
            $this->newCompanyMail->newmail('app_login',
                (new TemplatedEmail())
                    ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                    ->to(new Address($this->superAdminEmail, 'Super_Administrateur'))
                    ->subject($form->getData()['mail'])
                    ->context([
                        'contents'=>$form->getData()
                    ])
                    ->htmlTemplate('email/sav.html.twig')//$form->getData()['content'].$form->getData()['mail']
            );
            $message = $trans->trans('your message has been send');
            $this->addFlash('sucess', $message);
            return $this->redirectToRoute('app_user_dashboard');

        }
        return
            $this->render('easyAdmin/sav/mailer.html.twig', [
                'form' => $form->createView(),
            ]);
    }
}