<?php

namespace App\Controller\Dashboard;

use App\Controller\Dashboard\Settings\UserSettingTrait;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserSettingsDashboardController extends AbstractDashboardController
{
    use UserSettingTrait;
    /**
     * @Route("/user/settings", name="app_user_settings_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        $this->user = $this->getUser();
        if (!$this->user->getEnabled()) {
            return $this->render('easyAdmin/user/error/disabled.html.twig');
        }
        if (!$this->user->getIsVerified()) {
            return $this->render('easyAdmin/user/error/not_verified.html.twig');
        }
        if ($this->isGranted('ROLE_PWD_CHANGE')) {
            return $this->redirectToRoute('password_wait');
        }
        return $this->render('easyAdmin/user/settings/settings.html.twig');
    }
}

