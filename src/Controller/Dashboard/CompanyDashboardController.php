<?php

namespace App\Controller\Dashboard;

use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Service\Company\CompanyManager;
use App\Service\Event\EventManager;
use App\Service\Investment\InvestmentManager;
use App\Service\Rent\RentManager;
use App\Service\SecurityManager;
use App\Service\Stockpile\StockpileManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class CompanyDashboardController extends AbstractDashboardController
{
    use CompanyTrait;
    private $translator;

    /**
     * @Route("/company", name="company_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        return $this->redirect('/user'); //TODO voir si on peut renvoyer sur $this->company
    }

    /**
     * @Route("/company/{companySlug}", name="app_company_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function companyDashboard(
        Company $company,
        CompanyManager $companyManager,
        EventManager $eventManager,
        RentManager $rentManager,
        InvestmentManager $investmentManager,
        StockpileManager $stockpileManager,
        TranslatorInterface $translator,
        SecurityManager $securityManager): Response
    {
        $securityManager->ownSecurity($company);



        // Si l'article existe nous envoyons les données à la vue
        return $this->render('easyAdmin/company/index.html.twig', [
            'userCompanyRoles' => $this->userCompanyRoles = $companyManager->getUserRoles($company),
            'events' => $eventManager->getByCompanyNUser($company),
            'investments' => $investmentManager->getRoleByCompany($company),
            'stockpiles' => $stockpileManager->getByCompany($company),
            'company' => $this->company = $company,
            'rentsIn' => $rentManager->in($company),//TODO message ["waiting for signature","Return the **/**/**"], if waiting for action background color yellow else blue
            'rentsOut' => $rentManager->out($company),//TODO message "waiting for confirmation", if waiting for action background color yellow else blue
        ]);
    }
}
