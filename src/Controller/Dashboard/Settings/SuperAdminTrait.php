<?php


namespace App\Controller\Dashboard\Settings;


use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Entity\File\Image\CategoryImage;
use App\Entity\Product\TagTypeLimitation;
use App\Entity\Setting\Category;
use App\Entity\User\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;

trait SuperAdminTrait
{
    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Cagibig Mut Super Admin');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fab fa-cotton-bureau');
        // yield MenuItem::linkToCrud('The Label', 'icon class', EntityClass::class);
        yield MenuItem::section('Super Administrateur');

        //Users menu
        yield MenuItem::subMenu('Users', 'fa fa-users')->setSubItems([
            MenuItem::linkToCrud('Add user', 'fas fa-user-plus', User::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Show Users detail', 'fas fa-address-card', User::class)
                ->setAction('detail')
                ->setEntityId(28),
            MenuItem::linkToCrud('Display users in order', 'fas fa-users', User::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
            MenuItem::linkToCrud('Blocked users', 'fas fa-user-lock', User::class)
                ->setAction('new'),
        ]);

        //Companys menu
        yield MenuItem::subMenu('Companys', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Company', 'fa fa-file-text', Company::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Display Companys in order', 'fa fa-tags', Company::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
            MenuItem::linkToCrud('Companys awaiting validation', 'fa fa-warning', Company::class)//TODO quand Company isVerified false
            ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);

        //Events menu
        yield MenuItem::subMenu('Events', 'fa fa-calendar')->setSubItems([
            MenuItem::linkToCrud('Add event', 'fas fa-calendar-plus', Event::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Display events in order', 'fa fa-calendar', Event::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),
        ]);

        //category menu
        yield MenuItem::subMenu('Category', 'fa fa-comment')->setSubItems([
            MenuItem::linkToRoute('Add category', 'fa fa-file-text', 'super_admin_addCategory' ),

            MenuItem::linkToCrud('Show category detail', 'fa fa-comment', Category::class)
                ->setAction('detail')
            /*->setEntityId(4)*/,
            MenuItem::linkToCrud('Display category in order', 'fa fa-tags', Category::class)
                ->setQueryParameter('sortField', 'createdAt')
                ->setQueryParameter('sortDirection', 'DESC'),

        ]);

        // TagTypeLimitation
        yield MenuItem::subMenu('Tags Limitation', 'fa fa-comment')->setSubItems([
            MenuItem::linkToCrud('Add Tag', 'fa fa-file-text', TagTypeLimitation::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Display Tags ', 'fa fa-tags', TagTypeLimitation::class)
        ]);

        //ImageCategory
        yield MenuItem::subMenu('Image Category', 'fas fa-images')->setSubItems([
            MenuItem::linkToCrud('Add image', 'fa fa-file-text', CategoryImage::class)
                ->setAction('new'),
            MenuItem::linkToCrud('Display images ', 'fa fa-tags', CategoryImage::class)
        ]);
    }

    public function configureAssets(): Assets
    {
        return Assets::new()

            ->addCssFile('css/post_login.css')
            ->addCssFile('css/main.css')
            ->addCssFile('css/mediaQueries.css')
            ;

    }
}