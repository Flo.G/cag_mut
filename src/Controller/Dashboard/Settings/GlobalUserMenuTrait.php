<?php

namespace App\Controller\Dashboard\Settings;

use App\Entity\User\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use Symfony\Component\Security\Core\User\UserInterface;

trait GlobalUserMenuTrait
{
    /**
     * @var object|User|null
     */
    public $user;

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle($this->dashboard)
            ->renderContentMaximized()
            ->renderSidebarMinimized()
            ->disableUrlSignatures();
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        $this->user = $this->getUser();
        if ($this->user->getAvatar()) {
            $avatar = sprintf('%s/%s', $this->user->getAvatar()->getFilePath(), $this->user->getAvatar()->getImageName());

            return parent::configureUserMenu($this->user)
                ->setName($this->user->getUsername())
                ->setAvatarUrl($avatar)
                ->displayUserAvatar(true)
                ->addMenuItems([
                    MenuItem::linkToRoute('My profile', 'fa fa-id-card', 'app_user_settings_dashboard_profile'),
                    MenuItem::linkToUrl('Settings', 'fa fa-user-cog', '/user/settings'),
                    MenuItem::linkToUrl('Security', 'fa fa-lock', '/user/settings/security'),
                    MenuItem::section(),
                    MenuItem::linkToUrl('Home', 'fas fa-undo-alt', '/user'),
                ]);
        }

        return parent::configureUserMenu($this->user)
            ->setName($this->user->getUsername())
            ->displayUserAvatar(false)
            ->addMenuItems([
                MenuItem::linkToRoute('My profile', 'fa fa-id-card', 'app_user_settings_dashboard_profile'),
                MenuItem::linkToUrl('Settings', 'fa fa-user-cog', '/user/settings'),
                MenuItem::linkToUrl('Security', 'fa fa-lock', '/user/settings/security'),
                MenuItem::linkToUrl('ask a question', 'fa fa-question', '/sav/email/cagibig-69120'),
                MenuItem::section(),
                MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user'),
            ]);
    }
}
