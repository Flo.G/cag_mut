<?php

namespace App\Controller\Dashboard\Settings;

use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;

trait PostLoginTrait
{
    use GlobalUserMenuTrait;
    public $dashboard = 'USER';

    public function configureAssets(): Assets
    {
        return Assets::new()
            //->addWebpackEncoreEntry('admin-app')// <-- accessoirement avec webpack de merde

            ->addCssFile('css/post_login.css')
            ->addCssFile('css/main.css')
            ->addCssFile('css/mediaQueries.css')

            ;
    }
}
