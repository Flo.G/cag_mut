<?php

namespace App\Controller\Dashboard\Settings;

use App\Controller\Dashboard\DashboardController;
use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Entity\Investment\Investment;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;

trait CompanyTrait
{
    use GlobalUserMenuTrait;

    public $dashboard = 'COMPANY';
    public $company;
    public $securityManager;
    public $userCompanyRoles = [];

    public function configureAssets(): Assets
    {
        return Assets::new()
            //->addWebpackEncoreEntry('admin-app')// <-- accessoirement avec webpack de merde

            ->addCssFile('css/post_login.css')
            ->addCssFile('css/product.css')
            ->addCssFile('css/no_log.css')
            ->addCssFile('css/main.css')
            ->addCssFile('css/mediaQueries.css')

            ;
    }

    public function configureMenuItems(): iterable
    {

        //---------------ROLE_LIMIT
        if (in_array('ROLE_LIMIT', $this->getUser()->getRoles())) {
            //Home button
            yield MenuItem::linktoUrl($this->company->getName().'_LIMITED', 'fas fa-home', '/company/'.$this->company->getCompanySlug());

            //Param/User menu
            yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
            ]);

            //All platform products
            yield MenuItem::linktoUrl('All platform products', 'fas fa-boxes', '/company/'.$this->company->getCompanySlug().'/catalog');

            // back button
            yield MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user');

        //---------------ROLE_COMPANY_ADMIN
        } elseif (null !== $this->userCompanyRoles && in_array('ROLE_COMPANY_ADMIN', $this->userCompanyRoles, true)) {
            //if ($this->securityManager->is($this->company, ['ROLE_COMPANY_ADMIN'])) { //TODO essayer de trouver soluce pour ça c'est trop relou
            //Home button
            yield MenuItem::linktoUrl($this->company->getName().'_company_admin', 'fas fa-home', '/company/'.$this->company->getCompanySlug());

            //Materiel/Stockpile menu
            yield MenuItem::linkToUrl('Products', 'fas fa-truck-loading', '/company/'.$this->company->getCompanySlug().'/products');

            //Calendrier/Events menu
            yield MenuItem::subMenu('Calendar', 'far fa-calendar-alt')->setSubItems([
                MenuItem::linktoUrl('Add events', 'fas fa-calendar-plus', '/company/'.$this->company->getCompanySlug().'/event_add'),
                MenuItem::linktoUrl('Show events', 'fas fa-calendar-week', '/company/'.$this->company->getCompanySlug().'/calendar'),
            ]);

            //Contract menu
            yield MenuItem::subMenu('Contract', 'far fa-clipboard')->setSubItems([
            ]);

            //BIG/Investment menu
            yield MenuItem::subMenu('Investments', 'fab fa-bitcoin')->setSubItems([
                MenuItem::linkToRoute('Add investment', 'fa fa-file-text', 'app_company_add_investment', ['companySlug' => $this->company->getCompanySlug()]),
                MenuItem::linktoRoute('Your investments created', 'fa fa-tags', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux créé par la structure
                MenuItem::linktoRoute('Your investments invested', 'fa fa-tags', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux dans laquels la structure a investit
                MenuItem::linkToCrud('All investments', 'fa fa-tags', Investment::class)
                    ->setQueryParameter('sortField', 'createdAt')
                    ->setQueryParameter('sortDirection', 'DESC'),
            ]);

            //Accreditation/Users menu
            yield MenuItem::subMenu('Users', 'fa fa-users')->setSubItems([
                MenuItem::linkToCrud('Add affiliates user', 'fas fa-user-plus', Event::class)// add, show, delete
                ->setAction('new'),
                MenuItem::linktoRoute('View affiliate users', 'fa fa-user-friends', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux créé par la structure
                MenuItem::linktoRoute('Delete an affiliate user', 'fa fa-user-minus', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux créé par la structure
            ]);

            //Param/User menu
            yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                MenuItem::linkToCrud('Show company detail', 'fas fa-address-cog', Company::class)
                    ->setAction('detail')
                    ->setEntityId($this->company->getId()),
                MenuItem::linkToCrud('Edit company detail', 'fas fa-user-edit', Company::class)
                    ->setAction('edit')
                    ->setEntityId($this->company->getId()),
                menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
            ]);

            //All platform products
            yield MenuItem::linktoUrl('All platform products', 'fas fa-boxes', '/company/'.$this->company->getCompanySlug().'/catalog');

        //--------- ROLE_ADMIN_EVENTS
        } elseif (null !== $this->userCompanyRoles && in_array('ROLE_ADMIN_EVENTS', $this->userCompanyRoles, true)) {
            //Home button
            yield MenuItem::linktoUrl($this->company->getName().'_admin_events', 'fas fa-home', '/company/'.$this->company->getCompanySlug());

            //Calendrier/Events menu
            yield MenuItem::subMenu('Calendar', 'far fa-calendar-alt')->setSubItems([
                //MenuItem::linktoRoute('Add events', 'fas fa-calendar-plus', '/company/{slug}/event/add', ['slug' => $this->company->getSlug()]),
                MenuItem::linktoUrl('Add events', 'fas fa-calendar-plus', '/company/'.$this->company->getCompanySlug().'/event_add'),
                MenuItem::linktoUrl('Show events', 'fas fa-calendar-week', '/company/'.$this->company->getCompanySlug().'/calendar'),
            ]);

            //Param/User menu
            yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                MenuItem::linkToCrud('Show company detail', 'fas fa-address-cog', Company::class)
                    ->setAction('detail')
                    ->setEntityId($this->company->getId()),
                MenuItem::linkToCrud('Edit company detail', 'fas fa-user-edit', Company::class)
                    ->setAction('edit')
                    ->setEntityId($this->company->getId()),
                menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
            ]);

            //All platform products
            yield MenuItem::linktoUrl('All platform products', 'fas fa-boxes', '/company/'.$this->company->getCompanySlug().'/catalog');
        //--------- NO-ROLE-or error------------------
        } else {
            //Home button
            yield MenuItem::linktoUrl($this->company->getName().'_no_role', 'fas fa-home', '/company/'.$this->company->getCompanySlug());
            //Param/User menu
            yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
            ]);

            // back button
            yield MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user');

            //All platform products
            yield MenuItem::linktoUrl('All platform products', 'fas fa-boxes', '/company/'.$this->company->getCompanySlug().'/catalog');
        }
    }
}
