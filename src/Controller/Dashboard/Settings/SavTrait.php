<?php

namespace App\Controller\Dashboard\Settings;

use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;


trait SavTrait
{
    use GlobalUserMenuTrait;

    public $dashboard = 'SAV';


    public function configureAssets(): Assets
    {
        return Assets::new()

            ->addCssFile('css/post_login.css')
            ->addCssFile('css/main.css')
            ->addCssFile('css/mediaQueries.css')

            ;
    }

}
