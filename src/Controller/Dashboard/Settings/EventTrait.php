<?php

namespace App\Controller\Dashboard\Settings;

use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Entity\Investment\Investment;
use App\Entity\Stockpile\Stockpile;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;

trait EventTrait
{
    use GlobalUserMenuTrait;

    public $dashboard = 'EVENT';
    public $company;
    public $event;
    public $securityManager;
    public $userCompanyRoles = [];

    public function configureMenuItems(): iterable
    {
        // MENU SANS COMPANY
        if (empty($this->company)) {
            yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
            // back button
            yield MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user');
        } else {
            // MENU AVEC COMPANY
            yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
            // yield MenuItem::linkToCrud('The Label', 'icon class', EntityClass::class);
            yield MenuItem::linktoUrl('Add Stage', 'fas fa-dungeon', '/company/'.$this->company->getCompanySlug().'/event/'.$this->event->getEventSlug().'/stage_add');
            // back button
            yield MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user');
        }
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            //->addWebpackEncoreEntry('admin-app')// <-- accessoirement avec webpack de merde

            ->addCssFile('css/post_login.css')
            ->addCssFile('css/main.css')
            ->addCssFile('css/mediaQueries.css')

            ;
    }

    /*  public function configureMenuItems(): iterable
      {

          //---------------ROLE_LIMIT
          if (in_array('ROLE_LIMIT', $this->getUser()->getRoles())) {
              //Home button
              yield MenuItem::linktoRoute('Home', 'fas fa-home', 'show_company_dashboard', ['slug' => $this->company->getSlug()]);

              //Param/User menu
              yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                  menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
              ]);

              // back button
              yield MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user');

              //---------------ROLE_COMPANY_ADMIN
          } elseif (null !== $this->userCompanyRoles && in_array('ROLE_COMPANY_ADMIN', $this->userCompanyRoles, true)) {
              //if ($this->securityManager->is($this->company, ['ROLE_COMPANY_ADMIN'])) { //TODO essayer de trouver soluce pour ça c'est trop relou
              //Home button
              yield MenuItem::linktoRoute('Home', 'fas fa-home', 'show_company_dashboard', ['slug' => $this->company->getSlug()]);


              //Materiel/Stockpile menu
              yield MenuItem::subMenu('Stockpiles', 'fas fa-truck-loading')->setSubItems([
                  MenuItem::linkToCrud('Add stockpile', 'fa fa-file-text', Stockpile::class)
                      ->setAction('new'),
                  //MenuItem::linktoRoute('Show Stockpiles', 'fa fa-tags', '__________')//TODO faire pour que ça ne montre que ceux lié a la structure
              ]);

              //Calendrier/Events menu
              yield MenuItem::subMenu('Calendar', 'far fa-calendar-alt')->setSubItems([
                  MenuItem::linktoRoute('Add events', 'fas fa-calendar-plus', 'app_company_add_event', ['slug' => $this->company->getSlug()]),
                  MenuItem::linktoRoute('Show events', 'fas fa-calendar-week', 'app_company_calendar', ['slug' => $this->company->getSlug()]),
              ]);

              //Contract menu
              yield MenuItem::subMenu('Contract', 'far fa-clipboard')->setSubItems([
              ]);

              //BIG/Investment menu
              yield MenuItem::subMenu('Investments', 'fa fa-comment')->setSubItems([
                  MenuItem::linkToRoute('Add investment', 'fa fa-file-text', 'app_company_add_investment', ['slug' => $this->company->getSlug()]),
                  MenuItem::linktoRoute('Your investments created', 'fa fa-tags', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux créé par la structure
                  MenuItem::linktoRoute('Your investments invested', 'fa fa-tags', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux dans laquels la structure a investit
                  MenuItem::linkToCrud('All investments', 'fa fa-tags', Investment::class)
                      ->setQueryParameter('sortField', 'createdAt')
                      ->setQueryParameter('sortDirection', 'DESC'),
              ]);

              //Accreditation/Users menu
              yield MenuItem::subMenu('Users', 'fa fa-users')->setSubItems([
                  MenuItem::linkToCrud('Add affiliates user', 'fas fa-user-plus', Event::class)// add, show, delete
                  ->setAction('new'),
                  MenuItem::linktoRoute('View affiliate users', 'fa fa-user-friends', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux créé par la structure
                  MenuItem::linktoRoute('Delete an affiliate user', 'fa fa-user-minus', 'user_dashboard'), //TODO faire pour que ça ne montre que ceux créé par la structure
              ]);

              //Param/User menu
              yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                  MenuItem::linkToCrud('Show company detail', 'fas fa-address-cog', Company::class)
                      ->setAction('detail')
                      ->setEntityId($this->company->getId()),
                  MenuItem::linkToCrud('Edit company detail', 'fas fa-user-edit', Company::class)
                      ->setAction('edit')
                      ->setEntityId($this->company->getId()),
                  menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
              ]);

              //--------- ROLE_ADMIN_EVENTS
          } elseif (null !== $this->userCompanyRoles && in_array('ROLE_ADMIN_EVENTS', $this->userCompanyRoles, true)) {
              yield MenuItem::linktoRoute('Role_admin_events', 'far fa-calendar-alt', 'show_company_dashboard');

              //Calendrier/Events menu
              yield MenuItem::subMenu('Calendar', 'far fa-calendar-alt')->setSubItems([
                  MenuItem::linktoRoute('Add events', 'fas fa-calendar-plus', 'app_company_add_event', ['slug' => $this->company->getSlug()]),
                  MenuItem::linktoRoute('Show events', 'fas fa-calendar-week', 'app_company_calendar', ['slug' => $this->company->getSlug()]),
              ]);

              //Param/User menu
              yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                  MenuItem::linkToCrud('Show company detail', 'fas fa-address-cog', Company::class)
                      ->setAction('detail')
                      ->setEntityId($this->company->getId()),
                  MenuItem::linkToCrud('Edit company detail', 'fas fa-user-edit', Company::class)
                      ->setAction('edit')
                      ->setEntityId($this->company->getId()),
                  menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
              ]);
              //--------- NO-ROLE-or error------------------
          } else {
              //Home button
              yield MenuItem::linktoRoute('Home', 'fas fa-home', 'show_company_dashboard', ['slug' => $this->company->getSlug()]);

              //Param/User menu
              yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
                  menuItem::linkToUrl('Security and connection', 'fas fa-user-cog', '/user/settings'),
              ]);

              // back button
              yield MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user');
          }
      }*/
}
