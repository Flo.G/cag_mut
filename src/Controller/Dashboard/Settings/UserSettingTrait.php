<?php

namespace App\Controller\Dashboard\Settings;

use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;

trait UserSettingTrait
{
    use GlobalUserMenuTrait;

    public $dashboard = 'SETTINGS';

    public function configureAssets(): Assets
    {
        return Assets::new()
            //->addWebpackEncoreEntry('admin-app')// <-- accessoirement avec webpack de merde

            ->addCssFile('css/post_login.css')
            ->addCssFile('css/main.css')
            ->addCssFile('css/mediaQueries.css')
            ;
    }

    public function configureMenuItems(): iterable
    {
        //Home button
        //yield MenuItem::linktoRoute('Home', 'fas fa-home', 'show_company_dashboard', ['slug' => $this->company->getSlug()]);

        yield MenuItem::linkToRoute('My profile', 'fa fa-id-card', 'app_user_settings_dashboard_profile');
        yield MenuItem::linkToRoute('Settings', 'fa fa-user-cog', 'app_user_settings_dashboard_settings');
        yield MenuItem::linkToRoute('Security', 'fa fa-lock', 'app_user_settings_dashboard_security');

        // back button
        yield MenuItem::linkToUrl('Back', 'fas fa-undo-alt', '/user');
    }
}
