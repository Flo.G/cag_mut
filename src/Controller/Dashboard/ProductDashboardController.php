<?php

namespace App\Controller\Dashboard;

use App\Controller\Admin\Stockpile\StockpileCrudController;
use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Entity\Product\Product;
use App\Repository\Company\UserRoleRepository;
use App\Service\Company\CompanyManager;
use App\Service\Product\ProductManager;
use App\Service\SecurityManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductDashboardController extends AbstractDashboardController
{
    use CompanyTrait;

    /**
     * @Route("/products/index", name="products_index")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(StockpileCrudController::class)->generateUrl());
    }

    /**
     * @Route("/company/{companySlug}/products", name="app_company_products")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function productsDashboard(Company $company,
                                      CompanyManager $companyManager,
                                      SecurityManager $securityManager,
                                      ProductManager $productManager): Response
    {

        $this->company = $company;
        $this->userCompanyRoles = $companyManager->getUserRoles($company);

        if (!$securityManager->is($this->company, ['ROLE_COMPANY_ADMIN'])) {
            return $this->redirectToRoute('app_company_dashboard', ['companySlug' => $this->company->getCompanySlug()]);
        }

        $products = $productManager->getAllByCompany($this->company);

        return $this->render('easyAdmin/product/index.html.twig', [
            'products' => $products,
            'company' => $this->company,
        ]);
    }

    /**
     * @route("/compagny/{companySlug}/product/{productReference}", name="app_compagny_product_details")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function producDetail(Company $company,
                                 Product $product,
                                 CompanyManager $companyManager,
                                 SecurityManager $securityManager
                                                                    ){
        $this->company = $company;
        $this->userCompanyRoles = $companyManager->getUserRoles($company);

        if (!$securityManager->is($this->company, ['ROLE_COMPANY_ADMIN'])) {
            return $this->redirectToRoute('app_company_dashboard', ['companySlug' => $this->company->getCompanySlug()]);
        }

        $categories = $product->getCategories()->getValues();

        $parents = [];
        foreach ($categories as $category){
            $parents[] = $category->getParent();
        }


        return $this->render('easyAdmin/product/product.html.twig',[
            'company'=>$this->company,
            'product'=>$product,
            'investment'=>$product->getInvestment(),
            'categories'=> $product->getCategories()->getValues(),
            'stockpile'=> $product->getStockpile(),
            'catParents'=>$parents,
            'limitTypeLimitations'=>$product->getTagTypeLimitations()->getValues(),
            'imgProducts'=>$product->getImages()->getValues()
        ]);
    }


}
