<?php

namespace App\Controller\Dashboard;

use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Controller\Dashboard\Settings\PostLoginTrait;
use App\Entity\Company\Company;
use App\Service\Product\ProductManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogDashboardController extends AbstractDashboardController
{
    use CompanyTrait;

    /**
     * @Route("/catalog/index", name="index_catalog")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        return $this->redirect('/user');
    }

    /**
     * @Route("/company/{companySlug}/catalog", name="app_company_catalog")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function PlatformProductsDashboard(Company $company, ProductManager $productManager): Response
    {
        $this->company = $company;
        $products = $productManager->generateCatalogueData(true);

        return $this->render('easyAdmin/product/catalog.html.twig', [
            'products' => $products,
            'company' => $this->company
        ]);
    }

    /**
     * @Route("/catalog", name="app_catalog")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function CatalogDashboard(ProductManager $productManager): Response
    {
        $products = $productManager->generateCatalogueData(true);

        return $this->render('easyAdmin/product/unlinked_catalog.html.twig', [
            'products' => $products,
        ]);
    }
}
