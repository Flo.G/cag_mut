<?php

namespace App\Controller\Dashboard;

use App\Controller\Dashboard\Settings\PostLoginTrait;
use App\Entity\User\User;
use App\Service\Company\CompanyManager;
use App\Service\Event\EventManager;
use App\Service\SecurityManager;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    use PostLoginTrait;

    /**
     * @Route("/use", name="app_user")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        /*        // redirect to some CRUD controller
                $routeBuilder = $this->get(AdminUrlGenerator::class);

                return $this->redirect($routeBuilder->setController(OneOfYourCrudController::class)->generateUrl());

                // you can also redirect to different pages depending on the current user
                if ('jane' === $this->getUser()->getUsername()) {
                    return $this->redirect('...');
                }

                // you can also render some template to display a proper Dashboard
                // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
                return $this->render('some/path/my-dashboard.html.twig');*/

        return $this->redirect('/user');
    }

    /**
     * @Route("/user", name="app_user_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function UserDashboard(CompanyManager $companyManager, EventManager $eventManager, SecurityManager $securityManager): Response
    {
        $securityManager->ownSecurity();

        if ($this->IsGranted('ROLE_AFFILIATED')) {
            $companyRoles = $companyManager->showByUser();
            $eventRoles = $eventManager->showByUser();
            //dd($companyRoles);

            return $this->render('easyAdmin/user/index.html.twig', [
                'companyRoles' => $companyRoles,
                'eventRoles' => $eventRoles,
            ]);
        }

        return $this->render('easyAdmin/user/index.html.twig');
    }
}
