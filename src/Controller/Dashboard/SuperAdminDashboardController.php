<?php

namespace App\Controller\Dashboard;

use App\Controller\Admin\User\UserCrudController;

use App\Controller\Dashboard\Settings\SuperAdminTrait;
use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Entity\File\Image\CategoryImage;
use App\Entity\Product\Product;
use App\Entity\Product\TagTypeLimitation;
use App\Entity\Setting\Category;
use App\Entity\User\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use phpDocumentor\Reflection\DocBlock\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SuperAdminDashboardController extends AbstractDashboardController
{
    use SuperAdminTrait;

    /**
     * @Route("/super_admin", name="super_admin_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $waitingConfirmationCompanysData = $entityManager->getRepository(\App\Entity\Company\Company::class)->findBy(['enabled' => false]);

        return $this->render('easyAdmin/admin/sa_post_login.html.twig', [
            'companys' => $waitingConfirmationCompanysData,
        ]);
        //TODO make fixture all enabled

        //$routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        //return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl());
    }


}
