<?php

namespace App\Controller\Dashboard;

use App\Controller\Admin\Event\EventCrudController;
use App\Controller\Dashboard\Settings\EventTrait;
use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Service\Event\StageManager;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventDashboardController extends AbstractDashboardController
{
    use EventTrait;

    /**
     * @Route("/event/", name="index_event")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(EventCrudController::class)->generateUrl());
    }

    /**
     * @Route("/event/{eventSlug}", name="app_event_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function eventDashboard(Event $event, TranslatorInterface $translator): Response
    {
        $this->event = $event;

        if (!$this->event) {
            // Si aucun article n'est trouvé, nous créons une exception
            throw $this->createNotFoundException($translator->trans('The event was not found.'));
        }
        // Si l'article existe nous envoyons les données à la vue
        return $this->render('easyAdmin/event/index.html.twig', [
            'event' => $this->event,
        ]);
    }

    /**
     * @Route("/company/{companySlug}/event/{eventSlug}", name="app_company_event_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function eventCompDashboard(Company $company,
                                       Event $event,
                                       StageManager $stageManager,
                                       TranslatorInterface $translator): Response
    {
        $this->event = $event;
        $this->company = $company;
        $stages = $stageManager->getByEvent($this->event);

        if (!$this->event) {
            // Si aucun article n'est trouvé, nous créons une exception
            throw $this->createNotFoundException($translator->trans('The event was not found.'));
        }
        // Si l'article existe nous envoyons les données à la vue
        return $this->render('easyAdmin/event/index.html.twig', [
            'company' => $this->company,
            'event' => $this->event,
            'stages' => $stages,
        ]);
    }
}
