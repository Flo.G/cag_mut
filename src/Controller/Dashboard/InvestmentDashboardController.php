<?php

namespace App\Controller\Dashboard;

use App\Controller\Admin\Investment\InvestmentCrudController;
use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Entity\Investment\Investment;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvestmentDashboardController extends AbstractDashboardController
{
    use CompanyTrait;

    /**
     * @Route("/investment", name="investment")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(InvestmentCrudController::class)->generateUrl());
    }

    /**
     * @Route("/company/{companySlug}/investment/{investmentSlug}", name="app_company_investment_dashboard")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function investmentDashboard(Company $company, Investment $investment): Response
    {
        $this->company = $company;
        $this->investment = $investment;

        if (!$this->investment) {
            // Si aucun article n'est trouvé, nous créons une exception
            throw $this->createNotFoundException('The investment was not found.'); //TODO had translate
        }
        // Si l'article existe nous envoyons les données à la vue
        return $this->render('easyAdmin/investment/investment.html.twig', [
            'investment' => $this->investment,
            'company' => $this->company,
        ]);
    }
}
