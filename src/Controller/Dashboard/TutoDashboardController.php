<?php

namespace App\Controller\Dashboard;

use App\Controller\Dashboard\Settings\CompanyTrait;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class TutoDashboardController extends AbstractDashboardController
{
    use CompanyTrait;

    //-------------------------------Easy admin config fonctions------------------------------------------------

    public function configureMenuItems(): iterable
    {
        // home button
        yield MenuItem::linkToUrl('Home', 'fas fa-home', '/user');

        //Materiel menu
        yield MenuItem::linktoRoute('Stockpiles', 'fas fa-truck-loading', 'show_structure_dashboard');

        //Calendrier menu
        yield MenuItem::subMenu('Calendar', 'far fa-calendar-alt')->setSubItems([
        ]);

        //Contract menu
        yield MenuItem::subMenu('Contract', 'far fa-clipboard')->setSubItems([
        ]);

        //Big menu
        yield MenuItem::subMenu('Big', 'fab fa-bitcoin')->setSubItems([
        ]);

        //Accreditation menu
        yield MenuItem::subMenu('Accreditation', 'fas fa-project-diagram')->setSubItems([
        ]);

        //Other param menu
        yield MenuItem::subMenu('Parameters', 'fas fa-cog')->setSubItems([
        ]);
    }
}
