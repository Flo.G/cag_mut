<?php

namespace App\Controller\User\Registration;

use App\Controller\Dashboard\TutoDashboardController;
use App\Entity\File\Image\Avatar;
use App\Form\BaseType\VichAddImageType;
use App\Form\Registration\User\AddContactType;
use App\Form\Registration\User\EmailsType;
use App\Form\Registration\User\NicknameType;
use App\Service\Upload\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FinishRegistrationUserController extends TutoDashboardController
{
    /**
     * @Route("/registration/user/welcome", name="app_user_register_welcome")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        return $this->render('easyAdmin/user/registration/welcome.html.twig');
    }

    /**
     * @Route("/registration/user/present", name="app_user_register_present")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addNickname(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(NicknameType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute('app_user_register_email');
        }

        return $this->render('easyAdmin/user/registration/je_suis.html.twig', [
            'names' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/user/add_email", name="app_user_register_email")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addEmail(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(EmailsType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute('app_user_register_contact');
        }

        return $this->render('easyAdmin/user/registration/add_other_mail.html.twig', [
            'emails' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/user/add_contact", name="app_user_register_contact")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addContact(Request $request, EntityManagerInterface $manager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(AddContactType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute('app_user_register_avatar');
        }

        return $this->render('easyAdmin/user/registration/add_contacts.html.twig', [
            'contact' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/user/add_avatar", name="app_user_register_avatar")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addAvatar(Request $request, EntityManagerInterface $manager, FileUploader $fileUploader): Response
    {
        $user = $this->getUser();

        if ($user->getAvatar()) {
            $avatar = $user->getAvatar();
        } else {
            $avatar = new Avatar();
        }

        $form = $this->createForm(VichAddImageType::class, $avatar);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $avatar->setUser($user);
            $avatar->setFilePath($avatar::PATH);
            $avatar->setDescription($avatar::DESCRIPTION);
            $user->setAvatar($avatar);
            $manager->persist($avatar);
            $fileUploader->filter($avatar);
            $manager->flush();

            return $this->redirectToRoute('app_user_register_welcome');
        }

        return $this->render('easyAdmin/user/registration/vich_add_image.html.twig', [
            'image' => $form->createView(),
        ]);
    }
}
