<?php

namespace App\Controller\User\Registration;

use App\Controller\Dashboard\TutoDashboardController;
use App\Entity\Company\Company;
use App\Entity\Company\UserRole as CompUserRole;
use App\Entity\Event\Event;
use App\Entity\Event\UserRole as EventUserRole;
use App\Entity\User\User;
use App\Form\Registration\User\LinkCompanyType;
use App\Form\Registration\User\LinkEventType;
use App\Form\Registration\User\SearchCompanyType;
use App\Mail\UserToCompanyMail;
use App\Service\Company\CompanyManager;
use App\Service\Event\EventManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AffiliationController extends TutoDashboardController
{
    private $userToCompanyMail;
    private $noReplyEmail;

    public function __construct(UserToCompanyMail $userToCompanyMail)
    {
        $this->userToCompanyMail = $userToCompanyMail;
        $this->noReplyEmail = $_ENV['NO_REPLY_EMAIL'];
    }

    /**
     * @Route("/registration/user/congrats", name="app_user_register_link_congrats")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        return $this->render('easyAdmin/user/registration/congrats.html.twig');
    }

    /**
     * @Route("/registration/user/search_company", name="app_user_register_link_search")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function search(Request $request, EntityManagerInterface $manager)
    {
        $company = [];
        $form = $this->createForm(SearchCompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!is_null($form->getData()['eventSlug'])) {
                $result = $form->getData()['eventSlug'];

                return $this->redirectToRoute(('app_user_register_link_event'), ['eventSlug' => $result->getEventSlug()]);
            }
            $result = $form->getData()['slug'];

            return $this->redirectToRoute(('app_user_register_link_company'), ['companySlug' => $result->getCompanySlug()]);
        }

        return $this->render('easyAdmin/user/registration/search_company.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/user/link_company/{companySlug}", name="app_user_register_link_company")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function companyAffiliation(Company $company,
                                       Request $request,
                                       EntityManagerInterface $manager,
                                       CompanyManager $companyManager,
                                       TranslatorInterface $trans): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $choiceRoles = [];
        $form = $this->createForm(LinkCompanyType::class, $choiceRoles);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $choiceRoles = $form->getNormData();

            foreach ($choiceRoles['name'] as $role) {
                $alreadyAff = $companyManager->alreadyAffiliated($company, $role->getName(), $user);

                if (!is_null($alreadyAff)) {
                    $message = 'You are already affiliated as '.$role->getLabel().' with this company.';
                    $this->addFlash('verify_email_error', $message);
                } else {
                    $userRole = new CompUserRole();
                    $userRole->setUser($user);
                    $userRole->setCompany($company);
                    $userRole->setRoles($role->getName());
                    $user->addRole('ROLE_AFFILIATED'); //TODO fai buger setroles addRole aussi
                    $manager->persist($userRole);
                    $manager->flush();

                    // generate a signed url and email it to the Company email
                    $this->userToCompanyMail->askCompAffiliationMail($userRole,
                        (new TemplatedEmail())
                            ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                            ->to($userRole->getCompany()->getEmail())
                            ->subject('Please Confirm the affiliation asking')
                            ->htmlTemplate('email/affiliation_confirmation_email.html.twig')
                    );
                    $message = $trans->trans('An email has been send to the administrator of the structure to validate your affiliation.');
                    $this->addFlash('success', $message);
                }
            }

            return $this->redirectToRoute('app_user_register_link_congrats');
        }

        return $this->render('easyAdmin/user/registration/link_company.html.twig', [
            'form' => $form->createView(),
            'company' => $company,
        ]);
    }

    /**
     * @Route("/registration/user/link_event/{eventSlug}", name="app_user_register_link_event")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function eventAffiliation(Event $event,
                                     Request $request,
                                     EntityManagerInterface $manager,
                                     EventManager $eventManager,
                                     CompanyManager $companyManager,
                                     TranslatorInterface $trans): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $choiceRoles = [];

        $choiceRoles['company'] = $companyManager->getCompFromEvent($event);
        $form = $this->createForm(LinkEventType::class, $choiceRoles);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $choiceRoles = $form->getNormData();
            foreach ($choiceRoles['name'] as $role) {
                $alreadyAff = $eventManager->alreadyAffiliated($event, $role->getName(), $user);

                if (!is_null($alreadyAff)) {
                    $message = 'You are already affiliated as '.$role->getLabel().' with this event.'; // TODO flash translation
                    $this->addFlash('verify_email_error', $message);
                } else {
                    $userRole = new EventUserRole();
                    $userRole->setUser($user);
                    $userRole->setEvent($event);
                    $userRole->setCompany($choiceRoles['company']);
                    $userRole->setRoles($role->getName());
                    $user->setRoles(['ROLE_AFFILIATED']);
                    $manager->persist($userRole);
                    $manager->flush();

                    // generate a signed url and email it to the Company email
                    $this->userToCompanyMail->askEventAffiliationMail($userRole,
                        (new TemplatedEmail())
                            ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                            ->to($choiceRoles['company']->getEmail())
                            ->subject('Please Confirm the affiliation asking')
                            ->htmlTemplate('email/affiliation_event_confirmation_email.html.twig')
                    );
                    $message = $trans->trans('An email has been send to the administrator of the structure to validate your affiliation.');
                    $this->addFlash('success', $message);
                }
            }

            return $this->redirectToRoute('app_user_register_link_congrats');
        }

        return $this->render('easyAdmin/user/registration/link_event.html.twig', [
            'form' => $form->createView(),
            'event' => $event,
        ]);
    }
}
