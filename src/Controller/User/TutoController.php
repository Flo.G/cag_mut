<?php

namespace App\Controller\User;

use App\Controller\Dashboard\TutoDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TutoController extends TutoDashboardController
{
    /**
     * @Route("/tutorial/welcome", name="app_user_welcome")
     */
    public function index(): Response
    {
        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/user.html.twig',[
            'user' => $user,
        ]);
    }
    /**
     * @Route("/tutorial/user", name="app_user_tutorial")
     */
    public function tutorial(): Response
    {
        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/tutorial.html.twig',[
            'user'=>$user
        ]);
    }
    /**
     * @Route("/tutorial/status", name="app_user_status")
     */
    public function status(): Response
    {
        $status= "status";
        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/status.html.twig',[
            'user'=>$user,
            'status'=>$status
        ]);
    }
    /**
     * @Route("/tutorial/home", name="app_user_home")
     */

    public function home(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/home.html.twig',[
            'user'=>$user,

        ]);
    }

    /**
     * @Route("/tutorial/equipment", name="app_user_equipment")
     */

    public function equipment(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/equipment.html.twig',[
            'user'=>$user,

        ]);
    }
    /**
     * @Route("/tutorial/calendar", name="app_user_calendar")
     */

    public function calendar(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/calendar.html.twig',[
            'user'=>$user,

        ]);
    }
    /**
     * @Route("/tutorial/contract", name="app_user_contract")
     */

    public function contract(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/contract.html.twig',[
            'user'=>$user,

        ]);
    }
    /**
     * @Route("/tutorial/big", name="app_user_big")
     */

    public function big(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/big.html.twig',[
            'user'=>$user,

        ]);
    }
    /**
     * @Route("/tutorial/accreditation", name="app_user_accreditation")
     */

    public function accreditation(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/accreditation.html.twig',[
            'user'=>$user,

        ]);
    }

    /**
     * @Route("/tutorial/setting", name="app_user_setting")
     */

    public function setting(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/setting.html.twig',[
            'user'=>$user,

        ]);
    }
    /**
     * @Route("/tutorial/cart", name="app_user_cart")
     */

    public function cart(): Response
    {
        $shoppingCart='shoppingCart';
        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/cart.html.twig',[
            'user'=>$user,
            'shoppingCart'=>$shoppingCart
        ]);
    }
    /**
     * @Route("/tutorial/question", name="app_user_question")
     */

    public function question(): Response
    {
        $question='question';
        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/question.html.twig',[
            'user'=>$user,
            'question'=>$question
        ]);
    }

    /**
     * @Route("/tutorial/endPage", name="app_user_endPage")
     */

    public function endPage(): Response
    {

        $user = $this->getUser();
        return $this->render('easyAdmin/tutorial/endPage.html.twig',[
            'user'=>$user,
        ]);
    }

}
