<?php

namespace App\Controller\User\Settings;

use App\Controller\Dashboard\UserSettingsDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SettingsController extends UserSettingsDashboardController
{
    /**
     * @Route("/user/settings/profile", name="user_settings_dashboard_profile")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function setProfile(): Response
    {
        $this->user = $this->getUser();

        if (!$this->user->getEnabled()) {
            return $this->render('easyAdmin/user/error/disabled.html.twig');
        }
        if (!$this->user->getIsVerified()) {
            return $this->render('easyAdmin/user/error/not_verified.html.twig');
        }
        if ($this->isGranted('ROLE_PWD_CHANGE')) {
            return $this->redirectToRoute('password_wait');
        }

        return $this->render('easyAdmin/user/settings/security.html.twig');
    }

    /**
     * @Route("/user/settings/setting", name="user_settings_dashboard_settings")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function setSetting(): Response
    {
        $this->user = $this->getUser();

        if (!$this->user->getEnabled()) {
            return $this->render('easyAdmin/user/error/disabled.html.twig');
        }
        if (!$this->user->getIsVerified()) {
            return $this->render('easyAdmin/user/error/not_verified.html.twig');
        }
        if ($this->isGranted('ROLE_PWD_CHANGE')) {
            return $this->redirectToRoute('password_wait');
        }

        return $this->render('easyAdmin/user/settings/security.html.twig');
    }

    /**
     * @Route("/user/settings/security", name="app_user_settings_dashboard_security")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function setSecurity(): Response
    {
        $this->user = $this->getUser();

        if (!$this->user->getEnabled()) {
            return $this->render('easyAdmin/user/error/disabled.html.twig');
        }
        if (!$this->user->getIsVerified()) {
            return $this->render('easyAdmin/user/error/not_verified.html.twig');
        }
        if ($this->isGranted('ROLE_PWD_CHANGE')) {
            return $this->redirectToRoute('password_wait');
        }

        return $this->render('easyAdmin/user/settings/security.html.twig');
    }
}
