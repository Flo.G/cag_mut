<?php

namespace App\Controller\Investment;

use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Entity\Investment\CompanyRole;
use App\Entity\Investment\Investment;
use App\Entity\Setting\InvestModel;
use App\Entity\User\User;
use App\Form\Investment\ChoiceCostType;
use App\Form\Investment\ChoiceInvestModelType;
use App\Form\Investment\InvestmentChoiceTagTypeLimitationType;
use App\Form\Investment\InvestmentType;
use App\Mail\NewCompanyMail;
use App\Security\LoginFormAuthenticator;
use App\Service\Company\CompanyManager;
use App\Service\SecurityManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class AddInvestmentController extends abstractDashboardController
{
    use CompanyTrait;

    private $outForm;
    private $noReplyEmail;
    private $superAdminEmail;
    private $newCompanyMail;


    public function __construct(NewCompanyMail $newCompanyMail)
    {
        $this->outForm = 'app_company_investments';
        $this->noReplyEmail = $_ENV['NO_REPLY_EMAIL'];
        $this->superAdminEmail = $_ENV['SUPER_ADMIN_EMAIL'];
        $this->newCompanyMail = $newCompanyMail;
    }

    /**
     * @Route("/company/{companySlug}/add/investment", name="app_company_add_investment")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addInvestment(Company $company, CompanyManager $companyManager, SecurityManager $securityManager, LoginFormAuthenticator $authenticator, Request $request, EntityManagerInterface $manager): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_choice_model';
        $this->company = $company;
        $investment = new Investment();
        $companyRoles = new CompanyRole();
        $this->userCompanyRoles = $companyManager->getUserRoles($company);

        $form = $this->createForm(InvestmentType::class, $investment);

        $form->handleRequest($request);
        //dd($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $companyRoles->setRoles(['ROLE_INVESTOR']);
            $companyRoles->setCompany($company);
            $companyRoles->setInvestment($investment);
            $manager->persist($companyRoles);
            $manager->persist($investment);

            $manager->flush();

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
            ]);
        }

        return $this->render('easyAdmin/investment/addInvestment.html.twig', [
            'investmentForm' => $form->createView(),

        ]);
    }

    /**
     * @Route("/company/{companySlug}/investment/{investmentSlug}/choice_model", name="app_company_investment_choice_model")
     */
    public function investmentChoiceModel(Company $company,
                                          Investment $investment,
                                          Request $request,
                                          EntityManagerInterface $manager,
                                          SecurityManager $securityManager)

    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_tag_type_limitation';
        $this->company = $company;
        $this->investment = $investment;

        $form = $this->createForm(ChoiceInvestModelType::class);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            //dd($form->getData()["your_investment"]==="1");
            if($form->getData()["your_investment"]==="1"){
                $this->redirectToRoute("app_company_investment_cost",[
                    'companySlug' => $this->company,
                    'investmentSlug' => $investment->getInvestmentSlug()]);
            }
            $manager->flush();
        }
        return $this->render('easyAdmin/investment/choice_invest_model.html.twig', [
            'modelForm' => $form->createView(),
            'investmentSlug' => $investment->getInvestmentSlug(),
            'outForm' => $this->outForm,
        ]);
    }

    /**
     * @Route("/compnay/{companySlug}/investment/{investmentSlug}/cost", name="app_company_investment_cost")
     */
    public function investmentChoiceCost(Company $company,
                                         Investment $investment,
                                         InvestModel $model,
                                         Request $request,
                                         EntityManagerInterface $manager,
                                         SecurityManager $securityManager)
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $this->company = $company;
        $this->investment = $investment;
        $nextPage = 'app_company_investment_tag_type_limitation';

        $form = $this->createForm(ChoiceCostType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $investment->setInvestModel()->getId()['11'];
            $model->getInvestments();

            $manager->persist($investment);
            $manager->persist($model);
            $manager->flush();

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
            ]);
        }

        return $this->render('easyAdmin/investment/choice_cost.html.twig', [
            'costForm' => $form->createView(),
            'investmentSlug' => $investment->getInvestmentSlug(),
            'outForm' => $this->outForm,
        ]);
    }

    /**
     * @Route("/company/{companySlug}/investment/{investmentSlug}/tag_type_limitaion", name="app_company_investment_tag_type_limitation")
     */
    public function investmentChoiceTagTypeLimitation(Company $company,
                                                      Investment $investment,
                                                      Request $request,
                                                      EntityManagerInterface $manager,
                                                      SecurityManager $securityManager)
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $this->company = $company;
        $this->investment = $investment;

        $form = $this->createForm(InvestmentChoiceTagTypeLimitationType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if( isset($form->getData()['tagTypeLimitations']['0'])){
                $tagType = $form->getData()['tagTypeLimitations']['0'];
                $investment->addTagTypeLimitation($tagType);
            }
            $manager->flush();


            $this->newCompanyMail->newCompanyMail('app_user', $company,
                (new TemplatedEmail())
                    ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                    ->to(new Address($this->superAdminEmail, 'Super_Administrateur'))
                    ->subject('Please Confirm new Investment')
                    ->htmlTemplate('email/new_investment.html.twig')
            );

            return $this->render('easyAdmin/investment/investment_validation.html.twig',[
                'companySlug' => $this->company->getCompanySlug(),
                'investment' => $this->investment,
            ]);
        }
        return $this->render('easyAdmin/investment/investment_choice_tag_type_limitation.html.twig', [
            'form' => $form->createView(),
            'investmentSlug' => $investment->getInvestmentSlug(),
            'outForm' => $this->outForm,
        ]);
    }
}
