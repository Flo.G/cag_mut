<?php

namespace App\Controller\Investment;

use App\Repository\Investment\TagTypeLimitationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/tag_type_limitation_autocomplete.json", name="api_tag_type_limitation_autocomplete")
     */
    public function LineAutocomplete(Request $request, TagTypeLimitationRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }
}
