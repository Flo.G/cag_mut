<?php

namespace App\Controller\Investment;

use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Entity\Investment\TagTypeLimitation;
use App\Form\Investment\RequestTagType;
use App\Service\SecurityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RequestTagTypeLimitationController extends AbstractController
{
    use CompanyTrait;

    /**
     * @Route("/investment/{investmentSlug}/request/tag/type/limitation", name="app_investment_request_tag_type_limitation")
     */
    public function addTagTypeLimitation(Request $request,SecurityManager $securityManager, Company $company, EntityManagerInterface $manager): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $tagType = new TagTypeLimitation();

        $form = $this->createForm(RequestTagType::class, $tagType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();
        }

        return $this->render('easyAdmin/investment/request_tag_type.html.twig', [
            'controller_name' => 'RequestTagTypeController',
        ]);
    }
}
