<?php

namespace App\Controller\Company;

use App\Entity\Company\Company;
use App\Repository\Company\CompanyRepository;
use App\Repository\Setting\CompanyUserRoleRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController
{
    /**
     * @Route("/company_autocomplete.json", name="api_company_autocomplete")
     */
    public function CompanyAutocomplete(Request $request, CompanyRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }

    /**
     * @Route("/company_user_role_autocomplete.json", name="api_company_user_role_autocomplete")
     */
    public function CompanyUserRoleAutocomplete(Request $request, CompanyUserRoleRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }
}
