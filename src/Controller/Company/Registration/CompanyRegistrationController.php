<?php

namespace App\Controller\Company\Registration;

use App\Controller\Admin\Company\CompanyCrudController;
use App\Controller\Dashboard\TutoDashboardController;
use App\Entity\Company\Company;
use App\Entity\Company\UserRole;
use App\Entity\File\Image\Banner;
use App\Entity\File\Image\Logo;
use App\Entity\Investment\CompanyRole;
use App\Entity\Investment\Investment;
use App\Entity\User\User;
use App\Form\BaseType\VichAddImageType;
use App\Form\Registration\Company\AddAddressType;
use App\Form\Registration\Company\ChooseAffiliationCreationType;
use App\Form\Registration\Company\CompanyRegistrationFormType;
use App\Form\Registration\Company\VerifyMailFormType;
use App\Mail\NewCompanyMail;
use App\Repository\Company\CompanyRepository;
use App\Repository\Event\EventRepository;
use App\Security\LoginFormAuthenticator;
use App\Service\Company\CompanyManager;
use App\Service\Event\EventManager;
use App\Service\Upload\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CompanyRegistrationController extends TutoDashboardController
{
    private $newCompanyMail;
    private $superAdminEmail;
    private $noReplyEmail;

    public function __construct(NewCompanyMail $newCompanyMail)
    {
        $this->newCompanyMail = $newCompanyMail;
        $this->superAdminEmail = $_ENV['SUPER_ADMIN_EMAIL'];
        $this->noReplyEmail = $_ENV['NO_REPLY_EMAIL'];
    }

    /**
     * @Route("/registration/company", name="app_company_register")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

        return $this->redirect($routeBuilder->setController(CompanyCrudController::class)->generateUrl());
    }

    /**
     * @Route("/registration/company/name", name="app_company_register_name")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function register(Request $request, CompanyManager $companyManager, EventManager $eventManager, SerializerInterface $serializer): Response
    {
        $company = new Company();

        $form = $this->createForm(CompanyRegistrationFormType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nameCompanyExist = $companyManager->getByName($company);
            $siretExist = $companyManager->getBySiret($company);
            $nameEventExist = $eventManager->getByName($company->getName());

            if ((isset($nameCompanyExist) && !empty($nameCompanyExist)) || (isset($nameEventExist) && !empty($nameEventExist))) {
                if (isset($nameCompanyExist) && !empty($nameCompanyExist)) {
                    foreach ($nameCompanyExist as $val) {
                        $companiesID[] = $val->getId();
                    }
                    $choices['company'] = $companiesID;
                }
                if (isset($nameEventExist) && !empty($nameEventExist)) {
                    foreach ($nameEventExist as $val) {
                        $eventsID[] = $val->getId();
                    }
                    $choices['event'] = $eventsID;
                }

                $jChoices = $serializer->serialize($choices, 'json');

                return $this->redirectToRoute('app_company_register_choice_link_create', [
                    'jChoices' => $jChoices,
                    'name' => $company->getName(),
                    'siret' => $company->getSiret(),
                ]);
            }
            if (isset($siretExist) && !empty($siretExist)) {
                $company = $siretExist[0];

                return $this->redirectToRoute('app_siret_already_register', [
                    'companySlug' => $company->getCompanySlug(),
                ]);
            }

            return $this->redirectToRoute('app_company_register_address', ['name' => $company->getName(), 'siret' => $company->getSiret()]);
        }

        return $this->render('easyAdmin/company/registration/company_register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/company/siret_already/{companySlug}", name="app_siret_already_register")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function alreadySiret(Company $company): Response
    {
        return $this->render('easyAdmin/company/registration/already_siret.html.twig', [
            'company' => $company,
        ]);
    }

    /**
     * @Route("/registration/company/choice_link_create/{jChoices}/{name}/{siret}", name="app_company_register_choice_link_create")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @throws \JsonException
     */
    public function choiceLinkCreate(string $jChoices, string $name, string $siret, Request $request, CompanyRepository $companyRepository, EventRepository $eventRepository): Response
    {
        $choices = json_decode($jChoices, true, 512, JSON_THROW_ON_ERROR);

        if (isset($choices['company'])) {
            foreach ($choices['company'] as $val) {
                $choice['company'] = $companyRepository->findBy(['id' => $val]);
            }
        }

        if (isset($choices['event'])) {
            foreach ($choices['event'] as $val) {
                $choice['event'] = $eventRepository->findBy(['id' => $val]);
            }
        }

        $choice['name'] = $name;
        $choice['siret'] = $siret;

        $form = $this->createForm(ChooseAffiliationCreationType::class, $choice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $id = 0;
            $className = 'Registration';

            $choice = $form->getNormData();

            unset($choice['name'], $choice['siret']);
            $choice = array_filter($choice);

            foreach ($choice as $val) {
                $id = $val->getId();
                $className = get_class($val);
            }

            return $this->redirectToRoute('app_company_event_affiliation', ['id' => $id, 'class' => $className]);
        }

        return $this->render('easyAdmin/company/registration/link_choice.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/link_choice/{id}/{class}", name="app_company_event_affiliation")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function choiceLinkTo(string $id, string $class, Request $request, CompanyRepository $companyRepository, EventRepository $eventRepository): Response
    {
        if ('App\Entity\Company\Company' === $class) {
            $company = $companyRepository->find($id);

            return $this->redirectToRoute('app_user_register_link_company', ['slug' => $company->getCompanySlug()]);
        }
        $event = $eventRepository->find($id);

        return $this->redirectToRoute('app_user_register_link_event', ['slug' => $event->getEventSlug()]);
    }

    /**
     * @Route("/registration/company/adress/{name}/{siret}", name="app_company_register_address")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addAddress($name, $siret, Request $request, EntityManagerInterface $manager): Response
    {
        $company = new Company();
        $company->setName($name);
        $company->setSiret($siret);

        $form = $this->createForm(AddAddressType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*dd($company);*/
            $manager->persist($company);
            $manager->flush();

            return $this->redirectToRoute('app_company_register_logo', ['company' => $company->getId()]);
        }

        return $this->render('easyAdmin/company/registration/add_address.html.twig', [
            'address' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/company/add_logo", name="app_company_register_logo")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addLogo(Request $request, EntityManagerInterface $manager, CompanyManager $companyManager, FileUploader $fileUploader): Response
    {
        $company = $companyManager->getByRequest($request);

        if ($company->getLogo()) {
            $logo = $company->getLogo();
        } else {
            $logo = new Logo();
        }

        $form = $this->createForm(VichAddImageType::class, $logo);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo->setCompany($company);
            $logo->setFilePath($logo::PATH);
            $logo->setDescription($logo::DESCRIPTION);
            $company->setLogo($logo);
            $manager->persist($logo);
            $fileUploader->filter($logo);
            $manager->flush();

            //return $this->redirectToRoute('app_company_register_banner', ['company' => $company->getId()]);
            return $this->redirectToRoute('app_company_register_email', ['company' => $company->getId()]);
        }

        return $this->render('easyAdmin/company/registration/vich_add_logo.html.twig', [
            'image' => $form->createView(),
        ]);
    }

    /**
     * Route("/registration/company/add_banner", name="app_company_register_banner")
     * IsGranted("IS_AUTHENTICATED_FULLY").
     */
/*    public function addBanner(Request $request, EntityManagerInterface $manager, CompanyManager $companyManager, FileUploader $fileUploader): Response
    {
        $company = $companyManager->getByRequest($request);

        if ($company->getBanner()) {
            $banner = $company->getBanner();
        } else {
            $banner = new Banner();
        }

        $form = $this->createForm(VichAddImageType::class, $banner);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $banner->setCompany($company);
            $banner->setFilePath($banner::PATH);
            $banner->setDescription($banner::DESCRIPTION);
            $company->setBanner($banner);
            $manager->persist($banner);
            $fileUploader->filter($banner);
            $manager->flush();

            return $this->redirectToRoute('app_company_register_email', ['company' => $company->getId()]);
        }

        return $this->render('easyAdmin/company/registration/vich_add_banner.html.twig', [
                'image' => $form->createView(),
            ]);
    }*/

    /**
     * @Route("/registration/company/email", name="app_company_register_email")
     */
    public function verifyMail(Request $request, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, CompanyManager $companyManager): Response
    {
        /** @var User $userAdmin */
        $userAdmin = $this->getUser();
        $company = $companyManager->getByRequest($request);
        $userRole = new UserRole();

        $form = $this->createForm(VerifyMailFormType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company->setActivationToken(md5(uniqid('', true)));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $userRole->setRoles(['ROLE_COMPANY_ADMIN']);
            $userRole->setCompany($company);
            $userRole->setUser($userAdmin);
            $userRole->setIsVerified(true);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userRole);
            $entityManager->flush();

            $userAdmin->setRoles(['ROLE_AFFILIATED']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            // generate a signed url and email it to the SUPER_ADMIN
            $this->newCompanyMail->newCompanyMail('app_login', $company,
                (new TemplatedEmail())
                    ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                    ->to(new Address($this->superAdminEmail, 'Super_Administrateur'))
                    ->subject('Please Confirm new Company')
                    ->htmlTemplate('email/new_company_email.html.twig')
            );

            // generate a signed url and email it to the Company email
            $this->newCompanyMail->sendCompanyEmailConfirmation('app_verify_email_company', $userAdmin, $company,
                  (new TemplatedEmail())
                      ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                      ->to($company->getEmail())
                      ->subject('Please Confirm your Email')
                      ->htmlTemplate('email/company_confirmation_email.html.twig')
              );

            return $guardHandler->authenticateUserAndHandleSuccess(
                $userAdmin,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('easyAdmin/company/registration/verify_mail.html.twig', [
            'companyRegistrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/registration/company/email_reset", name="app_company_register_email_reset")
     */
    public function resetMail(Request $request, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, CompanyManager $companyManager): Response
    {
        /** @var User $userAdmin */
        $userAdmin = $this->getUser();
        $company = $companyManager->getByRequest($request);

        $form = $this->createForm(VerifyMailFormType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $company->setActivationToken(md5(uniqid('', true)));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            // generate a signed url and email it to the SUPER_ADMIN
            $this->newCompanyMail->newCompanyMail('app_login', $company,
                (new TemplatedEmail())
                    ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                    ->to(new Address($this->superAdminEmail, 'Super_Administrateur'))
                    ->subject('Please Confirm new Company')
                    ->htmlTemplate('email/new_company_email.html.twig')
            );

            // generate a signed url and email it to the Company email
            $this->newCompanyMail->sendCompanyEmailConfirmation('app_verify_email_company', $userAdmin, $company,
                (new TemplatedEmail())
                    ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                    ->to($company->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('email/company_confirmation_email.html.twig')
            );

            return $guardHandler->authenticateUserAndHandleSuccess(
                $userAdmin,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('easyAdmin/company/registration/verify_mail.html.twig', [
            'companyRegistrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/verify/email/company/{token}", name="verify_email_company")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function activation($token, CompanyRepository $Companys, TranslatorInterface $trans, EntityManagerInterface $manager): Response
    {
        // On recherche si une Company avec ce token existe dans la base de données
        $company = $Companys->findOneBy(['activationToken' => $token]);

        // Si aucun utilisateur n'est associé à ce token
        if (!$company) {
            // On renvoie une erreur 404
            throw $this->createNotFoundException('Cet Company n\'existe pas');
        }

        // On supprime le token et on ajoute une nouvelle ligne d'investment pour chaque structure
        $company->setActivationToken(null);
        $company->setIsVerified(true);
        $entityManager = $this->getDoctrine()->getManager();
        $investment = new Investment();
        $investment->setName($company->getCompanySlug().'_resources');
        $companyRole = new CompanyRole();
        $companyRole->setRoles(['ROLE_OWNER']);
        $companyRole->setCompany($company);
        $companyRole->setInvestment($investment);
        $manager->persist($companyRole);
        $manager->persist($investment);
        $entityManager->flush();

        $message = $trans->trans('Your company email address has been verified.');
        $this->addFlash('success', $message);
        $url = $this->generateUrl('app_company_dashboard', ['companySlug' => $company->getCompanySlug()]);

        return $this->redirect($url);
    }
}
