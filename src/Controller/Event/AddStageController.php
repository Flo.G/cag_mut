<?php

namespace App\Controller\Event;

use App\Controller\Dashboard\Settings\CalendarTrait;
use App\Controller\Dashboard\Settings\EventTrait;
use App\Controller\Dashboard\Settings\GlobalUserMenuTrait;
use App\Entity\Company\Company;
use App\Entity\Event\Event;
use App\Entity\Event\Stage;
use App\Form\Event\StageType;
use App\Service\SecurityManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AddStageController extends AbstractDashboardController
{
    use CalendarTrait;

    /**
     * @Route("/company/{companySlug}/event/{eventSlug}/stage_add", name="app_company_event_add_stage")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addStage(Company $company,
                             Event $event,
                             Request $request,
                             EntityManagerInterface $manager,
                             SecurityManager $securityManager,
                             TranslatorInterface $translator): Response
    {
        $securityManager->ownSecurity();
        $this->company = $company;
        $this->event = $event;

        if ($securityManager->is($event, ['ROLE_STAGEMAN', 'ROLE_EVENT_ADMIN'])) {
            $stage = new Stage();
            $stage->setEvent($this->event);
            $form = $this->createForm(StageType::class, $stage);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $event->addStage($stage);
                $manager->persist($stage);
                $manager->flush();

                return $this->redirectToRoute('app_company_calendar', ['companySlug' => $this->company->getCompanySlug()]);
            }

            return $this->render('easyAdmin/event/add_stage.html.twig', [
                'stageForm' => $form->createView(),
                'company' => $this->company,
            ]);
        }
        $message = $translator->trans('Your role does not allow you to access the page');
        $this->addFlash('No_access', $message);

        return $this->redirectToRoute('app_user_dashboard');
    }
}
