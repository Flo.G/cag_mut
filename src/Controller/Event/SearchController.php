<?php

namespace App\Controller\Event;

use App\Entity\Event\Event;
use App\Repository\Event\EventRepository;
use App\Repository\Setting\EventUserRoleRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController
{
    /**
     * @Route("/event_autocomplete.json", name="api_event_autocomplete")
     */
    public function EventAutocomplete(Request $request, EventRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }

    /**
     * @Route("/event_user_role_autocomplete.json", name="api_event_user_role_autocomplete")
     */
    public function EventUserRoleAutocomplete(Request $request, EventUserRoleRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }
}
