<?php

namespace App\Controller\Event;

use App\Controller\Dashboard\Settings\CalendarTrait;
use App\Entity\Company\Company;
use App\Entity\Event\CompanyRole;
use App\Entity\Event\Event;
use App\Entity\Event\UserRole;
use App\Form\Event\EventType;
use App\Service\Company\CompanyManager;
use App\Service\Event\EventManager;
use App\Service\Provider\CalendarProvider;
use App\Service\Rent\RentManager;
use App\Service\SecurityManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class AddEventController extends AbstractDashboardController
{
    use CalendarTrait;

    /**
     * @Route("/company/{companySlug}/event_add", name="app_company_event_add")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function addEvent(Company $company,
        Request $request,
        EntityManagerInterface $manager,
        SecurityManager $securityManager,
        EventManager $eventManager,
        RentManager $rentManager,
        CompanyManager $companyManager,
        CalendarProvider $calendarProvider,
        TranslatorInterface $translator): Response
    {
        $user = $this->getUser();

        $this->company = $company;
        $securityManager->ownSecurity();
        $this->userCompanyRoles = $companyManager->getUserRoles($this->company);

        $events = $eventManager->getByCompanyNUser($this->company);
        $rentsIn = $rentManager->in($company);
        $rentsOut = $rentManager->out($company);
        $rentsInternal = $rentManager->internal($events);
        $data = $calendarProvider->fullData($events, $rentsIn, $rentsOut, $rentsInternal);

        if ($securityManager->is($this->company, ['ROLE_ADMIN_EVENTS', 'ROLE_COMPANY_ADMIN'])) {
            $companyRoles = new CompanyRole();
            $event = new Event();
            $userEvent = new UserRole();

            $eventsLinked = $eventManager->getByCompany($this->company);
            $choice['eventsLinked'] = $eventManager->filTerE($eventsLinked);

            $form = $this->createForm(EventType::class, $choice);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $event->setName($form->getData()['name']);
                $event->setDescription($form->getData()['description']);
                $event->setDateStart($form->getData()['dateStart']);
                $event->setDateEnd($form->getData()['dateEnd']);
                $event->setBudget($form->getData()['budget']);
                if (isset($form->getData()['event']) && !empty($form->getData()['event'])) {
                    $event->setParent($form->getData()['event']);
                }

                $userEvent->setRoles(['ROLE_EVENT_ADMIN']);
                $userEvent->setCompany($this->company);
                $userEvent->setUser($user);
                $userEvent->setEvent($event);

                $companyRoles->setCompany($this->company);
                $companyRoles->setRoles(['ROLE_OWNER']);
                $companyRoles->setEvent($event);

                $manager->persist($event);
                $manager->persist($companyRoles);
                $manager->persist($userEvent);

                $manager->flush();

                return $this->redirectToRoute('app_company_event_add_stage', ['companySlug' => $this->company->getCompanySlug(), 'eventSlug' => $event->getEventSlug()]); //TODO 'companySlug' => $this->company->getCompanySlug(),
            }

            return $this->render('easyAdmin/event/add_event.html.twig', [
                'eventForm' => $form->createView(),
                'data' => $data,
                'company' => $this->company,
            ]);
        }

        $message = $translator->trans('Your role does not allow you to access the page');
        $this->addFlash('No_access', $message);

        return $this->redirectToRoute('app_user_dashboard');
    }
}
