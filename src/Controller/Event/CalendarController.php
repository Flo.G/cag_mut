<?php

namespace App\Controller\Event;

use App\Controller\Dashboard\Settings\CalendarTrait;
use App\Entity\Company\Company;
use App\Service\Company\CompanyManager;
use App\Service\Event\EventManager;
use App\Service\Provider\CalendarProvider;
use App\Service\Rent\RentManager;
use App\Service\SecurityManager;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalendarController extends AbstractDashboardController
{
    use CalendarTrait;

    /**
     * @Route("/company/event", name="app_company_event")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(): Response
    {
        return $this->redirectToRoute('app_user');
    }

    /**
     * @Route("/company/{companySlug}/calendar", name="app_company_calendar")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function showCalendar(Company $company,
                                 CompanyManager $companyManager,
                                 EventManager $eventManager,
                                 RentManager $rentManager,
                                 CalendarProvider $calendarProvider,
                                 SecurityManager $securityManager): Response
    {
        $securityManager->ownSecurity();
        $this->company = $company;
        $this->userCompanyRoles = $companyManager->getUserRoles($this->company);

        $events = $eventManager->getByCompanyNUser($this->company);
        $rentsIn = $rentManager->in($company);
        $rentsOut = $rentManager->out($company);
        $rentsInternal = $rentManager->internal($events);

        $data = $calendarProvider->fullData($events, $rentsIn, $rentsOut, $rentsInternal);

        return $this->render('easyAdmin/company/calendar.html.twig',[
            'company' => $this->company,
            'data' => $data
        ]);
    }
}
