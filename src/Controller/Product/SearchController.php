<?php

namespace App\Controller\Product;

use App\Repository\Product\LineRepository;
use App\Repository\Product\StateRepository;
use App\Repository\Product\TagTypeLimitationRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController
{
    /**
     * @Route("/line_autocomplete.json", name="api_line_autocomplete")
     */
    public function LineAutocomplete(Request $request, LineRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }

    /**
     * @Route("/state_autocomplete.json", name="api_state_autocomplete")
     */
    public function StateAutocomplete(Request $request, StateRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }

    /**
     * @Route("tag_type_autocomplete.json", name="api_tag_type_autocomplete")
     */
    public function TagTypeAutocomplete(Request $request, TagTypeLimitationRepository $repository): JsonResponse
    {
        $q = $request->get('q');

        $data = ['results' => $repository->autocomplete($q)];

        return new JsonResponse($data);
    }
}
