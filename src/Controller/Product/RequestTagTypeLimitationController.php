<?php

namespace App\Controller\Product;

use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Entity\Product\Product;
use App\Entity\Product\TagTypeLimitation;
use App\Form\Product\RequestTagType;
use App\Mail\NewCompanyMail;
use App\Service\Product\ProductManager;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Message;
use Symfony\Component\Routing\Annotation\Route;

class RequestTagTypeLimitationController extends abstractDashboardController
{
    use CompanyTrait;

    private $noReplyEmail;
    private $superAdminEmail;
    private $newCompanyMail;

    public function __construct(NewCompanyMail $newCompanyMail)
    {
        $this->noReplyEmail = $_ENV['NO_REPLY_EMAIL'];
        $this->superAdminEmail = $_ENV['SUPER_ADMIN_EMAIL'];
        $this->newCompanyMail = $newCompanyMail;
    }

    /**
     * @Route("company/{companySlug}/product/{productReference}/request/tag/type/limitation", name="app_company_product_request_tag_type_limitation")
     */
    public function addTagTypeLimitation(Request $request,
                                         Company $company,
                                         string $productReference,
                                         ProductManager $productManager): Response
    {
        //$nextPage = 'app_company_investment_add_product_validation';
        $this->company = $company;
        $product = $productManager->getUnique($productReference);
        $tagType = new TagTypeLimitation();

        $form = $this->createForm(RequestTagType::class, $tagType);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->newCompanyMail->newCompanyMail('app_user', $company,
                (new TemplatedEmail())
                    ->from(new Address($this->noReplyEmail, 'No_reply_cagibig'))
                    ->to(new Address($this->superAdminEmail, 'Super_Administrateur'))
                    ->subject('New type tag request')
                    ->context(['contents'=>$form->getData()])
                    ->htmlTemplate('email/add_new_tag_type.html.twig'));

            return $this->render('easyAdmin/product/add/product_choice_tag_type_limitation.html.twig', [
                'companySlug' => $this->company->getCompanySlug(),
                'productReference' => $product->getProductReference(),
            ]);

        }
        return $this->render('easyAdmin/product/request_tag_type.html.twig', [
            'addTagType' => $form->createView(),
            'companySlug' => $this->company,
            'productReference' => $product->getProductReference()
        ]);
    }
}
