<?php

namespace App\Controller\Product;

use App\Controller\Dashboard\Settings\CompanyTrait;
use App\Entity\Company\Company;
use App\Entity\File\Image\ProductImage;
use App\Entity\Investment\Investment;
use App\Entity\Product\Product;
use App\Form\BaseType\VichAddImageType;
use App\Form\Product\AddProductDetailsType;
use App\Form\Product\AddProductLocType;
use App\Form\Product\AddProductNameType;
use App\Form\Product\AddProductUploadLibraryType;
use App\Form\Product\AddProductValidationType;
use App\Form\Product\AddProductValueType;
use App\Form\Product\ProductChoiceStockpileType;
use App\Form\Product\ProductChoiceTagTypeLimitationType;
use App\Service\Product\ProductManager;
use App\Service\Product\TagTypeManager;
use App\Service\Provider\BigValueProvider;
use App\Service\SecurityManager;
use App\Service\Stockpile\StockpileManager;
use App\Service\Upload\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class UpdateProductController extends abstractDashboardController
{
    use CompanyTrait;
    //1- Add Product choice stockpile
    //2- Add Name Product
    //3- Add Product Value
    //4- Add Upload Img product
    //5- Add United Mesure
    //5- Choice tagtype
    //6- Add géographie
    //7- Choice TagType/Add TagType
    //8- Add Uplaod fiche technique
    //9- Add Hastag
    //10- Choice/add Investment
    private $outForm;

    public function __construct()
    {
        $this->outForm = 'app_company_products';
    }

    /**
     * @Route("/company/{companySlug}/investment/{investmentSlug}/choice/stockpile/product", name="app_company_investment_update_choice_stockpile_product")
     */
    public function updateChoiceStockpile(Company $company,
                                    Investment $investment,
                                    StockpileManager $stockpileManager,
                                    SecurityManager $securityManager,
                                    Request $request): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_add_product_name';

        $this->company = $company;

        $stockpiles = $stockpileManager->filTerE($stockpileManager->getByCompany($this->company));

        if(empty($stockpiles)){
            return $this->render('easyAdmin/product/error/no_stockpile.html.twig', [
                'outForm' => $this->outForm,
                'companySlug' => $this->company->getCompanySlug(),
            ]);
        }

        $form = $this->createform(ProductChoiceStockpileType::class, $stockpiles);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $stockpile = $form->getData()['stockpile'];

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
                'stockpileSlug' => $stockpile->getStockpileSlug(),
            ]);
        }

        return $this->render('easyAdmin/product/add/product_choice_stockpile.html.twig', [
            'form' => $form->createView(),
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     * Route("/company/{companySlug}/investment/{investmentSlug}/{stockpileSlug}/add/product/name", name="app_company_investment_add_product_name")
     */
    public function addProductName(Company $company,
                                   Investment $investment,
                                   string $stockpileSlug,
                                   Request $request,
                                   SecurityManager $securityManager,
                                   EntityManagerInterface $manager,
                                   ProductManager $productManager,
                                   StockpileManager $stockpileManager): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_add_product_value';
        $this->company = $company;
        $product = new Product();

        $stockpile = $stockpileManager->getUnique($stockpileSlug);
        $product->setStockpile($stockpile);

        $form = $this->createForm(AddProductNameType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($product);
            $globalReference = $productManager->createGlobalReference($this->company, $product);
            $product->setGlobalReference($globalReference);

            $manager->flush();

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
                'productReference' => $product->getProductReference(),
            ]);
        }

        return $this->render('easyAdmin/product/add/product_name.html.twig', [
            'form' => $form->createView(),
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     * Route("/company/{companySlug}/investment/{investmentSlug}/add/product/{productReference}/value", name="app_company_investment_add_product_value")
     */
    public function addProductValue(Company $company,
                                    Investment $investment,
                                    string $productReference,
                                    Request $request,
                                    SecurityManager $securityManager,
                                    EntityManagerInterface $manager,
                                    ProductManager $productManager,
                                    BigValueProvider $bigValueProvider): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_add_product_upload';

        $this->company = $company;
        $product = $productManager->getUnique($productReference);

        $form = $this->createForm(AddProductValueType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bigValue = $bigValueProvider->value($product->getDepositValue(), $product->getLine()->getId(), $product->getState()->getId());
            $product->setBigCost($bigValue);
            $manager->flush();

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
                'productReference' => $product->getProductReference(),
            ]);
        }

        return $this->render('easyAdmin/product/add/product_value.html.twig', [
            'form' => $form->createView(),
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     *
     * @Route("/company/{companySlug}/update/product/{productReference}/uplaod", name="app_company_update_product_upload")
     */
    public function updateProductUpload(Company $company,
                                     Product $product,
                                     Request $request,
                                     EntityManagerInterface $manager,
                                     FileUploader $fileUploader,
                                     TranslatorInterface $translator): Response
    {
//        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
//            $this->redirect($request->headers->get('referer'));
//        }
        $nextPage = 'app_company_investment_add_product_details';
        $this->company = $company;

        if ($product->getImages()) {
            $images = $product->getImages();
        }

        $image = new ProductImage();

        $form = $this->createForm(VichAddImageType::class, $image);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $image->setCompany($company);
            $image->addProductImage($product);
            $image->setFilePath($image::PATH);
            $image->setDescription($image::DESCRIPTION);
            $product->addImage($image);
            $company->addProductImage($image);
            $manager->persist($image);
            $fileUploader->filter($image);
            $manager->flush();

            $this->addFlash('success', $translator->trans('The image had been registered, you can add more or go to the next step.'));

            return $this->redirectToRoute('app_compagny_product_details', [
                'companySlug'=>$company->getCompanySlug(),
                'productReference'=>$product->getProductReference()
            ]);
        }

        return $this->render('easyAdmin/product/update/image_upload.html.twig', [
            'form' => $form->createView(),
            'images' => $images,
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     * route("/company/{companySlug}/investment/{investmentSlug}/add/product/{productReference}/upload_from_library", name="app_company_investment_add_product_upload_from_library")
     */
    public function addProductUploadFromLibrary(Company $company,
                                                Investment $investment,
                                                string $productReference,
                                                Request $request,
                                                SecurityManager $securityManager,
                                                EntityManagerInterface $manager,
                                                ProductManager $productManager,
                                                TranslatorInterface $translator): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_add_product_details';
        $this->company = $company;
        $product = $productManager->getUnique($productReference);

        $image['image'] = $this->company->getProductImages()->getValues();

        $form = $this->createForm(AddProductUploadLibraryType::class, $image);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $images = $form->getData()['choice'];
            foreach ($images as $image) {
                $product->addImage($image);
            }
            $manager->flush();

            $this->addFlash('success', $translator->trans('The image had been registered, you can add more or go to the next step.'));

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
                'productReference' => $product->getProductReference(),
            ]);
        }

        return $this->render('easyAdmin/product/add/image_upload_from_library.html.twig', [
            'form' => $form->createView(),
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     * Route("/company/{companySlug}/investment/{investmentSlug}/add/product/{productReference}/details", name="app_company_investment_add_product_details")
     */
    public function addProductDetails(Company $company,
                                      Investment $investment,
                                      string $productReference,
                                      Request $request,
                                      SecurityManager $securityManager,
                                      EntityManagerInterface $manager,
                                      ProductManager $productManager): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_add_product_tag_type';

        $this->company = $company;
        $product = $productManager->getUnique($productReference);

        $form = $this->createForm(AddProductDetailsType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
                'productReference' => $product->getProductReference(),
            ]);
        }

        return $this->render('easyAdmin/product/add/product_details.html.twig', [
            'form' => $form->createView(),
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     * Route("/company/{companySlug}/investment/{investmentSlug}/product/{productReference}/tag_type_limitation", name="app_company_investment_add_product_tag_type")
     */
    public function productChoiceTagTypeLimitation(Company $company,
                                                   Investment $investment,
                                                   string $productReference,
                                                   Request $request,
                                                   SecurityManager $securityManager,
                                                   EntityManagerInterface $manager,
                                                   ProductManager $productManager,
                                                    TagTypeManager $tagTypeManager): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_add_product_loc';

        $this->company = $company;
        $product = $productManager->getUnique($productReference);
        $tagTypes = $tagTypeManager->getFromProduct($product);

        $form = $this->createform(ProductChoiceTagTypeLimitationType::class, array_unique($tagTypes));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resultTagTypes = array_merge($form->getData()['tagTypeLimitations'], $form->getData()['tagTypePropositions']);

            $product->addTagTypeLimitations($resultTagTypes);
            $manager->flush();

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
                'productReference' => $product->getProductReference(),
            ]);
        }

        return $this->render('easyAdmin/product/add/product_choice_tag_type_limitation.html.twig', [
            'form' => $form->createView(),
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     * Route("/company/{companySlug}/investment/{investmentSlug}/add/product/{productReference}/loc", name="app_company_investment_add_product_loc")
     */
    public function addProductLoc(Company $company,
                                    Investment $investment,
                                    string $productReference,
                                    Request $request,
                                    SecurityManager $securityManager,
                                    EntityManagerInterface $manager,
                                    ProductManager $productManager): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $nextPage = 'app_company_investment_add_product_validation';
        $this->company = $company;
        $product = $productManager->getUnique($productReference);

        $form = $this->createForm(AddProductLocType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->flush();

            return $this->redirectToRoute($nextPage, [
                'companySlug' => $this->company,
                'investmentSlug' => $investment->getInvestmentSlug(),
                'productReference' => $product->getProductReference(),
            ]);
        }

        return $this->render('easyAdmin/product/add/product_loc.html.twig', [
            'form' => $form->createView(),
            'nextPage' => $nextPage,
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }

    /**
     * Route("/company/{companySlug}/investment/{investmentSlug}/add/product/{productReference}/validation", name="app_company_investment_add_product_validation")
     */
    public function addProductValidation(Company $company,
                                         Investment $investment,
                                         string $productReference,
                                         Request $request,
                                         SecurityManager $securityManager,
                                         ProductManager $productManager): Response
    {
        if (!$securityManager->is($company, ['ROLE_COMPANY_ADMIN'])) {
            $this->redirect($request->headers->get('referer'));
        }

        $this->company = $company;
        $product = $productManager->getUnique($productReference);

        $form = $this->createForm(AddProductValidationType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $products = $productManager->addSimilar($product, $investment);

            return $this->render('easyAdmin/product/add/product_conclude.html.twig', [
                'company' => $this->company,
                'products' => $products,
                'outForm' => $this->outForm,
                'companySlug' => $this->company->getCompanySlug(),
            ]);
        }

        return $this->render('easyAdmin/product/add/product_validation.html.twig', [
            'form' => $form->createView(),
            'outForm' => $this->outForm,
            'companySlug' => $this->company->getCompanySlug(),
        ]);
    }
}
