<?php

namespace App\Controller\Security;

use App\Entity\User\User;
use App\Form\Security\PasswordForget;
use App\Form\Security\ResetPasswordType;
use App\Mail\EmailVerifier;
use App\Security\LoginFormAuthenticator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class PasswordForgetController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("/password/forget", name="password_forget")
     */
    public function forgetPassword(
        Request $request,
        GuardAuthenticatorHandler $guardHandler,
        LoginFormAuthenticator $authenticator,
        TranslatorInterface $trans,
        string $noReplyEmail
    ): Response {
        $entityManager = $this->getDoctrine()->getManager();

        $form = $this->createForm(PasswordForget::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $data = $form->getData();
            if ($user = $entityManager->getRepository(User::class)->findOneBy(['email' => $data['email']])) {
                // generate a signed url and email it to the user
                $this->emailVerifier->sendEmailConfirmation('password_reset', $user,
                    (new TemplatedEmail())
                        ->from(new Address($noReplyEmail, 'No_reply_cagibig'))
                        ->to($user->getEmail())
                        ->subject('Please Change your Password')
                        ->htmlTemplate('email/change_password.html.twig')
                );

                // do anything else you need here, like send an email
                $user->addRole('ROLE_PWD_CHANGE');
                $entityManager->persist($user);
                $entityManager->flush();

                return $guardHandler->authenticateUserAndHandleSuccess(
                    $user,
                    $request,
                    $authenticator,
                    'mail' // firewall name in security.yaml
                );
            }
            $message = $trans->trans('This email address is not registered, maybe you wrote it wrong?');
            $this->addFlash('verify_email_error', $message);

            return $this->redirectToRoute('password_forget');
        }

        return $this->render('security/password_forget.html.twig', [
            'form_email' => $form->createView(),
        ]);
    }

    /**
     * @Route("/password/wait", name="password_wait")
     */
    public function waitPassword(Request $request): Response
    {
        return $this->render('security/password_wait.html.twig');
    }

    /**
     * @Route("/password/reset", name="password_reset")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder, TranslatorInterface $trans): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        // create form with user datas
        try {
            /** @var User $user */
            $user = $this->getUser();
            $this->emailVerifier->handleEmailConfirmation($request, $user);

            $form = $this->createForm(ResetPasswordType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $user->setPassword(
                     $passwordEncoder->encodePassword(
                         $user,
                         $form->get('password')->getData()
                     )
                 );
                $user->removeRole('ROLE_PWD_CHANGE');
                $entityManager->persist($user);
                $entityManager->flush();

                $message = $trans->trans('Your password as been successfully reinitialized.');
                $this->addFlash('success', $message);

                return $this->redirectToRoute('app_logout');
            }

            return $this->render('security/password_reset.html.twig', [
                 'form' => $form->createView(),
             ]);
        } catch (VerifyEmailExceptionInterface $exception) {
            $message = $exception->getReason();
            $errorMessage = "Looks like something goes wrong or $message";
            $this->addFlash('verify_email_error', $errorMessage);

            return $this->redirectToRoute('password_reset_error');
        }
    }

    /**
     * @Route("/password/reset_error", name="password_reset_error")
     */
    public function resetPasswordError(Session $session): Response
    {
        $errors = [];
        foreach ($session->getFlashBag()->get('verify_email_error', []) as $message) {
            $errors[] = $message;
        }

        return $this->render('security/error.html.twig', [
            'errors' => $errors,
        ]);
    }
}
