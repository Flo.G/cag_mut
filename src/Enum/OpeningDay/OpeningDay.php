<?php

namespace App\Enum\OpeningDay;

use Elao\Enum\FlaggedEnum;

class OpeningDay extends FlaggedEnum
{
    public const DAY_MONDAY = 1;
    public const DAY_TUESDAY = 2;
    public const DAY_WEDNESDAY = 4;
    public const DAY_THURSDAY = 8;
    public const DAY_FRIDAY = 16;
    public const DAY_SATURDAY = 32;
    public const DAY_SUNDAY = 64;

    public const ALL_WEEK = self::DAY_MONDAY | self::DAY_TUESDAY | self::DAY_WEDNESDAY | self::DAY_THURSDAY | self::DAY_FRIDAY | self::DAY_SATURDAY | self::DAY_SUNDAY;
    public const WORK_WEEK = self::DAY_MONDAY | self::DAY_TUESDAY | self::DAY_WEDNESDAY | self::DAY_THURSDAY | self::DAY_FRIDAY;
    public const WEEK_END = self::DAY_SATURDAY | self::DAY_SUNDAY;


    public static function values(): array
    {
        return [
            static::DAY_MONDAY,
            static::DAY_TUESDAY,
            static::DAY_WEDNESDAY,
            static::DAY_THURSDAY,
            static::DAY_FRIDAY,
            static::DAY_SATURDAY,
            static::DAY_SUNDAY,
        ];
    }

    public static function readables(): array
    {
        return [
            static::DAY_MONDAY => 'monday',
            static::DAY_TUESDAY => 'tuesday',
            static::DAY_WEDNESDAY => 'wednesday',
            static::DAY_THURSDAY => 'thursday',
            static::DAY_FRIDAY => 'friday',
            static::DAY_SATURDAY => 'saturday',
            static::DAY_SUNDAY => 'sunday',

            static::DAY_MONDAY | static::DAY_TUESDAY | static::DAY_WEDNESDAY | static::DAY_THURSDAY | static::DAY_FRIDAY | static::DAY_SATURDAY | static::DAY_SUNDAY => 'all week',
            static::DAY_MONDAY | static::DAY_TUESDAY | static::DAY_WEDNESDAY | static::DAY_THURSDAY | static::DAY_FRIDAY => 'work week',
            static::DAY_SATURDAY | static::DAY_SUNDAY => 'weekend',
        ];
    }
}
