<?php


namespace App\Enum\OpeningDay;


use Elao\Enum\Bridge\Doctrine\DBAL\Types\AbstractEnumType;

final class OpeningDayType extends AbstractEnumType
{
    public const NAME = 'opening day';

    protected function getEnumClass(): string
    {
        return OpeningDay::class;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}
