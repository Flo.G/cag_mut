<?php

namespace App\Enum\TVARate;

use Elao\Enum\ChoiceEnumTrait;
use Elao\Enum\ReadableEnum;

class TVARate extends ReadableEnum
{
    use ChoiceEnumTrait;

    public const RATE_5_5 = 5.5;
    public const RATE_20 = 20;

    protected static function choices(): array
    {
        return [
            self::RATE_5_5 => '5.5 %',
            self::RATE_20 => '20 %',
        ];
    }
}
