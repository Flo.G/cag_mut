<?php


namespace App\Enum\TVARate;

use Elao\Enum\Bridge\Doctrine\DBAL\Types\AbstractEnumType;



class TVARateType extends AbstractEnumType
{
    public const NAME = 'TVA rate';

    protected function getEnumClass(): string
    {
        return TVARate::class;
    }

    public function getName(): string
    {
        return static::NAME;
    }

}