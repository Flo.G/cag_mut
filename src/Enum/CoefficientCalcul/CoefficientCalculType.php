<?php


namespace App\Enum\CoefficientCalcul;

use Elao\Enum\Bridge\Doctrine\DBAL\Types\AbstractEnumType;



class CoefficientCalculType extends AbstractEnumType
{
    public const NAME = 'Coefficient time calcul';

    protected function getEnumClass(): string
    {
        return CoefficientCalcul::class;
    }

    public function getName(): string
    {
        return static::NAME;
    }

}