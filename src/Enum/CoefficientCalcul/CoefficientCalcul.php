<?php

namespace App\Enum\CoefficientCalcul;

use Elao\Enum\ChoiceEnumTrait;
use Elao\Enum\ReadableEnum;

class CoefficientCalcul extends ReadableEnum
{
    use ChoiceEnumTrait;

    public const LINEAR = 'linear';
    public const LOGARITHM = 'logarithm';
    public const STEPPED = 'stepped';

    protected static function choices(): array
    {
        return [
            self::LINEAR => 'linear',
            self::LOGARITHM => 'logarithm',
            self::STEPPED => 'stepped',
        ];
    }
}
