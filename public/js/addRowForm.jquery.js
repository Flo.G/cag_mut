/*----------INTEGRATION-EXEMPLE--------------------------
{{form_start (emails)}}
    {{ form_row(emails.email) }}
    <ul class="emails no-dots" data-prototype="{{ form_widget(emails.emails.vars.prototype)|e('html_attr') }}">
        {% for mail in emails.emails %}
            <li>{{ form_row(mail.email) }}</li>
        {% endfor %}
    </ul>
    <button class="register-button">{%trans%}Submit and next{%endtrans%}</button>
    <a href="{{path('app_user_register_contact')}}" class="register-button float-right">Next</a>
{{form_end (emails)}}


<script type="text/javascript">let thisContent = 'emails';</script> TODO modif let thisContent definition
<script type="text/javascript" src="{{ asset('js/addRowForm.jquery.js') }}"  ></script>

*/


var $addButtonLink = $('<a href="#" class="add-button"><i class="fas fa-plus-circle" data-collection-holder-class="' + thisContent + '"></i></a>');
//var $addEmailLink = $('<a href="#" class="add_item_link">Add an email</a>');
var $newLinkLi = $('<li></li>').append($addButtonLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    var $collectionHolder = $('ul.' + thisContent + '.' + thisContent );

    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addButtonLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see code block below)
        addRowForm($collectionHolder, $newLinkLi);
    });
});

function addRowForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '$$name$$' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<li></li>').append(newForm);

    // also add a remove button, just for this example
    $newFormLi.append('<a href="#" class="remove-button"><i class="far fa-times-circle"></i></a>');

    $newLinkLi.before($newFormLi);

    // handle the removal, just for this example
    $('.remove-button').click(function(e) {
        e.preventDefault();
        $(this).parent().remove();
        return false;
    });
}