$('.file-select-button').click(function(){
    $(this).closest('.upload-image-strip').find('input').click();
});

$('.upload-image-strip input').on('change', function (e) {
    let filename = e.target.value.split('\\').pop();
    let nf = $(this).closest('.upload-image-strip').find('.name-file');

    if (filename.length > 12) {
        let extension = filename.split('.').pop();
        let truncate = filename.substr(0, 12) + '... .' + extension;

        nf.text(truncate);
    } else if(filename.length > 0) {
        nf.text(filename);
    }
});

function img_pathUrl(input){
    $('#img_url')[0].src = (window.URL ? URL : webkitURL).createObjectURL(input.files[0]);
}

$("#TERRAIN-PLAT-POUR-INSTALLATION").click(function(){
    if($("#TERRAIN-PLAT-POUR-INSTALLATION").hasClass('bgChcker')){
        $("#TERRAIN-PLAT-POUR-INSTALLATION").removeClass('bgChcker');
    }
    else {
        $("#TERRAIN-PLAT-POUR-INSTALLATION").addClass('bgChcker');
    }
})

$("#NETTOYAGE-EN-FIN-DUTILISATION").click(function(){
    if($("#NETTOYAGE-EN-FIN-DUTILISATION").hasClass('bgChcker')){
        $("#NETTOYAGE-EN-FIN-DUTILISATION").removeClass('bgChcker');
    }
    else {
        $("#NETTOYAGE-EN-FIN-DUTILISATION").addClass('bgChcker');
    }
})


//vich_add_image_imageFile_file