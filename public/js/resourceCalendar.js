document.addEventListener('DOMContentLoaded', function () {
    let calendarEl = document.getElementById('calendar');
    let resources = JSON.parse(calendarEl.getAttribute("data-resources"));
    let events = JSON.parse(calendarEl.getAttribute("data-events"));
    let locale = calendarEl.getAttribute("data-locale");

    let calendar = new FullCalendar.Calendar(calendarEl, {
        selectable: true,
        locale: locale,
        timeZone: 'Europe/Paris',/*TODO recup la timezone en variable*/
        initialView: 'resourceTimelineMonth'/*'resourceTimelineDay'*/,
        aspectRatio: 1.5,
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'timelineMonth',
        },
        editable: false,
        resourceAreaHeaderContent: 'Events',
        resourceAreaWidth: "13%",
        resources: resources,
        events: events,

        /* dateClick: function(info) {
            //TODO add onClick
            alert('clicked ' + info.dateStr + ' on resource ' + info.resource.id);
        },
        select: function(info) {
            //TODO add onSelect
            alert('selected ' + info.startStr + ' to ' + info.endStr + ' on resource ' + info.resource.id);
        },*/
        //popup with event information
        eventClick: function (info) {
            let eventObj = info.event;
            let resourceType = eventObj.extendedProps.resourceType;

            // The condition allows to differentiate an Event from a Rent
            switch (resourceType) {
                case 'event' :
                    formatPopupEvent(eventObj);
                    break;
                case 'rent_in':
                    formatPopupRentInOut(eventObj);
                    break;
                case 'rent_out':
                    formatPopupRentInOut(eventObj);
                    break;
                case 'rent_internal':
                    formatPopupInternRent(eventObj);
                    break;
                default:
                    formatPopupEmpty(eventObj);
            }
        }
    });

    calendar.on('eventChange', (e) => {
        let url = `/api/calendar/${e.event.id}/edit`
        let datas = {
            "title": e.event.title,
            "descriptions": e.event.extendedProps.description,
            "start": e.event.start,
            "end": e.event.end,
            "backgroundColor": e.event.backgroundColor,
            "allDay": e.event.allDay,
            "stage": e.event.extendedProps.stage,
            "productsName": e.event.extendedProps.productsName,
            "productsQuantity": e.event.extendedProps.productsQuantity,
            "userFirstName": e.event.extendedProps.userFirstName,
            "userLastName": e.event.extendedProps.userLastName,
            "userPhones": e.event.extendedProps.userPhones,
            "userEmail": e.event.extendedProps.userEmail,
            "resourceType": e.event.extendedProps.resourceType

        }
        let xhr = new XMLHttpRequest
        xhr.open("PUT", url)
        xhr.send(JSON.stringify(datas))
    })

    calendar.render();
});

function formatPopupRentInOut(eventObject) {
    Swal.fire(
        {
            title: eventObject.title,
            html: ifNull(eventObject.extendedProps.description)
                + ' <br> du ' + FullCalendar.formatDate(eventObject.start, {
                    month: 'long',
                    year: 'numeric',
                    day: 'numeric',
                    timeZoneName: 'short',
                    timeZone: 'UTC',
                    locale: 'fr'
                })
                + "<br> au " + FullCalendar.formatDate(eventObject.end, {
                    month: 'long',
                    year: 'numeric',
                    day: 'numeric',
                    timeZoneName: 'short',
                    timeZone: 'UTC',
                    locale: 'fr'
                })
                + "<br> stage : " + ifNull(eventObject.extendedProps.stage) + "<br>"
                //retourne une liste de produit par son nom et sa quantité
                + listProduct(eventObject.extendedProps.productsName, eventObject.extendedProps.productsQuantity) + "<br>",
            footer: "<strong> Commanditaire : </strong><br>"
                + eventObject.extendedProps.userFirstName + " "
                + eventObject.extendedProps.userLastName + "<br>"
                + eventObject.extendedProps.userPhones + "<br>"
                + eventObject.extendedProps.userEmail
        })
}

function formatPopupEvent(eventObject){
    Swal.fire(
        {
            title: eventObject.title,
            html: ifNull(eventObject.extendedProps.description)
                + ' <br> du ' + FullCalendar.formatDate(eventObject.start, {
                    month: 'long',
                    year: 'numeric',
                    day: 'numeric',
                    timeZoneName: 'short',
                    timeZone: 'UTC',
                    locale: 'fr'
                })
                + "<br> au " + FullCalendar.formatDate(eventObject.end, {
                    month: 'long',
                    year: 'numeric',
                    day: 'numeric',
                    timeZoneName: 'short',
                    timeZone: 'UTC',
                    locale: 'fr'
                })
                + "<br> stage : " + ifNull(eventObject.extendedProps.stage) + "<br>",
        }
    )
}

function formatPopupInternRent(eventObject){
    Swal.fire(
        {
            title: eventObject.title,
            html: ifNull(eventObject.extendedProps.description)
                + ' <br> du ' + FullCalendar.formatDate(eventObject.start, {
                    month: 'long',
                    year: 'numeric',
                    day: 'numeric',
                    timeZoneName: 'short',
                    timeZone: 'UTC',
                    locale: 'fr'
                })
                + "<br> au " + FullCalendar.formatDate(eventObject.end, {
                    month: 'long',
                    year: 'numeric',
                    day: 'numeric',
                    timeZoneName: 'short',
                    timeZone: 'UTC',
                    locale: 'fr'
                })
                + "<br> stage : " + ifNull(eventObject.extendedProps.stage) + "<br>"
                //retourne une liste de produit par son nom et sa quantité
                + listProduct(eventObject.extendedProps.productsName, eventObject.extendedProps.productsQuantity) + "<br>",
            footer: "<strong> Commanditaire : </strong><br>"
                + 'bein c\'est toi!!!!???'
        })
}

function formatPopupEmpty(eventObj) {
    Swal.fire(
        {
            title: 'missing data'
        })
}

function listProduct($array1, $array2) {
    let listProduct = [];
    for (let i = 0; i < $array1.length; i++) {
        listProduct.push("<br><strong>" + $array1[i] + "</strong>" + " : " + $array2[i])
    }
    return listProduct;
}

function ifNull(arg) {
    if (!arg) {
        arg = '';
    }
    return arg
}

